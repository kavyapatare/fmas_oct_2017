package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
//import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.LinearLayout;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

public class FMGetStartedActivity extends AppCompatActivity {
    private static final String TAG = FMGetStartedActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private IACustomButton mStartCameraTutorialButton;
    private String APP_ID;
    private Context mContext;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private LinearLayout mGetStartedLinearLayout;
    public  boolean mIsFromActivityResult = false;
    public Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);
        mContext = this;
        initializeView();
        APP_ID = getApplicationContext().getResources().getString(R.string.APP_ID);
        checkForUpdates();
    }

    /**
     * called to initialize the get started screen ui
     */
    private void initializeView(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar_get_started);
        mToolBarCustomerNameTextView = (IACustomTextView)mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mStartCameraTutorialButton = (IACustomButton) findViewById(R.id.get_started_next_btn);
        mGetStartedLinearLayout = (LinearLayout) findViewById(R.id.get_started_ll);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();
        FMCustomer fmCustomer = new FMCustomer();
        fmCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));
        if (!fmCustomer.getCustomerEmailId().equalsIgnoreCase("")){
            mToolBarCustomerNameTextView.setText("Hello " + fmCustomer.getCustomerFirstName() + " !");
        }

        mStartCameraTutorialButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mStartCameraTutorialButton.setEnabled(false);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(1), "CurrentFileIndexNo");
                    intent = new Intent(FMGetStartedActivity.this, FMShowTutorialVideoActivity.class);
                    startActivityForResult(intent,Integer.parseInt(mContext.getResources().getString(R.string.show_tutorial_video)));
                    mStartCameraTutorialButton.setEnabled(true);


            }
        });
        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    /**
     * called to register the crash analytics sdk HockeyApp to check for
     * crashes
     */
    private void checkForCrashes() {
        CrashManager.register(this, APP_ID);
    }

    /**
     * called to register the crash analytics sdk HockeyApp to check for
     * updates after a crash
     */
    private void checkForUpdates() {
        UpdateManager.register(this, APP_ID);
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateManager.unregister();
        mStartCameraTutorialButton.setEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
        if (mIsFromActivityResult){
            mIsFromActivityResult = false;
            if (mGetStartedLinearLayout.getVisibility() == View.VISIBLE){
                mGetStartedLinearLayout.setVisibility(View.INVISIBLE);
            }
        } else {
            if (mGetStartedLinearLayout.getVisibility() == View.INVISIBLE){
                mGetStartedLinearLayout.setVisibility(View.VISIBLE);
            }
        }
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mStartCameraTutorialButton.setEnabled(true);
            }
        },1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mStartCameraTutorialButton.setEnabled(false);

        if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.show_tutorial_video))){
            if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result))){
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result)),data);
                finish();

            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.reinitialize_activity))){
                mGetStartedLinearLayout.setVisibility(View.INVISIBLE);
                mIsFromActivityResult = true;
                Intent intent = new Intent(FMGetStartedActivity.this,FMShowTutorialVideoActivity.class);
                startActivityForResult(intent,Integer.parseInt(mContext.getResources().getString(R.string.show_tutorial_video)));
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images))){

            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
