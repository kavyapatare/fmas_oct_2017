package com.findmeashoe_appc.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;

import org.json.JSONException;
import org.json.JSONObject;

public class FMStartingActivity extends AppCompatActivity {
    private static final String TAG = "FMStartingActivity";
    private Context mContext;
    private static final int RESULT_LOGIN_SUCCESSFUL = 101;
    private static final int RESULT_REGISTER_SUCCESSFUL = 102;
    private IACustomButton mStartingLoginButton;
    private IACustomButton mStartingRegisterButton;
    private String mFootResult;
    private GetMeasurementAsyncTask mGetMeasurementAsyncTask;
    private LinearLayout mStartingLayout;
    private FMCustomer currentCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the starting screen ui
     */
    private void initializeView() {
        mStartingLoginButton = (IACustomButton) findViewById(R.id.get_started_login_btn);
        mStartingRegisterButton = (IACustomButton) findViewById(R.id.get_started_register_btn);
        mStartingLayout = (LinearLayout) findViewById(R.id.starting_rl);
        mStartingLayout.setVisibility(View.GONE);

        mStartingLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToLoginActivity();
            }
        });
        mStartingRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToRegisterActivity();
            }
        });

        if (new IAUtilities().getSharedPreferences(mContext,"ServerIP").length() > 0){
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(new IAUtilities().getSharedPreferences(mContext,"ServerIP"));
                FMUrl.CURRENT_IP_ADDRESS = jsonObject.getString("CurrentIp");
                if (FMUrl.CURRENT_IP_ADDRESS.equalsIgnoreCase("")){
                    FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("CurrentIp",FMUrl.CURRENT_IP_ADDRESS);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            new IAUtilities().setSharedPreferences(mContext, jsonObject.toString(), "ServerIP");
            IAUtilities.showingLogs(TAG, "Json Object = " + jsonObject.toString(), IAUtilities.LOGS_SHOWN);
        }

        currentCustomer = new FMCustomer();
        currentCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));
        if (new IAUtilities().isNetworkAvailable(mContext))
            new GetLocationAsyncTask(currentCustomer).execute();
        else{
            new IAUtilities().setSharedPreferences(mContext,"IN","CountryCode");
            if (!currentCustomer.getCustomerEmailId().equalsIgnoreCase("")){
                mGetMeasurementAsyncTask = new GetMeasurementAsyncTask(currentCustomer);
                mGetMeasurementAsyncTask.execute();
            }else{
                mStartingLayout.setVisibility(View.VISIBLE);
            }
        }
    }

    /**
     * called to redirect the user to the get started screen
     */
    private void goToGetStartedActivity() {
        Intent intent = new Intent(FMStartingActivity.this, FMGetStartedActivity.class);
        startActivityForResult(intent,Integer.parseInt(mContext.getResources().getString(R.string.get_started)));
    }
    /**
     * called to redirect the user to the one more step screen
     */
    private void goToOneMoreStepActivity() {
        Intent intent = new Intent(FMStartingActivity.this, FMOneMoreStepActivity.class);
        startActivity(intent);
        finish();
    }

    /**
     * called to redirect the user to the login screen
     */
    private void goToLoginActivity() {
        Intent intent = new Intent(FMStartingActivity.this, FMLoginActivity.class);
        startActivityForResult(intent, RESULT_LOGIN_SUCCESSFUL);
    }

    /**
     * called to redirect the user to the register screen
     */
    private void goToRegisterActivity() {
        Intent intent = new Intent(FMStartingActivity.this, FMRegisterActivity.class);
        startActivityForResult(intent, RESULT_REGISTER_SUCCESSFUL);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IAUtilities.showingLogs(TAG,"requestCode = " + requestCode,IAUtilities.LOGS_SHOWN);
        IAUtilities.showingLogs(TAG,"resultCode = " + resultCode,IAUtilities.LOGS_SHOWN);
        if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.on_successful_response))){
            if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result))){

                finish();
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images))){
                goToGetStartedActivity();
            } else {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code)));
                finish();
            }
        } else if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.get_started))){
            if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result))){
                finish();
            } else {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code)));
                finish();
            }
        } else if (requestCode == RESULT_LOGIN_SUCCESSFUL){
            if (resultCode == RESULT_OK){
                mStartingLayout.setVisibility(View.GONE);
                currentCustomer = new FMCustomer();
                currentCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));
                if (new IAUtilities().isNetworkAvailable(mContext))
                    new GetLocationAsyncTask(currentCustomer).execute();
                else{
                    new IAUtilities().setSharedPreferences(mContext,"IN","CountryCode");
                    if (!currentCustomer.getCustomerEmailId().equalsIgnoreCase("")){
                        mGetMeasurementAsyncTask = new GetMeasurementAsyncTask(currentCustomer);
                        mGetMeasurementAsyncTask.execute();
                    }else{
                        mStartingLayout.setVisibility(View.VISIBLE);
                    }
                }
            }
        } else if (requestCode == RESULT_REGISTER_SUCCESSFUL){
            if (resultCode == RESULT_OK){
                Intent intent = new Intent(FMStartingActivity.this, FMOneMoreStepActivity.class);
                startActivity(intent);
            }
        }

    }

    /**
     * used to call the Get Measurement api
     */
    private class GetMeasurementAsyncTask extends AsyncTask<Void, Integer, FMResponse> {
        private FMCustomer mCurrentFMCustomer;


        public GetMeasurementAsyncTask(FMCustomer mFMCustomer) {
            this.mCurrentFMCustomer = mFMCustomer;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            String mResultUrl = null;
            String mEmailID = null;

            mEmailID = mCurrentFMCustomer.getCustomerEmailId();
            IAUtilities.showingLogs(TAG, "email = " + mEmailID, IAUtilities.LOGS_SHOWN);

            mResultUrl = FMUrl.TARGET_BASE_URL + "services/get-customer-measurement/" + mEmailID;
            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse mFMResponse = null;
            mFMResponse = webConnection.getResponse(mResultUrl);

            IAUtilities.showingLogs(TAG, "Timestamp before request = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            IAUtilities.showingLogs(TAG, "OnGetMeasurementResponse == " + mFMResponse, IAUtilities.LOGS_SHOWN);

            return mFMResponse;
        }

        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            IAUtilities.showingLogs(TAG, "Timestamp after response = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "GetMeasurementAsyncTask completed", IAUtilities.LOGS_SHOWN);
                //go to next page
                try {
                    JSONObject jsonObject = new JSONObject(response.getResponseData());
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        mFootResult = jsonObject1.toString();
                        goToOnSuccessfulResponseActivity();
                    } else if (jsonObject.getString("status").equalsIgnoreCase("error")){
                        goToOneMoreStepActivity();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //show error message
                goToGetStartedActivity();
            }
        }
    }

    /**
     * called to redirect the user to the successful result screen
     */
    private void goToOnSuccessfulResponseActivity() {
        Intent intent = new Intent(FMStartingActivity.this, FMOnSuccessfulResponseActivity.class);
        new IAUtilities().setSharedPreferences(mContext, mFootResult, "FootResult");
        startActivityForResult(intent,Integer.parseInt(mContext.getResources().getString(R.string.on_successful_response)));
    }

    /**
     * called to get the location of the user
     */
    private class GetLocationAsyncTask extends AsyncTask<Void,Void,FMResponse>{
        private ProgressDialog mProgressDialog = null;
        private boolean mLetterPaper = false;
        private FMCustomer fmCustomer;

        public GetLocationAsyncTask(FMCustomer fmCustomer) {
            this.fmCustomer = fmCustomer;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!((AppCompatActivity)mContext).isFinishing()) {
                mProgressDialog = new ProgressDialog(mContext);
                mProgressDialog.setMessage("Finding Location...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.setIndeterminate(true);
                mProgressDialog.show();
            }
        }

        @Override
        protected FMResponse doInBackground(Void... voids) {
            FMWebConnection fmWebConnection = new FMWebConnection();
            FMResponse fmResponse = fmWebConnection.getResponse(FMUrl.GET_LOCATION_URL);
            return fmResponse;
        }

        @Override
        protected void onPostExecute(FMResponse fmResponse) {
            super.onPostExecute(fmResponse);
            if (!((AppCompatActivity)mContext).isFinishing() && mProgressDialog != null) {
                mProgressDialog.dismiss();
            }
            if (fmResponse.isSuccess()){
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(fmResponse.getResponseData());
                    String mCountryCode = jsonObject.getString("country_code");
                    new IAUtilities().setSharedPreferences(mContext,mCountryCode,"CountryCode");
                    if (mCountryCode.equalsIgnoreCase("IN")){
                        mLetterPaper = false;
                        new IAUtilities().setSharedPreferences(mContext,String.valueOf(mLetterPaper),"LetterPaper");
                    } else if (mCountryCode.equalsIgnoreCase("US") || mCountryCode.equalsIgnoreCase("CA")){
                        mLetterPaper = true;
                        new IAUtilities().setSharedPreferences(mContext,String.valueOf(mLetterPaper),"LetterPaper");
                    } else{
                        mLetterPaper = false;
                        new IAUtilities().setSharedPreferences(mContext,String.valueOf(mLetterPaper),"LetterPaper");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                Toast.makeText(mContext,"Location not available.",Toast.LENGTH_SHORT).show();
                new IAUtilities().setSharedPreferences(mContext,"IN","CountryCode");
            }
            if (!fmCustomer.getCustomerEmailId().equalsIgnoreCase("")){
                mGetMeasurementAsyncTask = new GetMeasurementAsyncTask(fmCustomer);
                mGetMeasurementAsyncTask.execute();
            }else{
                mStartingLayout.setVisibility(View.VISIBLE);
            }
        }
    }
}
