package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;

public class FMOneMoreStepActivity extends AppCompatActivity {
    private Context mContext;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private IACustomButton mGotoGetStartedButton;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_one_more_step);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the one more step screen ui
     */
    private void initializeView(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar_one_more_step);
        mToolBarCustomerNameTextView = (IACustomTextView)mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mGotoGetStartedButton = (IACustomButton) findViewById(R.id.one_more_step_next_btn);
        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();
        FMCustomer fmCustomer = new FMCustomer();
        fmCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));
        if (!fmCustomer.getCustomerEmailId().equalsIgnoreCase("")){
            mToolBarCustomerNameTextView.setText("Hello " + fmCustomer.getCustomerFirstName() + " !");
        }

        mGotoGetStartedButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mGotoGetStartedButton.setEnabled(false);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(1), "CurrentFileIndexNo");
                Intent intent = new Intent(FMOneMoreStepActivity.this,FMGetStartedActivity.class);
                startActivity(intent);
                mGotoGetStartedButton.setEnabled(true);
                finish();
            }
        });
        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
