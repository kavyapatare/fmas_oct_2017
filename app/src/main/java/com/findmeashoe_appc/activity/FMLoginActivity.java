package com.findmeashoe_appc.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomEditText;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class FMLoginActivity extends AppCompatActivity {
    private static final String TAG = "FMLoginActivity";
    private Context mContext;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private IACustomEditText mLoginUserEmailField;
    private IACustomEditText mLoginUserPasswordField;
    private IACustomButton mLoginLayoutStartButton;
    private String mFootResult;
    private LinearLayout mForgotPasswordLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the login screen ui
     */
    private void initializeView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_starting);
        mToolBarCustomerNameTextView = (IACustomTextView)mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mLoginLayoutStartButton = (IACustomButton) findViewById(R.id.login_start_btn);
        mLoginUserEmailField = (IACustomEditText) findViewById(R.id.login_user_email_field_tv);
        mLoginUserPasswordField = (IACustomEditText) findViewById(R.id.login_user_password_field_tv);
        mForgotPasswordLayout = (LinearLayout) findViewById(R.id.forgot_password_ll);

        mToolBarCustomerNameTextView.setText("Login");
        mLoginLayoutStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mLoginLayoutStartButton.setEnabled(false);
                if (isValidLogin()) {
                    new IAUtilities().hideKeyboard(v, mContext);
                    onLoginButtonClick();
                } else {
                    new IAUtilities().setSharedPreferences(mContext, null, "FMCustomer");

                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mLoginLayoutStartButton.setEnabled(true);
                    }
                },5000);
            }
        });

        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mForgotPasswordLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (new IAUtilities().isNetworkAvailable(mContext)){
                    mForgotPasswordLayout.setEnabled(false);
                    if (new IAUtilities().validateEmailId(mLoginUserEmailField.getText().toString(),mContext)){
                        new ForgotPasswordAsyncTask(mLoginUserEmailField.getText().toString()).execute();
                    } else{
                        openDialog(mContext.getResources().getString(R.string.please_enter_a_valid_registered_email_address));
                        mForgotPasswordLayout.setEnabled(true);
                    }
                }
            }
        });
    }

    /**
     * used to call the forgot password api
     */
    private class ForgotPasswordAsyncTask extends AsyncTask<Void,Void,FMResponse>{
        private ProgressDialog mProgressDialog;
        private String mForgotPasswordEmailId;

        public ForgotPasswordAsyncTask(String mForgotPasswordEmailId) {
            this.mForgotPasswordEmailId = mForgotPasswordEmailId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(mContext);
            mProgressDialog.setMessage("Checking..");
            mProgressDialog.setCancelable(false);
            mProgressDialog.setIndeterminate(true);
            if (!((AppCompatActivity)mContext).isFinishing()){
                mProgressDialog.show();
            }
        }

        @Override
        protected FMResponse doInBackground(Void... voids) {
            FMWebConnection webConnection = new FMWebConnection();
            String mForgotPasswordUrlLink = FMUrl.PRESTASHOP_BASE_API + FMUrl.FORGOT_PASSWORD_URL;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("email",mForgotPasswordEmailId);
                jsonObject.put("ws_key", mContext.getResources().getString(R.string.ws_key));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            FMResponse fmResponse = webConnection.getResponse(mForgotPasswordUrlLink,jsonObject);
            IAUtilities.showingLogs(TAG," mForgotPasswordUrlLink response = " + fmResponse,IAUtilities.LOGS_SHOWN);
            return fmResponse;
        }

        @Override
        protected void onPostExecute(FMResponse fmResponse) {
            super.onPostExecute(fmResponse);
            if (!((AppCompatActivity)mContext).isFinishing()){
                mProgressDialog.dismiss();
            }
            if (fmResponse.isSuccess()){
                JSONObject jsonObject;
                try {
                    jsonObject = new JSONObject(fmResponse.getResponseData());
                    if (jsonObject.getString("status").equalsIgnoreCase("success")){
                        // success {"status":"success","message":"Password has been successfully updated."}
                        openDialog(mContext.getResources().getString(R.string.password_successfully_reset));
                    } else if (jsonObject.getString("status").equalsIgnoreCase("error")){
                        // failed {"status":"error","message":"Customer account not found"}
                        openDialog(mContext.getResources().getString(R.string.mail_id_not_registered));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                openDialog(mContext.getResources().getString(R.string.unable_to_reset_password));
            }
            mForgotPasswordLayout.setEnabled(true);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * called when login button is clicked
     */
    private void onLoginButtonClick() {
        FMCustomer mFMCustomer = new FMCustomer();

        mFMCustomer.setCustomerEmailId(mLoginUserEmailField.getText().toString());
        String mMD5PasswordString = getResources().getString(R.string.append_password_to_string) + mLoginUserPasswordField.getText().toString();
        String mMD5EncodedString = new IAUtilities().md5(mMD5PasswordString);
        IAUtilities.showingLogs(TAG, " MD5 result = " + mMD5EncodedString, IAUtilities.LOGS_SHOWN);
        mFMCustomer.setCustomerPassword(mMD5EncodedString);
        new LoginAsyncTask(mFMCustomer).execute();
    }

    /**
     * called to check login validation
     */
    private boolean isValidLogin() {
        //check login validataion
        if (new IAUtilities().isNetworkAvailable(getApplicationContext())) {
            boolean mEmailFlag = false;
            boolean mPasswordFlag = false;

            String mLoginEmail = mLoginUserEmailField.getText().toString();
            String mLoginPassword = mLoginUserPasswordField.getText().toString();
            mEmailFlag = new IAUtilities().validateEmailId(mLoginEmail, mContext);

            if (mLoginPassword.length() > 1){
                mPasswordFlag = true;
            }

            if (!mEmailFlag && !mPasswordFlag){
                openDialog(mContext.getResources().getString(R.string.please_enter_valid_username_or_password));
            }else{
                if (mLoginUserEmailField.getText().toString().isEmpty()){
                    openDialog(mContext.getResources().getString(R.string.please_enter_username));
                }else if(!mEmailFlag){
                    openDialog(mContext.getResources().getString(R.string.incorrect_username));
                } else if (!mPasswordFlag){
                    openDialog(mContext.getResources().getString(R.string.please_enter_password));
                }
            }
            return mEmailFlag && mPasswordFlag;
        } else{
            mLoginLayoutStartButton.setEnabled(true);
            return false;
        }
    }

    /**
     * called to open a popup message dialog
     * @param mMessageString the message to be shown
     */
    public void openDialog(final String mMessageString) {
        final Dialog dialog = new Dialog(this);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.default_dialog);
        TextView mMessageContentView = (TextView) dialog.findViewById(R.id.message_dialog_tv);
        mMessageContentView.setText(mMessageString);
        if (!((AppCompatActivity)mContext).isFinishing()){
            dialog.show();
        }

        IACustomButton mBackToDashboardButton = (IACustomButton) dialog.findViewById(R.id.dialog_done_btn);
        mBackToDashboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((AppCompatActivity)mContext).isFinishing()){
                    dialog.dismiss();
                }
            }
        });

    }

    /**
     *
     */
    private void goToOneMoreStepActivity() {
        setResult(RESULT_OK);
        finish();
    }

    /**
     * Login api called
     */
    private class LoginAsyncTask extends AsyncTask<Void, Void, FMResponse> {

        private ProgressDialog progressDialog;
        private FMCustomer mFMCustomer;

        public LoginAsyncTask(FMCustomer mFMCustomer) {
            this.mFMCustomer = mFMCustomer;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMLoginActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            if (!((AppCompatActivity)mContext).isFinishing()){
                progressDialog.show();
            }
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (mFMCustomer.getCustomerEmailId() != null && mFMCustomer.getCustomerPassword() != null) {
                    jsonObject.put("display", "full");
                    jsonObject.put("filter[email]", mFMCustomer.getCustomerEmailId());
                    jsonObject.put("filter[passwd]", mFMCustomer.getCustomerPassword());
                    jsonObject.put("output_format", "JSON");
                    jsonObject.put("ws_key", mContext.getResources().getString(R.string.ws_key));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse FMResponse = webConnection.getResponse(FMUrl.NEW_LOGIN_URL, jsonObject);
            IAUtilities.showingLogs(TAG, "LoginFmasResponse == " + FMResponse, IAUtilities.LOGS_SHOWN);

            return FMResponse;
        }

        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            if (!((AppCompatActivity)mContext).isFinishing()){
                progressDialog.dismiss();
            }

            final FMCustomer FMCustomer = new FMCustomer(mFMCustomer);
            if (response.isSuccess()) {
                if (response.getResponseData().contains("customers")) {
                    //go to next page
                    try {
                        JSONObject mJsonOffersListResponseObject = new JSONObject(response.getResponseData());
                        JSONArray mJsonResultObjectsArray = mJsonOffersListResponseObject.getJSONArray("customers");
                        if (mJsonResultObjectsArray != null) {
                            FMCustomer mFMCustomer = new FMCustomer();
                            mFMCustomer.loadData(mJsonResultObjectsArray.getJSONObject(0).toString());
                            new IAUtilities().setSharedPreferences(mContext,FMCustomer.getCustomerPassword(),"encryptedKey");
                            new IAUtilities().setSharedPreferences(mContext,mJsonResultObjectsArray.getJSONObject(0).toString(), "FMCustomer");
                            goToOneMoreStepActivity();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    openDialog(mContext.getResources().getString(R.string.the_email_address_or_the_password_you_entered_is_not_valid));
                    mLoginLayoutStartButton.setEnabled(true);
                }


            } else {
                //show error message
                openDialog(mContext.getResources().getString(R.string.failed_response));
            }
        }
    }


    /**
     * unused for now. Called to go to result activity for valid result
     */
    private void goToOnSuccessfulResponseActivity() {
        Intent intent = new Intent(FMLoginActivity.this, FMOnSuccessfulResponseActivity.class);
        new IAUtilities().setSharedPreferences(mContext, mFootResult, "FootResult");
        startActivity(intent);
    }

}

