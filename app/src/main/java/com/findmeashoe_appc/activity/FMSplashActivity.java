package com.findmeashoe_appc.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.findmeashoe_appc.R;

public class FMSplashActivity extends AppCompatActivity {
    private Boolean mIsBackPressed = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        initializeView();
    }

    /**
     * called to initialize the splash screen ui
     */
    private void initializeView(){

        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!mIsBackPressed){
                    goToStartingActivity();
                }
            }
        },5000);
    }

    /**
     * called to redirect the user to the starting screen
     */
    private void goToStartingActivity() {
        Intent intent = new Intent(FMSplashActivity.this, FMStartingActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        mIsBackPressed = true;
        super.onBackPressed();
    }
}
