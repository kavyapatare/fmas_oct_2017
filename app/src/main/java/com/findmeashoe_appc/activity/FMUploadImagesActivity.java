package com.findmeashoe_appc.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.adapters.FMImagesPagerAdapter;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * unused for now. Used FMUploadImagesRevisedActivity instead
 */
public class FMUploadImagesActivity extends AppCompatActivity {
    private static final String TAG = "FMUploadImagesActivity";
    private Toolbar mToolbar;
    private IACustomTextView mToolbarCustomerNameTextField;
    private LinearLayout mToolbarBackButton;
    private ViewPager mViewPager;
    private FMImagesPagerAdapter mFMImagesPagerAdapter;
    private ImageView mTopView;
    private ImageView mAngleView;
    private ImageView mSideView;
    private Context mContext;
    private IACustomButton mUploadImagesButton;
    private FMCustomer mCurrentFMCustomer;
    private Bitmap mTopViewBitmap, mAngleViewBitmap, mSideViewBitmap;
    private Bitmap mDummyBitmap;
    private String mFileStoragePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        setContentView(R.layout.activity_upload_images);
        initializeView();
    }

    private void initializeView() {

//        mFileStoragePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +
//                File.separator + "com.app.fmas.camera" + File.separator +
//                "Sample" + File.separator;

        mFileStoragePath = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                File.separator + "com.findmeashoe_appc.camera" + File.separator;

        Intent intent = getIntent();
        String mTopViewPath = intent.getStringExtra("image_1");
        String mAngleViewPath = intent.getStringExtra("image_2");
        String mSideViewPath = intent.getStringExtra("image_3");

        mTopView = (ImageView) findViewById(R.id.top_view_iv);
        mAngleView = (ImageView) findViewById(R.id.angle_view_iv);
        mSideView = (ImageView) findViewById(R.id.side_view_iv);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_preview);
        mUploadImagesButton = (IACustomButton) findViewById(R.id.upload_images_btn);
        mToolbarCustomerNameTextField = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);

        mCurrentFMCustomer = new FMCustomer();
        mCurrentFMCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));

        mToolbarCustomerNameTextField.setText("Hello " + mCurrentFMCustomer.getCustomerFirstName());
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        File mTopViewImageFile = new File(mTopViewPath);
        File mAngleViewImageFile = new File(mAngleViewPath);
        File mSideViewImageFile = new File(mSideViewPath);

//        File mTopViewImageFile = new  File(mFileStoragePath + "image_0.png");
//        File mAngleViewImageFile = new File(mFileStoragePath + "image_1.png");
//        File mSideViewImageFile = new File(mFileStoragePath + "image_2.png");

        mDummyBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.shoe);

        if (mTopViewImageFile.exists() && mAngleViewImageFile.exists() && mSideViewImageFile.exists()) {
            mTopViewBitmap = BitmapFactory.decodeFile(mTopViewImageFile.getAbsolutePath());
//            IAUtilities.showingLogs(TAG,"mTopViewImageFile = " + mTopViewImageFile, IAUtilities.LOGS_SHOWN);
            mTopView.setImageBitmap(mTopViewBitmap);

            mAngleViewBitmap = BitmapFactory.decodeFile(mAngleViewImageFile.getAbsolutePath());
//            IAUtilities.showingLogs(TAG,"mAngleViewImageFile = " + mAngleViewImageFile, IAUtilities.LOGS_SHOWN);
            mAngleView.setImageBitmap(mAngleViewBitmap);

            mSideViewBitmap = BitmapFactory.decodeFile(mSideViewImageFile.getAbsolutePath());
//            IAUtilities.showingLogs(TAG,"mSideViewImageFile = " + mSideViewImageFile, IAUtilities.LOGS_SHOWN);
            mSideView.setImageBitmap(mSideViewBitmap);
        }


        mUploadImagesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isValidCall()) {
                    new IAUtilities().hideKeyboard(v, mContext);
                    onUploadImagesButtonClick();
                }
            }
        });


//        mViewPager = (ViewPager) findViewById(R.id.customer_images_vp);

//        mFMImagesPagerAdapter = new FMImagesPagerAdapter(mTopView,mSideView,mAngleView);
//        mViewPager.setAdapter(mFMImagesPagerAdapter);

    }

    //add user api called
    private class AddCustomerDetailsAsyncTask extends AsyncTask<Void, Void, FMResponse> {

        private ProgressDialog progressDialog;
        private FMCustomer mFMCustomer;

        public AddCustomerDetailsAsyncTask(FMCustomer mFMCustomer) {
            this.mFMCustomer = mFMCustomer;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMUploadImagesActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (mFMCustomer.getCustomerEmailId() != null && mFMCustomer.getCustomerPassword() != null) {

                      /*Method: POST
Params:

d    tag = "client"
  topView
  topsideView
  sideView
  edgetopView
  edgetopsideView
  edgesideView
d    email
  device (in format : Model : Xiaomi HM 1S, Camera: 8 MP )
d    gender
d    age
d    username
d    appVersion  = v1.4.7 consumer

*topView, topsideView, sideView are base64 encoded string
* edgetopView, edgetopsideView, edgesideView are base64 static 1kb image*/
                    jsonObject.put("tag", "adduser_tag");
                    IAUtilities.showingLogs(TAG, "tag = " + jsonObject.get("tag"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("memberEmail", mFMCustomer.getCustomerEmailId());
                    IAUtilities.showingLogs(TAG, "memberEmail = " + jsonObject.getString("memberEmail"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("username", mFMCustomer.getCustomerEmailId().replaceAll("[!@#$%^&*()_+-=,./;':<>]",""));
                    IAUtilities.showingLogs(TAG, "username = " + jsonObject.getString("username"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("gender", mFMCustomer.getCustomerGender());
                    IAUtilities.showingLogs(TAG, "gender = " + jsonObject.getString("gender"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("age", 30);
                    IAUtilities.showingLogs(TAG, "age = " + jsonObject.getString("age"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("email", FMUrl.COMMON_VENDOR_EMAIL_ID);
                    IAUtilities.showingLogs(TAG, "email = " + jsonObject.getString("email"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("width", "100");
                    IAUtilities.showingLogs(TAG, "width = " + jsonObject.getString("width"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("length", " ");
                    IAUtilities.showingLogs(TAG, "length = " + jsonObject.getString("length"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("toeType", " ");
                    IAUtilities.showingLogs(TAG, "toeType = " + jsonObject.getString("toeType"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("bigToeAngle", " ");
                    IAUtilities.showingLogs(TAG, "bigToeAngle = " + jsonObject.getString("bigToeAngle"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("smallToeAngle", " ");
                    IAUtilities.showingLogs(TAG, "smallToeAngle = " + jsonObject.getString("smallToeAngle"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("inStepHeight", " ");
                    IAUtilities.showingLogs(TAG, "inStepHeight = " + jsonObject.getString("inStepHeight"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("ballHeight", " ");
                    IAUtilities.showingLogs(TAG, "ballHeight = " + jsonObject.getString("ballHeight"), IAUtilities.LOGS_SHOWN);
                    /*@"email" :@"adminappc@findmeashoe.com",
                                  @"width" : @"100",
                                  @"length" : @" ",
                                  @"toeType" : @" ",
                                  @"bigToeAngle" : @" ",
                                  @"smallToeAngle" : @" ",
                                  @"inStepHeight" : @" ",
                                  @"ballHeight" : @" "};*/

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse FMResponse = webConnection.postCustomerData(FMUrl.BASE_URL + "addusersfamily", jsonObject);
            IAUtilities.showingLogs(TAG, "Timestamp before request = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            IAUtilities.showingLogs(TAG, "OnUploadFmasResponse == " + FMResponse, IAUtilities.LOGS_SHOWN);

            return FMResponse;
        }


        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            IAUtilities.showingLogs(TAG, "Timestamp after response = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            final FMCustomer FMCustomer = new FMCustomer();
            if (response.isSuccess()) {
                //go to next page
                new UploadImagesAsyncTask(mFMCustomer).execute();
            } else {
                //show error message
                openDialog("Error Uploading Images");
            }

        }
    }

    //Upload image api called
    private class UploadImagesAsyncTask extends AsyncTask<Void, Void, FMResponse> {

        private ProgressDialog progressDialog;
        private FMCustomer mFMCustomer;

        public UploadImagesAsyncTask(FMCustomer mFMCustomer) {
            this.mFMCustomer = mFMCustomer;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMUploadImagesActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (mFMCustomer.getCustomerEmailId() != null && mFMCustomer.getCustomerPassword() != null) {

                      /*Method: POST
Params:

d    tag = "client"
  topView
  topsideView
  sideView
  edgetopView
  edgetopsideView
  edgesideView
d    email
  device (in format : Model : Xiaomi HM 1S, Camera: 8 MP )
d    gender
d    age
d    username
d    appVersion  = v1.4.7 consumer

*topView, topsideView, sideView are base64 encoded string
* edgetopView, edgetopsideView, edgesideView are base64 static 1kb image*/
                    jsonObject.put("tag", "client");
                    IAUtilities.showingLogs(TAG, "tag = " + jsonObject.get("tag"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("topView", new IAUtilities().bitmapToBase64(mTopViewBitmap));
                    IAUtilities.showingLogs(TAG, "topView = " + jsonObject.getString("topView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("topsideView", new IAUtilities().bitmapToBase64(mAngleViewBitmap));
                    IAUtilities.showingLogs(TAG, "topsideView = " + jsonObject.getString("topsideView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("sideView", new IAUtilities().bitmapToBase64(mSideViewBitmap));
                    IAUtilities.showingLogs(TAG, "sideView = " + jsonObject.getString("sideView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("edgetopView", new IAUtilities().bitmapToBase64(mDummyBitmap));
                    IAUtilities.showingLogs(TAG, "edgetopView = " + jsonObject.getString("edgetopView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("edgetopsideView", new IAUtilities().bitmapToBase64(mDummyBitmap));
                    IAUtilities.showingLogs(TAG, "edgetopsideView = " + jsonObject.getString("edgetopsideView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("edgesideView", new IAUtilities().bitmapToBase64(mDummyBitmap));
                    IAUtilities.showingLogs(TAG, "edgesideView = " + jsonObject.getString("edgetopsideView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("email", FMUrl.COMMON_VENDOR_EMAIL_ID);
                    IAUtilities.showingLogs(TAG, "email = " + jsonObject.getString("email"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("device", "Model : " + new IAUtilities().getDeviceName() + " , Camera: " + new IAUtilities().getCameraDetails() + " MP "  );
                    IAUtilities.showingLogs(TAG, "device = " + jsonObject.getString("device"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("gender", mFMCustomer.getCustomerGender());
                    IAUtilities.showingLogs(TAG, "gender = " + jsonObject.getString("gender"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("age", mFMCustomer.getCustomerAge());
                    IAUtilities.showingLogs(TAG, "age = " + jsonObject.getString("age"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("username", mFMCustomer.getCustomerEmailId().replaceAll("[!@#$%^&*()_+-=,./;':<>]",""));
                    IAUtilities.showingLogs(TAG, "username = " + jsonObject.getString("username"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("appVersion", "v1.4.7 consumer");
                    IAUtilities.showingLogs(TAG, "appVersion = " + jsonObject.getString("appVersion"), IAUtilities.LOGS_SHOWN);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse FMResponse = webConnection.postCustomerData(FMUrl.FOOT_MEASURE_URL, jsonObject);
            IAUtilities.showingLogs(TAG, "Timestamp before request = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            IAUtilities.showingLogs(TAG, "OnUploadFmasResponse == " + FMResponse, IAUtilities.LOGS_SHOWN);

            return FMResponse;
        }


        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            IAUtilities.showingLogs(TAG, "Timestamp after response = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            final FMCustomer FMCustomer = new FMCustomer();
            if (response.isSuccess()) {
                //go to next page
                if (response.getResponseData().contains("Invalid username")) {
                    openDialog("Invalid Username");
                } else if (response.getResponseData().contains("BigToeAngle") && response.getResponseData().contains("InStepHeight")) {
/*{"status": "OK", "BigToeAngle": "Straight", "InStepHeight": "86.33", "ToeType": "Half Tapered",
"SmallToeAngle": "Inward", "success": 1, "FootHeight": "262 mm", "FootWidth": "112 mm", "BallHeight": "0.0",
"API_URL": "http://target.findmeashoe.com/services/store-measurement/k@g.com/262/112/0/0/86/0/M/2/3/1/3"}*/
                    Intent intent = new Intent(FMUploadImagesActivity.this, FMOnSuccessfulResponseActivity.class);
                    try {
                        JSONObject jsonObject = new JSONObject(response.getResponseData());
                        intent.putExtra("big toe angle", jsonObject.getString("BigToeAngle"));
                        IAUtilities.showingLogs(TAG, "big toe angle = " + jsonObject.getString("BigToeAngle"), IAUtilities.LOGS_SHOWN);
                        intent.putExtra("in step height", jsonObject.getString("InStepHeight"));
                        intent.putExtra("Toe Type", jsonObject.getString("ToeType"));
                        intent.putExtra("Small Toe Angle", jsonObject.getString("SmallToeAngle"));
                        intent.putExtra("Foot Height", jsonObject.getString("FootHeight"));
                        intent.putExtra("Foot Width", jsonObject.getString("FootWidth"));
                        intent.putExtra("Ball Height", jsonObject.getString("BallHeight"));
                        startActivity(intent);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    FMCustomer.loadData(response.getResponseData());

//                    goToMainActivity();
//                    finish();
                }
            } else {
                //show error message
                openDialog("Error Uploading Images");
            }

        }
    }

    //called when login button click
    private void onUploadImagesButtonClick() {
        FMCustomer mFMCustomer = new FMCustomer(mCurrentFMCustomer);
//        new UploadImagesAsyncTask(mFMCustomer).execute();
        new AddCustomerDetailsAsyncTask(mFMCustomer).execute();
    }

    //check login validation
    private boolean isValidCall() {
        //check sign up validation
        return new IAUtilities().isNetworkAvailable(getApplicationContext());

    }

    public void openDialog(final String mMessageString) {
        final Dialog dialog = new Dialog(this); // context, this etc.
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.default_dialog);
        TextView mMessageContentView = (TextView) dialog.findViewById(R.id.message_dialog_tv);
        mMessageContentView.setText(mMessageString);
        dialog.show();
        IACustomButton mBackToDashboardButton = (IACustomButton) dialog.findViewById(R.id.dialog_done_btn);
        mBackToDashboardButton.setVisibility(View.GONE);
        // Hide after some seconds
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (dialog.isShowing()) {
                    IAUtilities.showingLogs(TAG, " mMessageString = " + mMessageString, IAUtilities.LOGS_SHOWN);
                    dialog.dismiss();
                }
            }
        };

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                handler.removeCallbacks(runnable);
            }
        });

        handler.postDelayed(runnable, 2000);
//        mBackToDashboardButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
////                finish();
//            }
//        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
