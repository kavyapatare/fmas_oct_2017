package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.adapters.FMProductsListAdapter;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMProduct;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.FMXmlParser;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class FMShowProductsActivity extends AppCompatActivity implements FMProductsListAdapter.FMProductsListAdapterInterface {
    private static final String TAG = "FMShowProductsActivity";
    private static final int MAX_CPU_CORES = Runtime.getRuntime().availableProcessors();
    private static final int MAX_THREADS = MAX_CPU_CORES * 2;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private Context mContext;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private ArrayList<FMProduct> mProductsList;
    private FMCustomer mCurrentCustomer;
    private FMProductsListAdapter mFMProductsListAdapter;
    private ListView mProductsListView;
    private ProductsThreadPoolExecutor mExecutor;
    private boolean mIsListReversed = true;
    private ImageView mFitSortToggleButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_products);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the show products list view screen ui
     */
    private void initializeView() {
        final Intent intent = getIntent();
        mProductsList = (ArrayList<FMProduct>) intent.getSerializableExtra("Selected Category");
        mCurrentCustomer = new FMCustomer();
        mCurrentCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));
        mToolbar = (Toolbar) findViewById(R.id.toolbar_see_products);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mProductsListView = (ListView) findViewById(R.id.products_lv);
        mFitSortToggleButton = (ImageView) findViewById(R.id.fit_sort_btn_iv);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        if (!mCurrentCustomer.getCustomerEmailId().equalsIgnoreCase("")){
            mToolBarCustomerNameTextView.setText("Hello " + mCurrentCustomer.getCustomerFirstName() + " !");
        }

        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });

        mFMProductsListAdapter = new FMProductsListAdapter(mProductsList, mContext, mCurrentCustomer);
        mProductsListView.setAdapter(mFMProductsListAdapter);

        mFitSortToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mIsListReversed){
                    mFitSortToggleButton.setImageResource(R.drawable.fit_up_arrow);
                } else {
                    mFitSortToggleButton.setImageResource(R.drawable.fit_down_arrow);
                }
                mIsListReversed = !mIsListReversed;
                Collections.reverse(mProductsList);
                mFMProductsListAdapter = new FMProductsListAdapter(mProductsList, mContext, mCurrentCustomer);
                mProductsListView.setAdapter(mFMProductsListAdapter);
            }
        });

        mExecutor = new ProductsThreadPoolExecutor(MAX_CPU_CORES, MAX_THREADS, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(MAX_THREADS, true));

        for (int mCount = 0;mCount < mProductsList.size();mCount++){
            final int finalMCount = mCount;

            mExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    new GetImageAsyncTask(mProductsList.get(finalMCount)).execute();
                }
            });
        }

        mProductsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent1 = new Intent(FMShowProductsActivity.this,FMShowProductDetailsActivity.class);
                intent1.putExtra("products",mProductsList);
                intent1.putExtra("current",position);
                startActivity(intent1);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * used to create a Thread pool to load the images efficiently
     */
    private class ProductsThreadPoolExecutor extends ThreadPoolExecutor {
        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        }

        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
        }

        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
        }

        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        }

        @Override
        protected void beforeExecute(Thread t, Runnable r) {
            super.beforeExecute(t, r);

        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
            mFMProductsListAdapter.notifyDataSetChanged();
        }
    }


    /**
     * used to fetch the product image url and load the image
     */
    private class GetImageAsyncTask extends AsyncTask<Void,Void,FMResponse> {
        private FMProduct fmProduct;

        public GetImageAsyncTask(FMProduct fmProduct){
            this.fmProduct = fmProduct;
        }

        @Override
        protected FMResponse doInBackground(Void... voids) {
            FMWebConnection webConnection = new FMWebConnection();
            String mResultUrl = fmProduct.getProductImageUrlLink();
            FMResponse mImageResponse = webConnection.getResponse(mResultUrl);
            if (mImageResponse.isSuccess()) {
                IAUtilities.showingLogs(TAG, "mImageResponse = " + mImageResponse.getResponseData() + "?&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H", IAUtilities.LOGS_SHOWN);

                FMXmlParser fmXmlParser = new FMXmlParser();
                String mImageUrl = fmXmlParser.parse(mImageResponse.getResponseData());
                fmProduct.setProductImageUrl(mImageUrl);
            }
            return mImageResponse;
        }

        @Override
        protected void onPostExecute(FMResponse mImageResponse) {
            super.onPostExecute(mImageResponse);
            IAUtilities.showingLogs(TAG, "mImageResponse = " + mImageResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
            mFMProductsListAdapter.notifyDataSetChanged();
        }
    }

    /**
     * called when the try me button is clicked
     * @param fmProduct
     */
    @Override
    public void onTryMeButtonClick(FMProduct fmProduct) {
        goToShowFittingDetailsActivity(fmProduct);
    }

    /**
     * called to redirect the user to the product fitting details screen
     * @param fmProduct
     */
    private void goToShowFittingDetailsActivity(FMProduct fmProduct){
        Intent intent = new Intent(FMShowProductsActivity.this,FMShowFittingDetailsActivity.class);
        intent.putExtra("Current Product",fmProduct);
        startActivity(intent);
    }
}
