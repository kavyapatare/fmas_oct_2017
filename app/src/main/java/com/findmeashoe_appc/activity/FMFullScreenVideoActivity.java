package com.findmeashoe_appc.activity;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMUrl;

public class FMFullScreenVideoActivity extends AppCompatActivity {
    private static final String TAG = FMFullScreenVideoActivity.class.getSimpleName();
    private VideoView mSampleVideoView;
    private MediaController mSampleMediaController;
    private int stopPosition = 0;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_video);
        initializeView();
    }

    /**
     * called to initialize the full screen video ui
     */
    private void initializeView() {
        mSampleVideoView = (VideoView) findViewById(R.id.full_screen_video_vv);
        mProgressBar = (ProgressBar) findViewById(R.id.video_loading_pb);

        if (!getIntent().getStringExtra("url").isEmpty()) {
            mSampleVideoView.setVideoPath(getIntent().getStringExtra("url"));
        } else {

        }
        Log.i(TAG, "buffer percentage = " + mSampleVideoView.getBufferPercentage());

        mSampleMediaController = new MediaController(this, false);

        mSampleMediaController.setAnchorView(mSampleVideoView);
        mSampleVideoView.setMediaController(mSampleMediaController);

        mSampleVideoView.setOnPreparedListener(new
                                                       MediaPlayer.OnPreparedListener() {
                                                           @Override
                                                           public void onPrepared(MediaPlayer mp) {
                                                               Log.i(TAG, "Duration = " +
                                                                       mSampleVideoView.getDuration());
                                                               mp.setOnBufferingUpdateListener(new MediaPlayer.OnBufferingUpdateListener() {
                                                                   @Override
                                                                   public void onBufferingUpdate(MediaPlayer mp, int percent) {
                                                                       if (mp.isPlaying()) {
                                                                           mProgressBar.setVisibility(View.GONE);
                                                                       } else {
                                                                           mProgressBar.setVisibility(View.VISIBLE);
                                                                       }
                                                                   }
                                                               });
                                                           }
                                                       });
        mSampleVideoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                setResult(FMUrl.VIDEO_COMPLETE);
                finish();
            }
        });
        Log.i(TAG, "seek backward = " + mSampleVideoView.canSeekBackward());
        mSampleVideoView.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                return false;
            }
        });
        mSampleVideoView.start();
    }


    @Override
    public void onPause() {
        Log.d(TAG, "onPause called");
        super.onPause();
        stopPosition = mSampleVideoView.getCurrentPosition(); //stopPosition is an int
        mSampleVideoView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume called");
        mSampleVideoView.seekTo(stopPosition);
        mSampleVideoView.start(); //Or use resume() if it doesn't work.
    }
}
