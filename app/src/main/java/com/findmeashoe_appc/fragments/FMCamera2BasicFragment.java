package com.findmeashoe_appc.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.ExifInterface;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.findmeashoe_appc.activity.FMUploadImagesRevisedActivity;
import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ui.FMAutoFitTextureView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

/**
 *  temporarily unused due to very less percentage of
 *  Camera 2 api capable hardware devices in the market
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class FMCamera2BasicFragment extends Fragment
        implements View.OnClickListener/*, FragmentCompat.OnRequestPermissionsResultCallback*/, Serializable {

    /**
     * Conversion from screen rotation to JPEG orientation.
     */
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    private static final int REQUEST_CAMERA_PERMISSION = 1;
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION = 2;
    private static final String FRAGMENT_CAMERA_PERMISSION_DIALOG = "cameradialog";
    private static final String FRAGMENT_MEMORY_WRITE_PERMISSION_DIALOG = "writedialog";

    static {
        ORIENTATIONS.append(Surface.ROTATION_0, 90);
        ORIENTATIONS.append(Surface.ROTATION_90, 0);
        ORIENTATIONS.append(Surface.ROTATION_180, 270);
        ORIENTATIONS.append(Surface.ROTATION_270, 180);
    }

    /**
     * Tag for the {@link Log}.
     */
    private static final String TAG = "FMCamera2BasicFragment";

    /**
     * Camera state: Showing camera preview.
     */
    private static final int STATE_PREVIEW = 0;

    /**
     * Camera state: Waiting for the focus to be locked.
     */
    private static final int STATE_WAITING_LOCK = 1;

    /**
     * Camera state: Waiting for the exposure to be precapture state.
     */
    private static final int STATE_WAITING_PRECAPTURE = 2;

    /**
     * Camera state: Waiting for the exposure state to be something other than precapture.
     */
    private static final int STATE_WAITING_NON_PRECAPTURE = 3;

    /**
     * Camera state: Picture was taken.
     */
    private static final int STATE_PICTURE_TAKEN = 4;
    /**
     * Max preview width that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_WIDTH = 1920;

    /**
     * Max preview height that is guaranteed by Camera2 API
     */
    private static final int MAX_PREVIEW_HEIGHT = 1080;

    /**
     * {@link TextureView.SurfaceTextureListener} handles several lifecycle events on a
     * {@link TextureView}.
     */
    private final TextureView.SurfaceTextureListener mSurfaceTextureListener
            = new TextureView.SurfaceTextureListener() {

        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture texture, int width, int height) {
            openCamera(width, height);
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture texture, int width, int height) {
            configureTransform(width, height);
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture texture) {
            return true;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture texture) {
        }

    };

    /**
     * ID of the current {@link CameraDevice}.
     */
    private String mCameraId;

    /**
     * An {@link FMAutoFitTextureView} for camera preview.
     */
    private FMAutoFitTextureView mTextureView;

    /**
     * A {@link CameraCaptureSession } for camera preview.
     */
    private CameraCaptureSession mCaptureSession;

    /**
     * A reference to the opened {@link CameraDevice}.
     */
    private CameraDevice mCameraDevice;

    /**
     * The {@link Size} of camera preview.
     */
    private Size mPreviewSize;

    /**
     * {@link CameraDevice.StateCallback} is called when {@link CameraDevice} changes its state.
     */
    private final CameraDevice.StateCallback mStateCallback = new CameraDevice.StateCallback() {

        @Override
        public void onOpened(@NonNull CameraDevice cameraDevice) {
            // This method is called when the camera is opened.  We start camera preview here.
            IAUtilities.showingLogs(TAG,"CameraDevice is opened", IAUtilities.LOGS_SHOWN);
            mIsCameraClosed = false;
            mCameraOpenCloseLock.release();
            mCameraDevice = cameraDevice;
            createCameraPreviewSession();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            IAUtilities.showingLogs(TAG,"CameraDevice is disconnected", IAUtilities.LOGS_SHOWN);
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int error) {
            IAUtilities.showingLogs(TAG,"CameraDevice is interrupted by error", IAUtilities.LOGS_SHOWN);
            mCameraOpenCloseLock.release();
            cameraDevice.close();
            mCameraDevice = null;
            Activity activity = getActivity();
            if (null != activity) {
                activity.finish();
            }
        }

        @Override
        public void onClosed(CameraDevice camera) {
            super.onClosed(camera);
            IAUtilities.showingLogs(TAG, "CameraDevice is closed", IAUtilities.LOGS_SHOWN);
            mIsCameraClosed = true;

        }
    };

    /**
     * An additional thread for running tasks that shouldn't block the UI.
     */
    private HandlerThread mBackgroundThread;

    /**
     * A {@link Handler} for running tasks in the background.
     */
    private Handler mBackgroundHandler;

    /**
     * An {@link ImageReader} that handles still image capture.
     */
    private ImageReader mImageReader;

    /**
     * This is the output file for our picture.
     */
    private File mFile;
    private File mTopViewFile, mAngleViewFile, mSideViewFile;
    private Bitmap mTopViewBitmap, mAngleViewBitmap, mSideViewBitmap, mCroppedBitmapBmp;

    private FMCamera2BasicFragment mCurrentFragmentContext = this;
    /**
     * This a callback object for the {@link ImageReader}. "onImageAvailable" will be called when a
     * still image is ready to be saved.
     */
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener
            = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {
            mBackgroundHandler.post(new ImageSaver(reader.acquireNextImage(), mFile, mCurrentFragmentContext));
        }
    };

    /**
     * {@link CaptureRequest.Builder} for the camera preview
     */
    private CaptureRequest.Builder mPreviewRequestBuilder;

    /**
     * {@link CaptureRequest} generated by {@link #mPreviewRequestBuilder}
     */
    private CaptureRequest mPreviewRequest;

    /**
     * The current state of camera state for taking pictures.
     *
     * @see #mCaptureCallback
     */
    private int mState = STATE_PREVIEW;

    /**
     * A {@link Semaphore} to prevent the app from exiting before closing the camera.
     */
    private Semaphore mCameraOpenCloseLock = new Semaphore(1);

    /**
     * A {@link CameraCaptureSession.CaptureCallback} that handles events related to JPEG capture.
     */
    private CameraCaptureSession.CaptureCallback mCaptureCallback
            = new CameraCaptureSession.CaptureCallback() {

        private void process(CaptureResult result) {
            switch (mState) {
                case STATE_PREVIEW: {
                    // We have nothing to do when the camera preview is working normally.
//                    IAUtilities.showingLogs(TAG,"mCaptureCallback is preview.",IAUtilities.LOGS_SHOWN);
                    break;
                }
                case STATE_WAITING_LOCK: {
                    Integer afState = result.get(CaptureResult.CONTROL_AF_STATE);
                    if (afState == null) {
                        captureStillPicture();
                    } else if (CaptureResult.CONTROL_AF_STATE_FOCUSED_LOCKED == afState ||
                            CaptureResult.CONTROL_AF_STATE_NOT_FOCUSED_LOCKED == afState) {
                        // CONTROL_AE_STATE can be null on some devices
                        Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                        if (aeState == null ||
                                aeState == CaptureResult.CONTROL_AE_STATE_CONVERGED) {
                            mState = STATE_PICTURE_TAKEN;
//                            IAUtilities.showingLogs(TAG,"mCaptureCallback is waiting lock for still capture.",IAUtilities.LOGS_SHOWN);
                            captureStillPicture();
                        } else {
//                            IAUtilities.showingLogs(TAG,"mCaptureCallback is waiting lock for precapture sequence.",IAUtilities.LOGS_SHOWN);
                            runPrecaptureSequence();
                        }
                    }
                    break;
                }
                case STATE_WAITING_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null ||
                            aeState == CaptureResult.CONTROL_AE_STATE_PRECAPTURE ||
                            aeState == CaptureRequest.CONTROL_AE_STATE_FLASH_REQUIRED) {
                        mState = STATE_WAITING_NON_PRECAPTURE;
//                        IAUtilities.showingLogs(TAG,"mCaptureCallback is waiting precapture.",IAUtilities.LOGS_SHOWN);
                    }
                    break;
                }
                case STATE_WAITING_NON_PRECAPTURE: {
                    // CONTROL_AE_STATE can be null on some devices
                    Integer aeState = result.get(CaptureResult.CONTROL_AE_STATE);
                    if (aeState == null || aeState != CaptureResult.CONTROL_AE_STATE_PRECAPTURE) {
                        mState = STATE_PICTURE_TAKEN;
//                        IAUtilities.showingLogs(TAG,"mCaptureCallback is waiting non precapture for capture still image.",IAUtilities.LOGS_SHOWN);
                        captureStillPicture();
                    }
                    break;
                }
            }
        }

        @Override
        public void onCaptureProgressed(@NonNull CameraCaptureSession session,
                                        @NonNull CaptureRequest request,
                                        @NonNull CaptureResult partialResult) {
            process(partialResult);
        }

        @Override
        public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                       @NonNull CaptureRequest request,
                                       @NonNull TotalCaptureResult result) {
            process(result);
        }

    };

    /**
     * Shows a {@link Toast} on the UI thread.
     *
     * @param text The message to show
     */
    private void showToast(final String text) {
        final Activity activity = getActivity();
        if (activity != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(activity, text, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

//    /**
//     * Given {@code choices} of {@code Size}s supported by a camera, chooses the smallest one whose
//     * width and height are at least as large as the respective requested values, and whose aspect
//     * ratio matches with the specified value.
//     *
//     * @param choices     The list of sizes that the camera supports for the intended output class
//     * @param width       The minimum desired width
//     * @param height      The minimum desired height
//     * @param aspectRatio The aspect ratio
//     * @return The optimal {@code Size}, or an arbitrary one if none were big enough
//     */
//    private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
//        // Collect the supported resolutions that are at least as big as the preview Surface
//        List<Size> bigEnough = new ArrayList<>();
//        int w = aspectRatio.getWidth();
//        int h = aspectRatio.getHeight();
//        IAUtilities.showingLogs(TAG, "w = " + w, IAUtilities.LOGS_SHOWN);
//        IAUtilities.showingLogs(TAG, "h = " + h, IAUtilities.LOGS_SHOWN);
//        for (Size option : choices) {
//            if (option.getHeight() == option.getWidth() * h / w &&
//                    option.getWidth() >= width && option.getHeight() >= height) {
//                bigEnough.add(option);
//                IAUtilities.showingLogs(TAG, " optimal width = " + option.getWidth() + " optimal height = " + option.getHeight(), IAUtilities.LOGS_SHOWN);
//            }
//            if (option.getHeight() == option.getWidth() * 9 / 16) {
//                bigEnough.add(option);
//                IAUtilities.showingLogs(TAG, " optimal width = " + option.getWidth() + " optimal height = " + option.getHeight(), IAUtilities.LOGS_SHOWN);
//            }
//        }
//
////        for (Size option : choices) {
////            IAUtilities.showingLogs(TAG,"width = " + option.getWidth() + " height = " + option.getHeight(), IAUtilities.LOGS_SHOWN);
////            if (option.getHeight() == option.getWidth() * 9/16 ) {
////                bigEnough.add(option);
////            }
////        }
//
//        // Pick the smallest of those, assuming we found any
//        if (bigEnough.size() > 0) {
//            return Collections.max(bigEnough, new CompareSizesByArea());
//        } else {
//            Log.e(TAG, "Couldn't find any suitable preview size");
//            return choices[0];
//        }
////        return choices[1];
//    }



    private static Size chooseOptimalSize(Size[] choices, int width, int height, Size aspectRatio) {
        // Collect the supported resolutions that are at least as big as the preview Surface
        List<Size> bigEnough = new ArrayList<>();
        int w = aspectRatio.getWidth();
        int h = aspectRatio.getHeight();
        for (Size option : choices) {
            if (option.getHeight() == option.getWidth() * h / w &&
                    option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
                IAUtilities.showingLogs(TAG, " optimal width = " + option.getWidth() + " optimal height = " + option.getHeight(), IAUtilities.LOGS_SHOWN);
            }
        }

        // Pick the smallest of those, assuming we found any
        if (bigEnough.size() > 0) {
            Size size = Collections.min(bigEnough, new CompareSizesByArea());
//            cameraHeight = size.getHeight();
//            cameraWidth = size.getWidth();
            return Collections.min(bigEnough, new CompareSizesByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size");

            return choices[0];
        }
    }



    public static FMCamera2BasicFragment newInstance() {
        return new FMCamera2BasicFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_camera2_basic, container, false);
    }

    private ImageView mCaptureImageButton;
    private LinearLayout mBackButton;
    private LinearLayout mMenuButton;
    private ImageView mCaptureTopViewButton;
    private ImageView mCaptureAngleViewButton;
    private ImageView mCaptureSideViewButton;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    //    private FMCustomer mCurrentCustomer;
    private RelativeLayout mCropViewLayout;
    private String mFileStoragePath;
    private int mCurrentButtonIndex;
    private boolean mRetakeFlagEnabled;
    private int currentApiVersion;
    private CameraManager manager;
    private boolean mIsCameraClosed = true;

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(getActivity());
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        view.findViewById(R.id.texture).setOnClickListener(this);
        mTextureView = (FMAutoFitTextureView) view.findViewById(R.id.texture);
        mCaptureImageButton = (ImageView) view.findViewById(R.id.capture_image_btn);
        mCaptureTopViewButton = (ImageView) view.findViewById(R.id.top_view_btn);
        mCaptureAngleViewButton = (ImageView) view.findViewById(R.id.angle_view_btn);
        mCaptureSideViewButton = (ImageView) view.findViewById(R.id.side_view_btn);
//        mCurrentCustomer = new IAUtilities().getSharedPreferences(getActivity(), "FMCustomer");
        mCropViewLayout = (RelativeLayout) view.findViewById(R.id.crop_view_rl);
        mMenuButton = (LinearLayout) view.findViewById(R.id.menu_button_ll);
//        mFileStoragePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) +
//                File.separator + "com.app.fmas.camera" + File.separator +
//                mCurrentCustomer.getCustomerUsername() + mCurrentCustomer.getCustomerEmailId() + File.separator;
        mFileStoragePath = getActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                File.separator + "com.findmeashoe_appc.camera" + File.separator;
        setWidthAccToHeight();
        mCaptureImageButton.setOnClickListener(this);
        mCaptureTopViewButton.setEnabled(false);
        mCaptureAngleViewButton.setEnabled(false);
        mCaptureSideViewButton.setEnabled(false);

        mBackButton = (LinearLayout) view.findViewById(R.id.fragment_camera2_back_button_ll);

//        mCaptureImageButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                captureImage();
//            }
//        });

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        mMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });

        mCaptureTopViewButton.setOnClickListener(this);
        mCaptureAngleViewButton.setOnClickListener(this);
        mCaptureSideViewButton.setOnClickListener(this);

        if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(0))) {
            mCurrentButtonIndex = 0;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);

        } else if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(1))) {
            mCurrentButtonIndex = 1;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(true);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);

        } else if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(2))) {
            mCurrentButtonIndex = 2;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(true);
            mCaptureSideViewButton.setEnabled(false);
        } else if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(3))) {
            mCurrentButtonIndex = 3;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(true);
        } else if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(4))) {
            mCurrentButtonIndex = 1;
            mRetakeFlagEnabled = true;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(true);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
        } else if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(5))) {
            mCurrentButtonIndex = 2;
            mRetakeFlagEnabled = true;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(true);
            mCaptureSideViewButton.setEnabled(false);
        } else if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(6))) {
            mCurrentButtonIndex = 3;
            mRetakeFlagEnabled = true;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(true);
        } else if (new IAUtilities().getSharedPreferences(getActivity(), "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(7))) {
            mCurrentButtonIndex = 3;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.next_button_selector));
            mCaptureTopViewButton.setEnabled(true);
            mCaptureAngleViewButton.setEnabled(true);
            mCaptureSideViewButton.setEnabled(true);
        }
        manager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
    }

        @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mFile = new File(getActivity().getExternalFilesDir(null), "pic.jpg");
    }


    private File openFileForImage(int type) {
        File imageDirectory = null;
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            imageDirectory = new File(
                    mFileStoragePath);
            if (!imageDirectory.exists() && !imageDirectory.mkdirs()) {
                imageDirectory = null;
            } else {
                return new File(imageDirectory.getPath() +
                        File.separator + "image_" +
                        type + ".png");
            }
        }

        return null;
    }

    private  void setWindowWithoutStatusBar(){
        currentApiVersion = Build.VERSION.SDK_INT;
        // This work only for android 4.4+
        if(currentApiVersion >= Build.VERSION_CODES.KITKAT)
        {

            final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    /*| View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION*/
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    /*| View.SYSTEM_UI_FLAG_HIDE_NAVIGATION*/
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;


            getActivity().getWindow().getDecorView().setSystemUiVisibility(flags);

//             Code below is to handle presses of Volume up or Volume down.
//             Without this, after pressing volume buttons, the navigation bar will
//             show up and won't hide
            final View decorView = getActivity().getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

    }
    @Override
    public void onResume() {
        super.onResume();
        startBackgroundThread();
        setWindowWithoutStatusBar();
        setWidthAccToHeight();
        // When the screen is turned off and turned back on, the SurfaceTexture is already
        // available, and "onSurfaceTextureAvailable" will not be called. In that case, we can open
        // a camera and start preview from here (otherwise, we wait until the surface is ready in
        // the SurfaceTextureListener).
        if (mTextureView.isAvailable()) {
            IAUtilities.showingLogs(TAG, " mTextureView = " + mTextureView.getWidth() + " " + mTextureView.getHeight(), IAUtilities.LOGS_SHOWN);
//            if (mTextureView.getWidth() > mTextureView.getHeight()){
//                openCamera(mTextureView.getWidth(), mTextureView.getHeight());
//            }else{
//                openCamera(mTextureView.getHeight(), mTextureView.getWidth());
//            }
            setUpCameraOutputs(mTextureView.getWidth(), mTextureView.getHeight());
            configureTransform(mTextureView.getWidth(), mTextureView.getHeight());
        } else {
            mTextureView.setSurfaceTextureListener(mSurfaceTextureListener);
        }
    }

    @Override
    public void onPause() {
        closeCamera();
        stopBackgroundThread();
        super.onPause();
    }

    private void requestCameraPermission() {
//        if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)) {
//            new ConfirmationCameraDialog().show(getChildFragmentManager(), FRAGMENT_CAMERA_PERMISSION_DIALOG);
//        } else {
//            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA},
//                    REQUEST_CAMERA_PERMISSION);
//        }
    }

    private void requestMemoryWritePermission() {
//        if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
//            new ConfirmationMemoryWriteDialog().show(getChildFragmentManager(), FRAGMENT_MEMORY_WRITE_PERMISSION_DIALOG);
//        } else {
//            FragmentCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                    REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION);
//        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA_PERMISSION:
                if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    ErrorDialog.newInstance(getString(R.string.request_camera_permission))
                            .show(getChildFragmentManager(), FRAGMENT_CAMERA_PERMISSION_DIALOG);
                } else {
                    Toast.makeText(getActivity(), "Camera Permission not granted", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
                break;

            case REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION:
                if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    ErrorDialog.newInstance(getString(R.string.request_memory_write_permission))
                            .show(getChildFragmentManager(), FRAGMENT_MEMORY_WRITE_PERMISSION_DIALOG);
                } else {
                    Toast.makeText(getActivity(), "Memory Write Permission not granted", Toast.LENGTH_SHORT).show();
                    getActivity().finish();
                }
                break;

            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                break;
        }
    }

//    /**
//     * Sets up member variables related to camera.
//     *
//     * @param width  The width of available size for camera preview
//     * @param height The height of available size for camera preview
//     */
//    private void setUpCameraOutputs(int width, int height) {
//        Activity activity = getActivity();
//        try {
//            for (String cameraId : manager.getCameraIdList()) {
//                CameraCharacteristics characteristics
//                        = manager.getCameraCharacteristics(cameraId);
//
//                // We don't use a front facing camera in this sample.
//                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
//                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
//                    continue;
//                }
//
//                StreamConfigurationMap map = characteristics.get(
//                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
//                if (map == null) {
//                    continue;
//                }
//
//                // For still image captures, we use the largest available size.
//                DisplayMetrics metrics = new DisplayMetrics();
//                getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//                Size optimalSize;
////                Size optimalSize;
//                if (metrics.widthPixels >= 1440 && metrics.heightPixels >= 1080) {
//                    optimalSize = new Size(1920,1080);
//
////htc one m7 preview output sizes = [1920x1088, 1440x1088, 1456x832, 1088x1088, 1280x720, 960x720, 960x544, 720x720, 800x480, 768x464, 720x480, 768x432, 640x480, 544x544, 576x432, 640x384, 640x368, 480x480, 480x320, 384x288, 352x288, 320x240, 240x160, 176x144]
//// htc one m7 image sizes = [2688x1520, 2592x1456, 2048x1520, 2048x1216, 2048x1152, 1920x1088, 1600x1200, 1600x896, 1520x1520, 1456x1088, 1456x880, 1456x832, 1440x1088, 1280x960, 1280x768, 1280x720, 1024x768, 1088x1088, 800x600, 800x480, 720x720, 640x480, 640x384, 640x368, 480x480, 352x288, 320x240, 176x144]
////                    mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
////                            width, height, optimalSize);
//                    mPreviewSize = optimalSize;
//                    IAUtilities.showingLogs(TAG," mCamera preview size = " + mPreviewSize.getWidth() + " "
//                     + mPreviewSize.getHeight(), IAUtilities.LOGS_SHOWN);
//                } else {
////                    optimalSize = new Size(metrics.widthPixels,metrics.heightPixels);
////                    mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
////                            width, height, optimalSize);
//                    optimalSize = new Size(10,70);
//                    CameraManager cameraManager = (CameraManager) getActivity().getSystemService(Context.CAMERA_SERVICE);
//                    String mCameraId = cameraManager.getCameraIdList()[0];
//                    CameraCharacteristics cc = cameraManager.getCameraCharacteristics(mCameraId);
//                    StreamConfigurationMap streamConfigs = cc.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
//                    Size[] jpegSizes = streamConfigs.getOutputSizes(ImageFormat.JPEG);
//
//                    for (int i=0;i<jpegSizes.length;i++){
//                        IAUtilities.showingLogs(TAG,"camera sizes width = " + jpegSizes[i].getWidth() + "camera sizes height = " + jpegSizes[i].getHeight(), IAUtilities.LOGS_SHOWN);
//                        if (jpegSizes[i].getWidth() == (metrics.widthPixels + getNavBarHeight(getActivity()))
//                                && jpegSizes[i].getHeight() == metrics.heightPixels){
//                            optimalSize = jpegSizes[i];
//                            break;
//                        }
//                    }
//                    if (metrics.widthPixels == 1196){
//                        optimalSize = new Size(1280,960);
//                    } else if (metrics.widthPixels == 897){
//                        optimalSize = new Size(960,720);
//                    }
////                    optimalSize = new Size(1280, 720);
////                    mPreviewSize = new Size(1280, 720);
//
//                    mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class),
//                            width, height, optimalSize);
//
//                    IAUtilities.showingLogs(TAG," mCamera preview size = " + mPreviewSize.getWidth() + " "
//                            + mPreviewSize.getHeight(), IAUtilities.LOGS_SHOWN);
//                }
//
//
//                mImageReader = ImageReader.newInstance(optimalSize.getWidth(), optimalSize.getHeight(),
//                        ImageFormat.JPEG, /*maxImages*/1);
//                mImageReader.setOnImageAvailableListener(
//                        mOnImageAvailableListener, mBackgroundHandler);
//
//                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
//                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
//                // garbage capture data.
//
//
//                IAUtilities.showingLogs(TAG, "mPreviewSize.height = " + mPreviewSize.getHeight() + " mPreviewSize.width = " + mPreviewSize.getWidth(), IAUtilities.LOGS_SHOWN);
//
//                // We fit the aspect ratio of TextureView to the size of preview we picked.
//                int orientation = getResources().getConfiguration().orientation;
//                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
//                    mTextureView.setAspectRatio(
//                            mPreviewSize.getWidth(), mPreviewSize.getHeight());
//                    IAUtilities.showingLogs(TAG, "mTextureView.height = " + mTextureView.getHeight() + " mTextureView.width = " + mTextureView.getWidth(), IAUtilities.LOGS_SHOWN);
//
//                } else {
//                    mTextureView.setAspectRatio(
//                            mPreviewSize.getHeight(), mPreviewSize.getWidth());
//                }
//
//                mCameraId = cameraId;
//                return;
//            }
//        } catch (CameraAccessException e) {
//            e.printStackTrace();
//        } catch (NullPointerException e) {
//            // Currently an NPE is thrown when the Camera2API is used but not supported on the
//            // device this code runs.
//            ErrorDialog.newInstance(getString(R.string.camera_error))
//                    .show(getChildFragmentManager(), FRAGMENT_CAMERA_PERMISSION_DIALOG);
//        }
//    }

    private double calculateAspectRatio(Size mAspectRatioSize){
        IAUtilities.showingLogs(TAG,"mAspectRatioSize width = " + mAspectRatioSize.getWidth()
                + " height = " + mAspectRatioSize.getHeight(),IAUtilities.LOGS_SHOWN);
        double mAspectRatio = (double)mAspectRatioSize.getWidth()/mAspectRatioSize.getHeight();
        IAUtilities.showingLogs(TAG,"mAspectRatio = " + mAspectRatio,IAUtilities.LOGS_SHOWN);
        return mAspectRatio;
    }

    private Size mOptimalPreviewSize;
    private Size mOptimalPictureSize;
    private CameraCharacteristics cameraCharacteristics;

    /**
     * Sets up member variables related to camera.
     *
     * @param width  The width of available size for camera preview
     * @param height The height of available size for camera preview
     */
    private void setUpCameraOutputs(int width, int height) {
        Activity activity = getActivity();
        CameraManager manager = (CameraManager) activity.getSystemService(Context.CAMERA_SERVICE);
        try {
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics
                        = manager.getCameraCharacteristics(cameraId);

                // We don't use a front facing camera in this sample.
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing == CameraCharacteristics.LENS_FACING_FRONT) {
                    continue;
                }

                cameraCharacteristics = characteristics;
                StreamConfigurationMap map = characteristics.get(
                        CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                if (map == null) {
                    continue;
                }

                DisplayMetrics displaymetrics = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);

                double mDeviceAspectratio = calculateAspectRatio(new Size(displaymetrics.widthPixels,displaymetrics.heightPixels));

                // Find out if we need to swap dimension to get the preview size relative to sensor
                // coordinate.
//                int displayRotation = activity.getWindowManager().getDefaultDisplay().getRotation();
//                int sensorOrientation =
//                        characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
//                boolean swappedDimensions = false;
//                switch (displayRotation) {
//                    case Surface.ROTATION_0:
//                    case Surface.ROTATION_180:
//                        if (sensorOrientation == 90 || sensorOrientation == 270) {
//                            swappedDimensions = true;
//                        }
//                        break;
//                    case Surface.ROTATION_90:
//                    case Surface.ROTATION_270:
//                        if (sensorOrientation == 0 || sensorOrientation == 180) {
//                            swappedDimensions = true;
//                        }
//                        break;
//                    default:
//                        Log.e(TAG, "Display rotation is invalid: " + displayRotation);
//                }
//
//                Point displaySize = new Point();
//                activity.getWindowManager().getDefaultDisplay().getSize(displaySize);
//                int rotatedPreviewWidth = width;
//                int rotatedPreviewHeight = height;
//                int maxPreviewWidth = displaySize.x;
//                int maxPreviewHeight = displaySize.y;
//
//                if (swappedDimensions) {
//                    rotatedPreviewWidth = height;
//                    rotatedPreviewHeight = width;
//                    maxPreviewWidth = displaySize.y;
//                    maxPreviewHeight = displaySize.x;
//                }
//
//                if (maxPreviewWidth > MAX_PREVIEW_WIDTH) {
//                    maxPreviewWidth = MAX_PREVIEW_WIDTH;
//                }
//
//                if (maxPreviewHeight > MAX_PREVIEW_HEIGHT) {
//                    maxPreviewHeight = MAX_PREVIEW_HEIGHT;
//                }

                // Danger, W.R.! Attempting to use too large a preview size could  exceed the camera
                // bus' bandwidth limitation, resulting in gorgeous previews but the storage of
                // garbage capture data.
//                mPreviewSize = chooseOptimalSize(map.getOutputSizes(SurfaceTexture.class), maxPreviewWidth,
//                        maxPreviewHeight, mOptimalPictureSize);

                mOptimalPreviewSize = Collections.max(
                        Arrays.asList(map.getOutputSizes(SurfaceTexture.class)),
                        new CompareSizesByArea());

                List<Size> mOutputPreviewSizes = Arrays.asList(map.getOutputSizes(SurfaceTexture.class));

                Collections.sort(mOutputPreviewSizes, new Comparator<Size>() {
                    @Override
                    public int compare(Size size, Size t1) {
                        return size.getHeight() * size.getWidth() - t1.getHeight() * t1.getWidth();
                    }
                });

                List<Size> mRelevantOutputPreviewSizes = new ArrayList<>();

                for (int i=0;i< mOutputPreviewSizes.size();i++){
                    mOptimalPreviewSize = mOutputPreviewSizes.get(i);
//                    IAUtilities.showingLogs(TAG,"mOptimalPreviewSize width = " + mOptimalPreviewSize.getWidth() + " mOptimalPreviewSize height = " + mOptimalPreviewSize.getHeight(), IAUtilities.LOGS_SHOWN);
                    if (mOptimalPreviewSize.getWidth() > 600/*(600 * displaymetrics.scaledDensity)*/
                            && mDeviceAspectratio >= calculateAspectRatio(mOptimalPreviewSize)){
                        IAUtilities.showingLogs(TAG,"mOptimalPreviewSize width = " + mOptimalPreviewSize.getWidth() + " mOptimalPreviewSize height = " + mOptimalPreviewSize.getHeight(), IAUtilities.LOGS_SHOWN);
                        mRelevantOutputPreviewSizes.add(mOptimalPreviewSize);
                    }
                }

//                for (int i=0;i< mRelevantOutputPreviewSizes.size();i++){
//                    mOptimalPreviewSize = mRelevantOutputPreviewSizes.get(i);
//                }

                mOptimalPreviewSize = mRelevantOutputPreviewSizes.get(0);
                IAUtilities.showingLogs(TAG,"mOptimalPreviewSize width = " + mOptimalPreviewSize.getWidth() + " mOptimalPreviewSize height = " + mOptimalPreviewSize.getHeight(), IAUtilities.LOGS_SHOWN);


                mPreviewSize = mOptimalPreviewSize;

                double mPreviewAspectRatio = calculateAspectRatio(mOptimalPreviewSize);

//                RelativeLayout.LayoutParams mCameraPreviewParams = new
//                        RelativeLayout.LayoutParams((int)(mPreviewAspectRatio * getActivity().getWindowManager().getDefaultDisplay().getHeight())
//                        ,getActivity().getWindowManager().getDefaultDisplay().getHeight());
//
//                IAUtilities.showingLogs(TAG,"mCameraPreviewParams width = " + mCameraPreviewParams.width + " height = "
//                        + mCameraPreviewParams.height,IAUtilities.LOGS_SHOWN);
//                mCameraPreviewParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//
//                mTextureView.setLayoutParams(mCameraPreviewParams);
                // For still image captures, we use the mOptimalPictureSize available size.
                mOptimalPictureSize = Collections.max(
                        Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)),
                        new CompareSizesByArea());
                IAUtilities.showingLogs(TAG,"mOptimalPictureSize width = " + mOptimalPictureSize.getWidth() + " mOptimalPictureSize height = " + mOptimalPictureSize.getHeight(), IAUtilities.LOGS_SHOWN);IAUtilities.showingLogs(TAG,"mOptimalPictureSize width = " + mOptimalPictureSize.getWidth() + " mOptimalPictureSize height = " + mOptimalPictureSize.getHeight(), IAUtilities.LOGS_SHOWN);
                List<Size> mOutputPictureSizes = Arrays.asList(map.getOutputSizes(ImageFormat.JPEG));

                Collections.sort(mOutputPictureSizes, new Comparator<Size>() {
                    @Override
                    public int compare(Size size, Size t1) {
                        return size.getHeight() * size.getWidth() - t1.getHeight() * t1.getWidth();
                    }
                });

                List<Size> mRelevantOutputPictureSizes = new ArrayList<>();

                for (int i=0;i< mOutputPictureSizes.size();i++){
                    mOptimalPictureSize = mOutputPictureSizes.get(i);
//                    IAUtilities.showingLogs(TAG,"mOptimalPictureSize width = " + mOptimalPictureSize.getWidth() + " mOptimalPictureSize height = " + mOptimalPictureSize.getHeight(), IAUtilities.LOGS_SHOWN);
                    if (mOptimalPictureSize.getWidth() > 600/*(600 * displaymetrics.scaledDensity)*/
                            && mPreviewAspectRatio >= calculateAspectRatio(mOptimalPictureSize)){
                        IAUtilities.showingLogs(TAG,"mOptimalPictureSize width = " + mOptimalPictureSize.getWidth() + " mOptimalPictureSize height = " + mOptimalPictureSize.getHeight(), IAUtilities.LOGS_SHOWN);
                        mRelevantOutputPictureSizes.add(mOptimalPictureSize);
                    }
                }

//                for (int i=0;i< mRelevantOutputPictureSizes.size();i++){
//                    mOptimalPictureSize = mRelevantOutputPictureSizes.get(i);
//                }

                mOptimalPictureSize = mRelevantOutputPictureSizes.get(0);
                IAUtilities.showingLogs(TAG,"mOptimalPictureSize width = " + mOptimalPictureSize.getWidth() + " mOptimalPictureSize height = " + mOptimalPictureSize.getHeight(), IAUtilities.LOGS_SHOWN);


                mImageReader = ImageReader.newInstance(mOptimalPictureSize.getWidth(), mOptimalPictureSize.getHeight(),
                        ImageFormat.JPEG, /*maxImages*/2);
                mImageReader.setOnImageAvailableListener(
                        mOnImageAvailableListener, mBackgroundHandler);

                // We fit the aspect ratio of TextureView to the size of preview we picked.
                int orientation = getResources().getConfiguration().orientation;
                if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
                    mTextureView.setAspectRatio(
                            mPreviewSize.getWidth(), mPreviewSize.getHeight());
                } else {
                    mTextureView.setAspectRatio(
                            mPreviewSize.getHeight(), mPreviewSize.getWidth());
                }

                mCameraId = cameraId;
                return;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            // Currently an NPE is thrown when the Camera2API is used but not supported on the
            // device this code runs.
            ErrorDialog.newInstance(getString(R.string.camera_error))
                    .show(getChildFragmentManager(), FRAGMENT_CAMERA_PERMISSION_DIALOG);
        }
    }


    public int getNavBarHeight(Context c) {
        int result = 0;
        boolean hasMenuKey = ViewConfiguration.get(c).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

        if(!hasMenuKey && !hasBackKey) {
            //The device has a navigation bar
            Resources resources = c.getResources();

            int orientation = getResources().getConfiguration().orientation;
            int resourceId;
            if (isTablet(c)){
                resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape", "dimen", "android");
            }  else {
                resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_width", "dimen", "android");
            }

            if (resourceId > 0) {
                return getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    private boolean isTablet(Context c) {
        return (c.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }


    /**
     * Opens the camera specified by {@link FMCamera2BasicFragment#mCameraId}.
     */
    private void openCamera(int width, int height) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getActivity().checkSelfPermission(Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                requestCameraPermission();
                return;
            }
        }
        IAUtilities.showingLogs(TAG,"setup width = " + width + " setup height = " + height, IAUtilities.LOGS_SHOWN);
        setUpCameraOutputs(width, height);
        configureTransform(width, height);
        try {
            if (!mCameraOpenCloseLock.tryAcquire(2500, TimeUnit.MILLISECONDS)) {
                throw new RuntimeException("Time out waiting to lock camera opening.");
            }
            if (mIsCameraClosed){
                manager.openCamera(mCameraId, mStateCallback, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera opening.", e);
        }
    }

    /**
     * Closes the current {@link CameraDevice}.
     */
    private void closeCamera() {
        try {
            mCameraOpenCloseLock.acquire();
            if (null != mCaptureSession) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
            if (null != mCameraDevice) {
                mCameraDevice.close();
                mCameraDevice = null;
            }
            if (null != mImageReader) {
                mImageReader.close();
                mImageReader = null;
            }
        } catch (InterruptedException e) {
            throw new RuntimeException("Interrupted while trying to lock camera closing.", e);
        } finally {
            mCameraOpenCloseLock.release();
        }
    }

    /**
     * Starts a background thread and its {@link Handler}.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("CameraBackground");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
    }

    /**
     * Stops the background thread and its {@link Handler}.
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
            mBackgroundThread = null;
            mBackgroundHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a new {@link CameraCaptureSession} for camera preview.
     */
    private void createCameraPreviewSession() {
        try {
            SurfaceTexture texture = mTextureView.getSurfaceTexture();
            assert texture != null;

            // We configure the size of default buffer to be the size of camera preview we want.
            texture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());

            // This is the output Surface we need to start preview.
            Surface surface = new Surface(texture);

            // We set up a CaptureRequest.Builder with the output Surface.
            mPreviewRequestBuilder
                    = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            mPreviewRequestBuilder.addTarget(surface);

            final ProgressDialog progressDialog = new ProgressDialog(getActivity());
            progressDialog.setMessage("Initializing ..");
            progressDialog.setCancelable(false);

            // Here, we create a CameraCaptureSession for camera preview.
            mCameraDevice.createCaptureSession(Arrays.asList(surface, mImageReader.getSurface()),
                    new CameraCaptureSession.StateCallback() {

                        @Override
                        public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                            progressDialog.dismiss();
                            // The camera is already closed
                            if (null == mCameraDevice) {
                                return;
                            }

                            // When the session is ready, we start displaying the preview.
                            mCaptureSession = cameraCaptureSession;
                            try {
                                // Auto focus should be continuous for camera preview.
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                                        CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                                // Flash is automatically enabled when necessary.
                                mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                                        CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
//                                mPreviewRequestBuilder.set(CaptureRequest.FLASH_MODE,
//                                        CaptureRequest.FLASH_MODE_TORCH);


                                // Finally, we start displaying the camera preview.
                                mPreviewRequest = mPreviewRequestBuilder.build();
                                mCaptureSession.setRepeatingRequest(mPreviewRequest,
                                        mCaptureCallback, mBackgroundHandler);
                            } catch (CameraAccessException e) {
                                e.printStackTrace();
                            } catch (IllegalStateException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onConfigureFailed(
                                @NonNull CameraCaptureSession cameraCaptureSession) {
                            showToast("Failed");
                        }
                    }, null
            );
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Configures the necessary {@link Matrix} transformation to `mTextureView`.
     * This method should be called after the camera preview size is determined in
     * setUpCameraOutputs and also the size of `mTextureView` is fixed.
     *
     * @param viewWidth  The width of `mTextureView`
     * @param viewHeight The height of `mTextureView`
     */
    private void configureTransform(int viewWidth, int viewHeight) {
        Activity activity = getActivity();
        if (null == mTextureView || null == mPreviewSize || null == activity) {
            return;
        }
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, viewWidth, viewHeight);
        RectF bufferRect = new RectF(0, 0, mPreviewSize.getHeight(), mPreviewSize.getWidth());
        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.FILL);
            float scale = Math.max(
                    (float) viewHeight / mPreviewSize.getHeight(),
                    (float) viewWidth / mPreviewSize.getWidth());
            matrix.postScale(scale, scale, centerX, centerY);
            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
        } else if (Surface.ROTATION_180 == rotation) {
            matrix.postRotate(180, centerX, centerY);
        }
        mTextureView.setTransform(matrix);
    }

    /**
     * Initiate a still image capture.
     */
    private void takePicture() {
        lockFocus();
    }

    /**
     * Lock the focus as the first step for a still image capture.
     */
    private void lockFocus() {
        try {
            // This is how to tell the camera to lock focus.
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the lock.
            mState = STATE_WAITING_LOCK;
            if (mCaptureSession != null){
                mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                        mBackgroundHandler);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (mFile.exists()){
                            if (System.currentTimeMillis() - mFile.lastModified() >= 5000){
                                Toast.makeText(getActivity(),"Camera resources are busy. Please retake photo.",Toast.LENGTH_SHORT).show();
                            }else{
//                                Toast.makeText(getActivity(),"Image Saved.",Toast.LENGTH_SHORT).show();
                            }
                        }else{
//                            Toast.makeText(getActivity(),"Writing to new file. Please wait...",Toast.LENGTH_SHORT).show();
                        }
                    }
                },3000);
            }else {

            }

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Run the precapture sequence for capturing a still image. This method should be called when
     * we get a response in {@link #mCaptureCallback} from {@link #lockFocus()}.
     */
    private void runPrecaptureSequence() {
        try {
            // This is how to tell the camera to trigger.F
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER,
                    CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
            // Tell #mCaptureCallback to wait for the precapture sequence to be set.
            mState = STATE_WAITING_PRECAPTURE;
            mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                    mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /**
     * Capture a still picture. This method should be called when we get a response in
     * {@link #mCaptureCallback} from both {@link #lockFocus()}.
     */
    private void captureStillPicture() {
        try {
            final Activity activity = getActivity();
            if (null == activity || null == mCameraDevice) {
                return;
            }
            // This is the CaptureRequest.Builder that we use to take a picture.
            final CaptureRequest.Builder captureBuilder =
                    mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            captureBuilder.addTarget(mImageReader.getSurface());

            // Use the same AE and AF modes as the preview.
            captureBuilder.set(CaptureRequest.CONTROL_AF_MODE,
                    CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
            captureBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);

            // Orientation
//            int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
//            captureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATIONS.get(rotation));

            CameraCaptureSession.CaptureCallback mCurrentCaptureCallback
                    = new CameraCaptureSession.CaptureCallback() {

                @Override
                public void onCaptureCompleted(@NonNull CameraCaptureSession session,
                                               @NonNull CaptureRequest request,
                                               @NonNull TotalCaptureResult result) {
//                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
//                        Handler handler = new Handler(getActivity().getMainLooper());
//                        handler.postDelayed(new Runnable() {
//                            public void run() {
//                                if (mCaptureTopViewButton.isEnabled() && mCaptureAngleViewButton.isEnabled() && mCaptureSideViewButton.isEnabled()) {
//                                    Toast.makeText(getActivity(), "Please select a view to retake", Toast.LENGTH_SHORT).show();
//                                } else {
//                                    if (!mRetakeFlagEnabled) {
//                                        if (mCurrentButtonIndex == 1) {
//                                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mTopViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
//                                            IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
//                                            calculateActualCoordinates(mTopViewBitmap);
//                                            mCroppedBitmapBmp = Bitmap.createBitmap(mTopViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
//                                            IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
//                                            mTopViewFile = openFileForImage(mCurrentButtonIndex);
//                                            saveImageToFile(mTopViewFile);
//                                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
//                                            Gson gson = new Gson();
//                                            String mDataString = gson.toJson(2);
//                                            new IAUtilities().setSharedPreferences(getActivity(), mDataString, "CurrentFileIndexNo");
//                                            gotoPreviousActivity();
//                                        } else if (mCurrentButtonIndex == 2) {
//                                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mAngleViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
//                                            IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
//                                            calculateActualCoordinates(mAngleViewBitmap);
//                                            mCroppedBitmapBmp = Bitmap.createBitmap(mAngleViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
//                                            IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
//                                            mAngleViewFile = openFileForImage(mCurrentButtonIndex);
//                                            saveImageToFile(mAngleViewFile);
//                                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
//                                            Gson gson = new Gson();
//                                            String mDataString = gson.toJson(3);
//                                            new IAUtilities().setSharedPreferences(getActivity(), mDataString, "CurrentFileIndexNo");
//                                            gotoPreviousActivity();
//                                        } else if (mCurrentButtonIndex == 3) {
//                                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mSideViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
//                                            IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
//                                            calculateActualCoordinates(mSideViewBitmap);
//                                            mCroppedBitmapBmp = Bitmap.createBitmap(mSideViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
//                                            IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
//                                            mSideViewFile = openFileForImage(mCurrentButtonIndex);
//                                            saveImageToFile(mSideViewFile);
//                                            mRetakeFlagEnabled = true;
//                                            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.next_button_selector));
//                                            mCaptureTopViewButton.setEnabled(true);
//                                            mCaptureAngleViewButton.setEnabled(true);
//                                            mCaptureSideViewButton.setEnabled(true);
//                                            Gson gson = new Gson();
//                                            String mDataString = gson.toJson(mCurrentButtonIndex);
//                                            new IAUtilities().setSharedPreferences(getActivity(), mDataString, "CurrentFileIndexNo");
//                                        }
//                                    } else {
//                                        if (mCurrentButtonIndex == 1) {
//                                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mTopViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
//                                            IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
//                                            calculateActualCoordinates(mTopViewBitmap);
//                                            mCroppedBitmapBmp = Bitmap.createBitmap(mTopViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
//                                            IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
//                                            mTopViewFile = openFileForImage(mCurrentButtonIndex);
//                                            saveImageToFile(mTopViewFile);
//                                            mCaptureTopViewButton.setEnabled(true);
//                                            mCaptureAngleViewButton.setEnabled(true);
//                                            mCaptureSideViewButton.setEnabled(true);
//                                            Gson gson = new Gson();
//                                            String mDataString = gson.toJson(4);
//                                            new IAUtilities().setSharedPreferences(getActivity(), mDataString, "CurrentFileIndexNo");
//                                        } else if (mCurrentButtonIndex == 2) {
//                                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mAngleViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
//                                            IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
//                                            calculateActualCoordinates(mAngleViewBitmap);
//                                            mCroppedBitmapBmp = Bitmap.createBitmap(mAngleViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
//                                            IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
//                                            mAngleViewFile = openFileForImage(mCurrentButtonIndex);
//                                            saveImageToFile(mAngleViewFile);
//                                            mCaptureTopViewButton.setEnabled(true);
//                                            mCaptureAngleViewButton.setEnabled(true);
//                                            mCaptureSideViewButton.setEnabled(true);
//                                            Gson gson = new Gson();
//                                            String mDataString = gson.toJson(5);
//                                            new IAUtilities().setSharedPreferences(getActivity(), mDataString, "CurrentFileIndexNo");
//                                        } else if (mCurrentButtonIndex == 3) {
//                                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
//                                            mSideViewBitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath());
//                                            IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
//                                            calculateActualCoordinates(mSideViewBitmap);
//                                            mCroppedBitmapBmp = Bitmap.createBitmap(mSideViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
//                                            IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
//                                            mSideViewFile = openFileForImage(mCurrentButtonIndex);
//                                            saveImageToFile(mSideViewFile);
//                                            mCaptureTopViewButton.setEnabled(true);
//                                            mCaptureAngleViewButton.setEnabled(true);
//                                            mCaptureSideViewButton.setEnabled(true);
//                                            Gson gson = new Gson();
//                                            String mDataString = gson.toJson(6);
//                                            new IAUtilities().setSharedPreferences(getActivity(), mDataString, "CurrentFileIndexNo");
//                                        }
//                                        mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.next_button_selector));
//                                    }
//                                    if (mCurrentButtonIndex >= 0 && mCurrentButtonIndex <= 3) {
//                                        mCurrentButtonIndex++;
//                                    } else {
//                                        mCurrentButtonIndex = 0;
//                                    }
//                                    mCroppedBitmapBmp.recycle();
//                                }
//                                mCaptureImageButton.setEnabled(true);
//                            }
//                        }, 240);
//                    }
//                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){
//                        getActivity().runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                mListener.onImageSavedCropImage();
//                            }
//                        });
//                    }
                    unlockFocus();
                }
            };


            mCaptureSession.stopRepeating();
            mCaptureSession.capture(captureBuilder.build(), mCurrentCaptureCallback, null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    public void saveCroppedImageMethod(){
        IAUtilities.showingLogs("saveCroppedImageMethod", "in saveCroppedImageMethod...", IAUtilities.LOGS_SHOWN);

        if (mCaptureTopViewButton.isEnabled() && mCaptureAngleViewButton.isEnabled() && mCaptureSideViewButton.isEnabled()) {
            Toast.makeText(getActivity(), "Please select a view to retake", Toast.LENGTH_SHORT).show();
        } else {
            if (!mRetakeFlagEnabled) {
                if (mCurrentButtonIndex == 1) {
                    mTopViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
                    IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
                    calculateActualCoordinates(mTopViewBitmap);
                    mCroppedBitmapBmp = Bitmap.createBitmap(mTopViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
                    IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
                    mTopViewFile = openFileForImage(mCurrentButtonIndex);
                    saveImageToFile(mTopViewFile);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
                        }
                    });
                    new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(2), "CurrentFileIndexNo");
                    mTopViewBitmap.recycle();
                    gotoPreviousActivity();
                } else if (mCurrentButtonIndex == 2) {
                    mAngleViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
                    IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
                    calculateActualCoordinates(mAngleViewBitmap);
                    mCroppedBitmapBmp = Bitmap.createBitmap(mAngleViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
                    IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
                    mAngleViewFile = openFileForImage(mCurrentButtonIndex);
                    saveImageToFile(mAngleViewFile);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
                        }
                    });
                    new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(3), "CurrentFileIndexNo");
                    mAngleViewBitmap.recycle();
                    gotoPreviousActivity();
                } else if (mCurrentButtonIndex == 3) {
                    mSideViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
                    IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
                    calculateActualCoordinates(mSideViewBitmap);
                    mCroppedBitmapBmp = Bitmap.createBitmap(mSideViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
                    IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
                    mSideViewFile = openFileForImage(mCurrentButtonIndex);
                    saveImageToFile(mSideViewFile);
                    mRetakeFlagEnabled = true;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.next_button_selector));
                            mCaptureTopViewButton.setEnabled(true);
                            mCaptureAngleViewButton.setEnabled(true);
                            mCaptureSideViewButton.setEnabled(true);
                        }
                    });
                    new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(7), "CurrentFileIndexNo");
                    mSideViewBitmap.recycle();
                }
            } else {
                if (mCurrentButtonIndex == 1) {
                    mTopViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
                    IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
                    calculateActualCoordinates(mTopViewBitmap);
                    mCroppedBitmapBmp = Bitmap.createBitmap(mTopViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
                    IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
                    mTopViewFile = openFileForImage(mCurrentButtonIndex);
                    saveImageToFile(mTopViewFile);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureTopViewButton.setEnabled(true);
                            mCaptureAngleViewButton.setEnabled(true);
                            mCaptureSideViewButton.setEnabled(true);
                            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.next_button_selector));
                        }
                    });
                    new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(4), "CurrentFileIndexNo");
                    mTopViewBitmap.recycle();
                } else if (mCurrentButtonIndex == 2) {
                    mAngleViewBitmap = BitmapFactory.decodeFile(mFile.getPath());
                    IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
                    calculateActualCoordinates(mAngleViewBitmap);
                    mCroppedBitmapBmp = Bitmap.createBitmap(mAngleViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
                    IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
                    mAngleViewFile = openFileForImage(mCurrentButtonIndex);
                    saveImageToFile(mAngleViewFile);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureTopViewButton.setEnabled(true);
                            mCaptureAngleViewButton.setEnabled(true);
                            mCaptureSideViewButton.setEnabled(true);
                            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.next_button_selector));
                        }
                    });
                    new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(5), "CurrentFileIndexNo");
                    mAngleViewBitmap.recycle();
                } else if (mCurrentButtonIndex == 3) {
                    mSideViewBitmap = BitmapFactory.decodeFile(mFile.getAbsolutePath());
                    IAUtilities.showingLogs(TAG, "mCropViewLayout.getLeft() = " + mCropViewLayout.getLeft() + " mCropViewLayout.getTop() = " + mCropViewLayout.getTop() + " width = " + Integer.toString(mCropViewLayout.getWidth()) + " height = " + Integer.toString(mCropViewLayout.getHeight()), IAUtilities.LOGS_SHOWN);
                    calculateActualCoordinates(mSideViewBitmap);
                    mCroppedBitmapBmp = Bitmap.createBitmap(mSideViewBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
                    IAUtilities.showingLogs(TAG, " mCroppedBitmapBmp.width = " + mCroppedBitmapBmp.getWidth() + " mCroppedBitmapBmp.height = " + mCroppedBitmapBmp.getHeight(), IAUtilities.LOGS_SHOWN);
                    mSideViewFile = openFileForImage(mCurrentButtonIndex);
                    saveImageToFile(mSideViewFile);
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.retake_picture_selector));
                            mCaptureTopViewButton.setEnabled(true);
                            mCaptureAngleViewButton.setEnabled(true);
                            mCaptureSideViewButton.setEnabled(true);
                            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.next_button_selector));
                        }
                    });
                    new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(6), "CurrentFileIndexNo");
                    mSideViewBitmap.recycle();
                }
            }
            if (mCurrentButtonIndex >= 0 && mCurrentButtonIndex <= 3) {
                mCurrentButtonIndex++;
            } else {
                mCurrentButtonIndex = 0;
            }
            mCroppedBitmapBmp.recycle();
        }
    }

    private int mXCoordinateStart, mYCoordinateStart, mWidth, mHeight;

    private void calculateActualCoordinates(Bitmap mCameraBitmap) {
        mXCoordinateStart = 0;
        mYCoordinateStart = 0;
        mWidth = 0;
        mHeight = 0;

        IAUtilities.showingLogs(TAG, " mCameraBitmap = " + mCameraBitmap.getWidth() + " " + mCameraBitmap.getHeight(), IAUtilities.LOGS_SHOWN);
        IAUtilities.showingLogs(TAG, " mTextureView width = " + mCameraBitmap.getWidth() + " height = " + mCameraBitmap.getHeight(), IAUtilities.LOGS_SHOWN);

        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);

//        if (metrics.widthPixels >= 1440 || metrics.heightPixels >= 1080) {
            mXCoordinateStart = (mCropViewLayout.getLeft() - mTextureView.getLeft()) * mCameraBitmap.getWidth() / mTextureView.getWidth() /*- (int)(40 * metrics.scaledDensity)*/;
            mYCoordinateStart = (mCropViewLayout.getTop() - mTextureView.getTop()) * mCameraBitmap.getHeight() / mTextureView.getHeight();
            mWidth = mCropViewLayout.getWidth() * mCameraBitmap.getWidth() / mTextureView.getWidth() /*+ (int)(80 * metrics.scaledDensity)*/;
            mHeight = mCropViewLayout.getHeight() * mCameraBitmap.getHeight() / mTextureView.getHeight() /*+ (int)(20 * metrics.scaledDensity)*/;
            mXCoordinateStart = mXCoordinateStart - (int)(mWidth * metrics.scaledDensity * 10 / 100);
            mWidth = mWidth + (int)(mWidth * metrics.scaledDensity * 20 / 100);
            if (mCameraBitmap.getHeight() <= mHeight){
                mHeight = mCameraBitmap.getHeight();
            }
            if (mCameraBitmap.getWidth() <= mWidth){
                mWidth = mCameraBitmap.getWidth();
            }
            IAUtilities.showingLogs(TAG, "mXCoordinateStart = " + mXCoordinateStart + " mYCoordinateStart = " + mYCoordinateStart + " mWidth = " + mWidth + " mHeight = " + mHeight, IAUtilities.LOGS_SHOWN);
//         } else {
//            mXCoordinateStart = mCropViewLayout.getLeft() * mCameraBitmap.getWidth() / mPreviewSize.getWidth() - 20 * (int)metrics.scaledDensity;
//            mYCoordinateStart = mCropViewLayout.getTop() * mCameraBitmap.getHeight() / metrics.heightPixels + (int)(20 * metrics.scaledDensity);
//            mWidth = mCropViewLayout.getWidth() * mCameraBitmap.getWidth() / mPreviewSize.getWidth() + 40 * (int) metrics.scaledDensity;
//            mHeight = mCropViewLayout.getHeight() * mCameraBitmap.getHeight() / metrics.heightPixels + (int)(30 * metrics.scaledDensity);
//            mXCoordinateStart = mXCoordinateStart - (int)(mWidth * metrics.scaledDensity * 10 / 100);
//            mWidth = mWidth + (int)(mWidth * metrics.scaledDensity * 20 / 100);
//            if (mCameraBitmap.getHeight() <= mHeight){
//                mHeight = mCameraBitmap.getHeight();
//            }
//            if (mCameraBitmap.getWidth() <= mWidth){
//                mWidth = mCameraBitmap.getWidth();
//            }
//            IAUtilities.showingLogs(TAG, "mXCoordinateStart = " + mXCoordinateStart + " mYCoordinateStart = " + mYCoordinateStart + " mWidth = " + mWidth + " mHeight = " + mHeight, IAUtilities.LOGS_SHOWN);
//        }
    }

//    private void calculateActualCoordinates(Bitmap mCameraBitmap) {
//        mXCoordinateStart = 0;
//        mYCoordinateStart = 0;
//        mWidth = 0;
//        mHeight = 0;
//
////        IAUtilities.showingLogs(TAG, " mCameraBitmap = " + mCameraBitmap.getWidth() + " " + mCameraBitmap.getHeight(), IAUtilities.LOGS_SHOWN);
//
//        DisplayMetrics metrics = new DisplayMetrics();
//        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
//
//        float mHeightScaleFactor = (float)mCameraBitmap.getHeight()/metrics.heightPixels;
//        float mWidthScaleFactor = (float)mCameraBitmap.getWidth() / (metrics.widthPixels + getNavBarHeight(getActivity()));
//        IAUtilities.showingLogs(TAG,"scale factor height = " + mHeightScaleFactor + " camera bitmap height = " + mCameraBitmap.getHeight()
//         + " device height = " + metrics.heightPixels, IAUtilities.LOGS_SHOWN);
//
//        if ((metrics.widthPixels + getNavBarHeight(getActivity())) >= 1920 || metrics.heightPixels >= 1080) {
//            mXCoordinateStart = (int)(mCropViewLayout.getLeft() * mWidthScaleFactor) /*- (int)(40 * metrics.scaledDensity)*/;
//            mYCoordinateStart = (int)(mCropViewLayout.getTop() * mHeightScaleFactor);
//            mWidth = (int)(mCropViewLayout.getWidth() * mWidthScaleFactor) + (int)(80 * metrics.scaledDensity);
//            mHeight = (int)(mCropViewLayout.getHeight() * mHeightScaleFactor) + (int)(20 * metrics.scaledDensity);
//            mXCoordinateStart = mXCoordinateStart - (int)(mWidth * metrics.scaledDensity * 10 / 100);
//            mWidth = mWidth + (int)(mWidth * metrics.scaledDensity * 20 / 100);
//            IAUtilities.showingLogs(TAG, "mXCoordinateStart = " + mXCoordinateStart + " mYCoordinateStart = " + mYCoordinateStart + " mWidth = " + mWidth + " mHeight = " + mHeight, IAUtilities.LOGS_SHOWN);
//        } else {
//            mXCoordinateStart = mCropViewLayout.getLeft() * mCameraBitmap.getWidth() / mPreviewSize.getWidth() /*- 20 * (int)metrics.scaledDensity*/;
//            mYCoordinateStart = mCropViewLayout.getTop() * mCameraBitmap.getHeight() / metrics.heightPixels /*+ (int)(20 * metrics.scaledDensity)*/;
//            mWidth = mCropViewLayout.getWidth() * mCameraBitmap.getWidth() / mPreviewSize.getWidth() + 40 * (int) metrics.scaledDensity;
//            mHeight = mCropViewLayout.getHeight() * mCameraBitmap.getHeight() / metrics.heightPixels + (int)(30 * metrics.scaledDensity);
//            mXCoordinateStart = mXCoordinateStart - (int)(mWidth * metrics.scaledDensity * 10 / 100);
//            mWidth = mWidth + (int)(mWidth * metrics.scaledDensity * 20 / 100);
//            IAUtilities.showingLogs(TAG, "mXCoordinateStart = " + mXCoordinateStart + " mYCoordinateStart = " + mYCoordinateStart + " mWidth = " + mWidth + " mHeight = " + mHeight, IAUtilities.LOGS_SHOWN);
//        }
//    }

    private void setWidthAccToHeight() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mCropViewLayout.getLayoutParams();
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float mVerticalHeight = metrics.heightPixels - 100 * metrics.scaledDensity;
        float mScaleFactor = 0;
//        if (Boolean.parseBoolean(new IAUtilities().getSharedPreferences(getActivity(),"LetterPaper"))){
//            mScaleFactor = 215.9f / 279.4f;
//        }else {
            mScaleFactor = 210.0f / 297.0f;
//        }
//        if (mCurrentCustomer.getCustomerPaperType() == 1) {
        mScaleFactor = mScaleFactor * mVerticalHeight;
//        } else if (mCurrentCustomer.getCustomerPaperType() == 2) {
//            mScaleFactor = 215.9f / 279.4f * mVerticalHeight;
//        }
        params.height = (int) Math.ceil(mVerticalHeight);
        params.width = (int) Math.ceil(mScaleFactor);
        mCropViewLayout.setLayoutParams(params);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        IAUtilities.showingLogs(TAG,"Preview result = " + result, IAUtilities.LOGS_SHOWN);
        return result;
    }


    private void saveImageToFile(File file) {
        if (mCroppedBitmapBmp != null) {
            FileOutputStream outStream = null;
            try {
                outStream = new FileOutputStream(file);
                if (!mCroppedBitmapBmp.compress(Bitmap.CompressFormat.PNG, 100, outStream)) {
                    Toast.makeText(getActivity(), "Unable to save image to file.",
                            Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(getActivity(), "Saved image to: " + file.getPath(),
//                            Toast.LENGTH_SHORT).show();
                }
                outStream.close();
                mCroppedBitmapBmp.recycle();
            } catch (Exception e) {
                Toast.makeText(getActivity(), "Unable to save image to file.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

//    private void copyFiles(File mTargetFile, File mSourceFile){
//        try
//        {
//            FileUtils.copyFile(mSourceFile, mTargetFile);
//        }
//        catch (IOException e)
//        {
//            e.printStackTrace();
//        }
//    }

    private void copyFiles(File mTargetFile, File mSourceFile) {

        try {

            InputStream in = new FileInputStream(mSourceFile);
            OutputStream out = new FileOutputStream(mTargetFile);

            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            in.close();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Unlock the focus. This method should be called when still image capture sequence is
     * finished.
     */
    private void unlockFocus() {
        try {
            // Reset the auto-focus trigger
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,
                    CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            mPreviewRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE,
                    CaptureRequest.CONTROL_AE_MODE_ON_ALWAYS_FLASH);
            if (mCaptureSession !=null){
                mCaptureSession.capture(mPreviewRequestBuilder.build(), mCaptureCallback,
                        mBackgroundHandler);
                // After this, the camera will go back to the normal state of preview.
                mState = STATE_PREVIEW;
                mCaptureSession.setRepeatingRequest(mPreviewRequest, mCaptureCallback,
                        mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.capture_image_btn: {
                mCaptureImageButton.setEnabled(false);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (getActivity().checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) !=
                            PackageManager.PERMISSION_GRANTED) {
                        requestMemoryWritePermission();
                        return;
                    }
                }
//                view.setEnabled(false);
                if (mCaptureTopViewButton.isEnabled() && mCaptureAngleViewButton.isEnabled() && mCaptureSideViewButton.isEnabled()) {
                    new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(7), "CurrentFileIndexNo");
                    view.setEnabled(true);
//                    goToUploadImagesActivity();
                    goToUploadImagesRevisedActivity();
                    mCaptureImageButton.setEnabled(true);
                } else {

//                    Runnable runnable = new Runnable() {
//                        @Override
//                        public void run() {
//                            takePicture();
//                        }
//                    };
//                    new Thread(runnable).start();
                    takePicture();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mCaptureImageButton.setEnabled(true);
                        }
                    },3000);
//                    takePicture();
//                    view.setEnabled(true);
                }
                break;
            }
        }
        if (view == mCaptureTopViewButton && mRetakeFlagEnabled) {
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 1;
            new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(mCurrentButtonIndex + 3), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureAngleViewButton && mRetakeFlagEnabled) {
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 2;
            new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(mCurrentButtonIndex + 3), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureSideViewButton && mRetakeFlagEnabled) {
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 3;
            new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(mCurrentButtonIndex + 3), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureTopViewButton) {
            mCaptureTopViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 1;
            new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(mCurrentButtonIndex), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureAngleViewButton) {
            mCaptureAngleViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 2;
            new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(mCurrentButtonIndex), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureSideViewButton) {
            mCaptureSideViewButton.setBackground(getActivity().getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(getActivity().getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 3;
            new IAUtilities().setSharedPreferences(getActivity(), String.valueOf(mCurrentButtonIndex), "CurrentFileIndexNo");
            gotoPreviousActivity();
        }
    }

//    private void goToUploadImagesActivity() {
//        Intent intent = new Intent(getActivity(), FMUploadImagesActivity.class);
//        intent.putExtra("image_1", mFileStoragePath + "image_1.png");
//        intent.putExtra("image_2", mFileStoragePath + "image_2.png");
//        intent.putExtra("image_3", mFileStoragePath + "image_3.png");
//        startActivity(intent);
//    }

    private void goToUploadImagesRevisedActivity() {
        Intent intent = new Intent(getActivity(), FMUploadImagesRevisedActivity.class);
        intent.putExtra("image_resolution", mOptimalPictureSize.getWidth() + " x " + mOptimalPictureSize.getHeight());
        IAUtilities.showingLogs(TAG,"image_resolution = " + mOptimalPictureSize.getWidth() + " x " + mOptimalPictureSize.getHeight(),IAUtilities.LOGS_SHOWN);
        intent.putExtra("zoom_level", cameraCharacteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM));
        IAUtilities.showingLogs(TAG,"zoom_level = " + cameraCharacteristics.get(CameraCharacteristics.SCALER_AVAILABLE_MAX_DIGITAL_ZOOM),IAUtilities.LOGS_SHOWN);
        getActivity().startActivityForResult(intent, Integer.parseInt(getActivity().getResources().getString(R.string.upload_images_revised)));
    }


    private static final int REINITIALIZE_ACTIVITY = 10;

    private void gotoPreviousActivity() {
        getActivity().setResult(Integer.parseInt(getActivity().getResources().getString(R.string.reinitialize_activity)));
        getActivity().finish();
//        Intent intent = new Intent(getActivity(), FMShowTutorialVideoActivity.class);
//        startActivity(intent);
    }

    /**
     * Saves a JPEG {@link Image} into the specified {@link File}.
     */
    private class ImageSaver implements Runnable {

        /**
         * The JPEG image
         */
        private final Image mImage;
        /**
         * The file we save the image into.
         */
        private final File mFile1;
        private final FMCamera2BasicFragment fmCamera2BasicFragment;

        public ImageSaver(Image image, File file, FMCamera2BasicFragment fmCamera2BasicFragment) {
            mImage = image;
            mFile1 = file;
            this.fmCamera2BasicFragment = fmCamera2BasicFragment;
            IAUtilities.showingLogs("image saver","in image saver constructer", IAUtilities.LOGS_SHOWN);

        }

        @Override
        public void run() {
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] bytes = new byte[buffer.remaining()];
            buffer.get(bytes);
            FileOutputStream output = null;
            try {
                output = new FileOutputStream(mFile1);
                output.write(bytes);
                IAUtilities.showingLogs("image saver", "in image saver writing to file", IAUtilities.LOGS_SHOWN);

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                mImage.close();
                if (null != output) {
                    try {
                        output.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }finally {
                        if (mFile1 != null){

                            IAUtilities.showingLogs("image saver", "writing is done and mfile1 is not null", IAUtilities.LOGS_SHOWN);
                            fmCamera2BasicFragment.mFile = mFile1;
                            saveCroppedImageMethod();//                            fmCamera2BasicFragment.getActivity().runOnUiThread(new Runnable() {
//                                @Override
//                                public void run() {
//                                    if (mFile1 != null) {
//
//                                        fmCamera2BasicFragment.mFile = mFile1;
//                                        saveCroppedImageMethod();
//                                        onPause();
//                                        onResume();
//                                    }
//                                    else
//                                        IAUtilities.showingLogs("in fmcamera2basic", "mfile1 is null", IAUtilities.LOGS_SHOWN);
//
////                            Toast.makeText(getActivity(),"mFile is null",Toast.LENGTH_SHORT).show();
//                                }
//                            });
                        }
                        else
                            IAUtilities.showingLogs("image saver","mfile is not null in image saver class", IAUtilities.LOGS_SHOWN);
                    }
                }
            }
        }
    }

    /**
     * Compares two {@code Size}s based on their areas.
     */
    static class CompareSizesByArea implements Comparator<Size> {

        @Override
        public int compare(Size lhs, Size rhs) {
            // We cast here to ensure the multiplications won't overflow
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() -
                    (long) rhs.getWidth() * rhs.getHeight());
        }

    }


    /**
     * Shows an error message dialog.
     */
    public static class ErrorDialog extends DialogFragment {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message) {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity)
                    .setMessage(getArguments().getString(ARG_MESSAGE))
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            activity.finish();
                        }
                    })
                    .create();
        }

    }

    /**
     * Shows OK/Cancel confirmation dialog about camera permission.
     */
    public static class ConfirmationCameraDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_camera_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            FragmentCompat.requestPermissions(parent,
//                                    new String[]{Manifest.permission.CAMERA},
//                                    REQUEST_CAMERA_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Activity activity = parent.getActivity();
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

    /**
     * Shows OK/Cancel confirmation dialog about camera permission.
     */
    public static class ConfirmationMemoryWriteDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity())
                    .setMessage(R.string.request_memory_write_permission)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            FragmentCompat.requestPermissions(parent,
//                                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
//                                    REQUEST_WRITE_EXTERNAL_STORAGE_PERMISSION);
                        }
                    })
                    .setNegativeButton(android.R.string.cancel,
                            new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Activity activity = parent.getActivity();
                                    if (activity != null) {
                                        activity.finish();
                                    }
                                }
                            })
                    .create();
        }
    }

    private Bitmap rotateImage(Activity activity, String imagePath) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, bounds);
        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(imagePath, opts);
// Read EXIF Data
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(imagePath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        String orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        int orientation = orientString != null ? Integer.parseInt(orientString) : ExifInterface.ORIENTATION_NORMAL;
        int rotationAngle = 0;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;
// Rotate Bitmap
        Matrix matrix = new Matrix();
        Bitmap rotatedBitmap;
        if (getDeviceName().equalsIgnoreCase(activity.getString(R.string.nexus_5x))) {
            matrix.setRotate(180, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
            rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        } else {
            matrix.setRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
            rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        }
// Return result
        return rotatedBitmap;
    }

    /**
     * Returns the consumer friendly device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return "" + model;
        }
        return "" + model;
    }
}