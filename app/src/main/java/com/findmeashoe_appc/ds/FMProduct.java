package com.findmeashoe_appc.ds;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by iaurokitkat on 17/12/15.
 */
public class FMProduct implements Serializable {
    private int mProductId = -1;
    private String mProductName = "";
    private String mProductPrice = "";
    private String mProductImageUrl = "";
    private String mProductSize = "";
    private String mProductImageUrlLink = "";
    private String mProductFittingScore = "";
    private String mProductBuyLink = "";
    private String mProductDescription = "";

    public FMProduct() {
    }

    public FMProduct(FMProduct fmProduct) {
        this.setProductId(fmProduct.getProductId());
        this.setProductName(fmProduct.getProductName());
        this.setProductPrice(fmProduct.getProductPrice());
        this.setProductImageUrl(fmProduct.getProductImageUrl());
        this.setProductSize(fmProduct.getProductSize());
        this.setProductImageUrlLink(fmProduct.getProductImageUrlLink());
        this.setProductFittingScore(fmProduct.getProductFittingScore());
        this.setProductBuyLink(fmProduct.getProductBuyLink());
        this.setProductDescription(fmProduct.getProductDescription());
    }

    public int getProductId() {
        return mProductId;
    }

    public void setProductId(int mCategoryId) {
        this.mProductId = mCategoryId;
    }

    public String getProductName() {
        return mProductName;
    }

    public void setProductName(String mCategoryName) {
        this.mProductName = mCategoryName;
    }

    public String getProductPrice() {
        return mProductPrice;
    }

    public void setProductPrice(String mProductPrice) {
        this.mProductPrice = mProductPrice;
    }

    public String getProductImageUrl() {
        return mProductImageUrl;
    }

    public void setProductImageUrl(String mProductImage) {
        this.mProductImageUrl = mProductImage;
    }

    public String getProductSize() {
        return mProductSize;
    }

    public void setProductSize(String mProductSize) {
        this.mProductSize = mProductSize;
    }

    public String getProductImageUrlLink() {
        return mProductImageUrlLink;
    }

    public void setProductImageUrlLink(String mProductImageUrlLink) {
        this.mProductImageUrlLink = mProductImageUrlLink;
    }

    public String getProductFittingScore() {
        return mProductFittingScore;
    }

    public void setProductFittingScore(String mProductFittingScore) {
        this.mProductFittingScore = mProductFittingScore;
    }

    public String getProductBuyLink() {
        return mProductBuyLink;
    }

    public void setProductBuyLink(String mProductBuyLink) {
        this.mProductBuyLink = mProductBuyLink;
    }

    public String getProductDescription() {
        return mProductDescription;
    }

    public void setProductDescription(String mProductDescription) {
        this.mProductDescription = mProductDescription;
    }

    public void loadData(String response) {
        // parse object
        try {

            JSONObject jsonObject = new JSONObject(response);
            this.setProductId(jsonObject.optInt("id"));
            this.setProductName(jsonObject.optString("name"));
            this.setProductPrice(jsonObject.optString("price"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
