package com.findmeashoe_appc.ds;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by sayali on 24/9/15.
 */
public class FMCustomer implements Serializable {
    private String mCustomerUsername = "";
    private String mCustomerEmailId = "";
    private String mCustomerPassword = "";
    private String mCustomerStatus = "";
    private int mCustomerSuccess = 0;
    private String mCustomerGender = "";
    private int mCustomerAge = 0;
    private String mCustomerTag = "";
    private int mCustomerPaperType = -1;
    private String mCustomerFirstName = "";
    private String mCustomerLastName = "";
    private String mCustomerId = "";

    public FMCustomer(FMCustomer mCustomer) {
        this.setCustomerUsername(mCustomer.getCustomerUsername());
        this.setCustomerEmailId(mCustomer.getCustomerEmailId());
        this.setCustomerPassword(mCustomer.getCustomerPassword());
        this.setCustomerStatus(mCustomer.getCustomerStatus());
        this.setCustomerSuccess(mCustomer.getCustomerSuccess());
        this.setCustomerGender(mCustomer.getCustomerGender());
        this.setCustomerAge(mCustomer.getCustomerAge());
        this.setCustomerTag(mCustomer.getCustomerTag());
        this.setCustomerPaperType(mCustomer.getCustomerPaperType());
        this.setCustomerFirstName(mCustomer.getCustomerFirstName());
        this.setCustomerLastName(mCustomer.getCustomerLastName());
        this.setCustomerId(mCustomer.getCustomerId());
    }

    public FMCustomer() {
    }

    public String getCustomerUsername() {
        return mCustomerUsername;
    }

    public void setCustomerUsername(String mCustomerUsername) {
        this.mCustomerUsername = mCustomerUsername;
    }

    public String getCustomerPassword() {
        return mCustomerPassword;
    }

    public void setCustomerPassword(String mCustomerPassword) {
        this.mCustomerPassword = mCustomerPassword;
    }

    public String getCustomerEmailId() {
        return mCustomerEmailId;
    }

    public void setCustomerEmailId(String mCustomerEmailId) {
        this.mCustomerEmailId = mCustomerEmailId;
    }

    public String getCustomerStatus() {
        return mCustomerStatus;
    }

    public void setCustomerStatus(String mCustomerStatus) {
        this.mCustomerStatus = mCustomerStatus;
    }

    public int getCustomerSuccess() {
        return mCustomerSuccess;
    }

    public void setCustomerSuccess(int mCustomerSuccess) {
        this.mCustomerSuccess = mCustomerSuccess;
    }

    public String getCustomerGender() {
        return mCustomerGender;
    }

    public void setCustomerGender(String mCustomerGender) {
        this.mCustomerGender = mCustomerGender;
    }

    public int getCustomerAge() {
        return mCustomerAge;
    }

    public void setCustomerAge(int mCustomerAge) {
        this.mCustomerAge = mCustomerAge;
    }

    public String getCustomerTag() {
        return mCustomerTag;
    }

    public void setCustomerTag(String mCustomerTag) {
        this.mCustomerTag = mCustomerTag;
    }

    public int getCustomerPaperType() {
        return mCustomerPaperType;
    }

    public void setCustomerPaperType(int mCustomerPaperType) {
        this.mCustomerPaperType = mCustomerPaperType;
    }

    public String getCustomerFirstName() {
        return mCustomerFirstName;
    }

    public void setCustomerFirstName(String mCustomerFirstName) {
        this.mCustomerFirstName = mCustomerFirstName;
    }

    public String getCustomerLastName() {
        return mCustomerLastName;
    }

    public void setCustomerLastName(String mCustomerLastName) {
        this.mCustomerLastName = mCustomerLastName;
    }

    public String getCustomerId() {
        return mCustomerId;
    }

    public void setCustomerId(String mCustomerId) {
        this.mCustomerId = mCustomerId;
    }

    public void loadData(String response) {
        // parse object
        try {

            JSONObject jsonObject = new JSONObject(response);
            this.setCustomerUsername(jsonObject.optString("username"));
            this.setCustomerId(jsonObject.optString("id"));
            this.setCustomerFirstName(jsonObject.optString("firstname"));
            this.setCustomerStatus(jsonObject.optString("status"));
            this.setCustomerSuccess(jsonObject.optInt("success"));
            this.setCustomerPassword(jsonObject.optString("passwd"));
            String mCustomerGenderString = jsonObject.optString("id_gender");
            if (jsonObject.optString("id_gender").equalsIgnoreCase(String.valueOf(1))) {
                mCustomerGenderString = "M";
            } else if (jsonObject.optString("id_gender").equalsIgnoreCase(String.valueOf(2))) {
                mCustomerGenderString = "F";
            }
            this.setCustomerGender(mCustomerGenderString);
            this.setCustomerAge(jsonObject.optInt("age"));
            this.setCustomerEmailId(jsonObject.optString("email"));
            this.setCustomerTag(jsonObject.optString("tag"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
