package com.findmeashoe_appc.ds;

/**
 * Created by iaurokitkat on 30/12/15.
 */
public class FMError {
    private String mErrorCode = "";
    private String mErrorRemark = "";
    private int mErrorImage = -1;
    private String mErrorDescription = "";

    public FMError() {
    }

    public FMError(FMError fmError) {
        this.setErrorCode(fmError.getErrorCode());
        this.setErrorRemark(fmError.getErrorRemark());
        this.setErrorImage(fmError.getErrorImage());
        this.setErrorDescription(fmError.getErrorDescription());
    }

    public FMError(String mErrorCode, String mErrorRemark, int mErrorImage, String mErrorDescription) {
        this.mErrorCode = mErrorCode;
        this.mErrorRemark = mErrorRemark;
        this.mErrorImage = mErrorImage;
        this.mErrorDescription = mErrorDescription;
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(String mErrorCode) {
        this.mErrorCode = mErrorCode;
    }

    public String getErrorRemark() {
        return mErrorRemark;
    }

    public void setErrorRemark(String mErrorRemark) {
        this.mErrorRemark = mErrorRemark;
    }

    public int getErrorImage() {
        return mErrorImage;
    }

    public void setErrorImage(int mErrorImage) {
        this.mErrorImage = mErrorImage;
    }

    public String getErrorDescription() {
        return mErrorDescription;
    }

    public void setErrorDescription(String mErrorHeading) {
        this.mErrorDescription = mErrorHeading;
    }
}
