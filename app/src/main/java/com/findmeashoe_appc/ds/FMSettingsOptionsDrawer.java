package com.findmeashoe_appc.ds;

import android.content.Context;

/**
 * Created by iaurokitkat on 1/9/15.
 */
public class FMSettingsOptionsDrawer {
    private String mRecruiterCategoryName;
    private Integer mRecruiterCategoryIcon;



    public String getSettingsOptionName(Context mContext) {
        return mRecruiterCategoryName;
    }

    public void setSettingsOptionName(Context mContext, String mRecruiterCategoryName) {
        this.mRecruiterCategoryName = mRecruiterCategoryName;
    }

    public Integer getSettingsOptionIcon(Context mContext) {
        return mRecruiterCategoryIcon;
    }

    public void setSettingsOptionIcon(Context mContext, Integer mRecruiterCategoryIcon) {
        this.mRecruiterCategoryIcon = mRecruiterCategoryIcon;
    }
}
