package com.findmeashoe_appc.ds;

/**
 * Created by sayali on 24/9/15.
 */
public class FMResponse {
    private String mErrorMessage;
    private String mResponseData;
    private boolean mIsSuccess = false;
    private String mErrorCode;

    public String getErrorMessage() {
        return mErrorMessage;
    }

    public void setErrorMessage(String mErrorMessage) {
        this.mErrorMessage = mErrorMessage;
    }

    public boolean isSuccess() {
        return mIsSuccess;
    }

    public void setIsSuccess(boolean mIsSuccess) {
        this.mIsSuccess = mIsSuccess;
    }

    public String getResponseData() {
        return mResponseData;
    }

    public void setResponseData(String mResponseData) {
        this.mResponseData = mResponseData;
    }

    public String getErrorCode() {
        return mErrorCode;
    }

    public void setErrorCode(String mErrorCode) {
        this.mErrorCode = mErrorCode;
    }

    @Override
    public String toString() {
        return "FMResponse{" +
                "mErrorMessage='" + mErrorMessage + '\'' +
                ", mResponseData='" + mResponseData + '\'' +
                ", mIsSuccess=" + mIsSuccess +
                ", mErrorCode='" + mErrorCode + '\'' +
                '}';
    }
}
