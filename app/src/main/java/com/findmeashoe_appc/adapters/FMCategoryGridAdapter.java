package com.findmeashoe_appc.adapters;

import android.content.Context;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;


import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCategory;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMXmlParser;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by iaurokitkat on 15/6/15.
 */

public class FMCategoryGridAdapter extends BaseAdapter {
    public static final String TAG = "FMCatalogCategory";
    ArrayList<FMCategory> objects;
    Context context;
    private LayoutInflater layoutInflater;

    public FMCategoryGridAdapter(ArrayList<FMCategory> objects, Context context) {
        this.objects = objects;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_grid_category, viewGroup, false);
            viewHolder = new ViewHolder();
            viewHolder.tvCategoryName = (IACustomTextView) view.findViewById(R.id.category_name_tv);
            viewHolder.ivCategoryImage = (ImageView) view.findViewById(R.id.catalog_category_iv);
            viewHolder.pbImageLoading = (ProgressBar) view.findViewById(R.id.progressbar_image);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.tvCategoryName.setText(objects.get(position).getCategoryName());
        viewHolder.pbImageLoading.setVisibility(View.VISIBLE);

        new GetImageAsyncTask(objects.get(position), viewHolder).execute();
        return view;
    }

    private class ViewHolder {
        IACustomTextView tvCategoryName;
        ImageView ivCategoryImage;
        ProgressBar pbImageLoading;

    }

    private class GetImageAsyncTask extends AsyncTask<Void, Void, FMResponse> {
        private FMCategory fmCategory;
        private ViewHolder viewHolder;

        public GetImageAsyncTask(FMCategory fmCategory, ViewHolder viewHolder) {
            this.fmCategory = fmCategory;
            this.viewHolder = viewHolder;
        }

        @Override
        protected FMResponse doInBackground(Void... voids) {
            FMWebConnection webConnection = new FMWebConnection();
            String mResultUrl = fmCategory.getCategoryImageUrlLink();
            FMResponse mImageResponse = webConnection.getResponse(mResultUrl);
            if (mImageResponse.isSuccess()) {
                IAUtilities.showingLogs(TAG, "mImageResponse = " + mImageResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
                FMXmlParser fmXmlParser = new FMXmlParser();
                String mImageUrl = fmXmlParser.parse(mImageResponse.getResponseData());
                fmCategory.setCategoryImageUrl(mImageUrl);
            }
            return mImageResponse;
        }

        @Override
        protected void onPostExecute(FMResponse mImageResponse)
        {
            super.onPostExecute(mImageResponse);
            IAUtilities.showingLogs(TAG, "mImageResponse = " + mImageResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
            if (mImageResponse.isSuccess())
            {
                if (!fmCategory.getCategoryImageUrl().equalsIgnoreCase("")) {
                    Picasso.with(context).load(fmCategory.getCategoryImageUrl())
                            .into(viewHolder.ivCategoryImage, new Callback() {
                                @Override
                                public void onSuccess() {
                                    viewHolder.pbImageLoading.setVisibility(View.GONE);
                                }

                                @Override
                                public void onError() {
                                    viewHolder.pbImageLoading.setVisibility(View.GONE);
                                }
                            });
                } else
                    {
                    viewHolder.pbImageLoading.setVisibility(View.GONE);
                }
            }
        }
    }
}
