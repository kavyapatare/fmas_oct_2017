package com.findmeashoe_appc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMSettingsOptionsDrawer;
import com.findmeashoe_appc.ui.IACustomTextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by iaurokitkat on 8/9/15.
 */
public class FMSettingsOptionsDrawerAdapter extends BaseAdapter {
    private Context context;
    private LayoutInflater layoutInflater;
    private List<FMSettingsOptionsDrawer> objects = new ArrayList<FMSettingsOptionsDrawer>();

    public FMSettingsOptionsDrawerAdapter(Context context, ArrayList<FMSettingsOptionsDrawer> mSettingsOptions) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        objects = mSettingsOptions;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.navigation_item, null);
            viewHolder = new ViewHolder();
            viewHolder.ivCategoryImage = (ImageView) view.findViewById(R.id.img_nav_icon);
            viewHolder.tvCategoryName = (IACustomTextView) view.findViewById(R.id.txt_nav_title);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.tvCategoryName.setText(objects.get(position).getSettingsOptionName(context));

        viewHolder.tvCategoryName.setTextColor(context.getResources().getColor(R.color.deep_black));
        viewHolder.ivCategoryImage.setImageResource(objects.get(position).getSettingsOptionIcon(context));
        return view;
    }

    public class ViewHolder {
        ImageView ivCategoryImage;
        IACustomTextView tvCategoryName;
    }
}

