package com.findmeashoe_appc.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.findmeashoe_appc.R;

/**
 * This custom TextView field contains all the functions of a regular TextView and a custom font typeface.
 * Created by iaurokitkat on 28/7/15.
 */
public class IACustomTextView extends TextView {

    Context mContext;

    public IACustomTextView(Context context) {
        super(context);
        mContext = context;
    }

    public IACustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);

        init(mFontName);
        mTypedArray.recycle();
        setSelected(true);
    }

    public IACustomTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);

        init(mFontName);
        mTypedArray.recycle();
        setSelected(true);
    }


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public IACustomTextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        mContext = context;

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);

        init(mFontName);
        mTypedArray.recycle();
        setSelected(true);
    }

    /**
     * Setting custom typeface
     *
     * @param mFontName
     */

    public void init(String mFontName) {
        if (mFontName != null) {
            try {
                Typeface mTypeface = null;

                if (mFontName.contains("Bold")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Bold.ttf");
                } else if (mFontName.contains("Semibold")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Semibold.ttf");
                } else if (mFontName.contains("Regular")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Regular.ttf");
                } else if (mFontName.contains("Light")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Light.ttf");
                }
                setTypeface(mTypeface);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
