package com.findmeashoe_appc.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.util.StateSet;
import android.widget.Button;

import com.findmeashoe_appc.R;

/**
 * This custom Button field contains all the functions of a regular Button and a custom font typeface.
 * Created by iaurokitkat on 28/7/15.
 */
public class IACustomButton extends Button {

    private Drawable mDrawable;
    private Context mContext;

    public IACustomButton(Context context) {
        super(context);
        mContext = context;
        mDrawable = getBackground();
    }

    public IACustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        mDrawable = getBackground();

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);
        mTypedArray.recycle();
        init(mFontName);
    }

    public IACustomButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mDrawable = getBackground();

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);
        mTypedArray.recycle();
        init(mFontName);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public IACustomButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);

        mDrawable = getBackground();

        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);
        mTypedArray.recycle();
        init(mFontName);
    }

    /**
     * Setting custom typeface
     *
     * @param mFontName
     */

    public void init(String mFontName) {
        if (mFontName != null) {
            try {
                Typeface mTypeface = null;

                if (mFontName.contains("Bold")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Bold.ttf");
                } else if (mFontName.contains("Semibold")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Semibold.ttf");
                } else if (mFontName.contains("Regular")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Regular.ttf");
                } else if (mFontName.contains("Light")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Light.ttf");
                }
                setTypeface(mTypeface);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    @Override
    public void setBackgroundResource(int resid) {
        //    super.setBackgroundResource(resid);

        mDrawable = getResources().getDrawable(resid);
        drawableStateChanged();
    }

    @Override
    public void setBackgroundColor(int color) {
        //    super.setBackgroundColor(color);
        mDrawable = new ColorDrawable(color);
        drawableStateChanged();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();

        int[] states = getDrawableState();

        if (StateSet.stateSetMatches(new int[]{android.R.attr.state_pressed}, states) || StateSet.stateSetMatches(new int[]{android.R.attr.state_focused}, states)) {
            mDrawable.setAlpha(180);
            setBackgroundDrawable(mDrawable);
        } else if (!StateSet.stateSetMatches(new int[]{android.R.attr.state_enabled}, states)) {

        } else {
            mDrawable.setAlpha(255);
            setBackgroundDrawable(mDrawable);
        }
    }

    //		public void setAplhaManully(){
    //			m_drawable.setAlpha(90);
    //			setBackgroundDrawable(m_drawable);
    //		}

}
