package com.findmeashoe_appc.utils;

/**
 * Created by iaurokitkat on 19/12/14.
 */
public interface FMConstantsInterface {

    public int LEFT_IMAGE = 0;
    public int RIGHT_IMAGE = 1;
    public int FULL_IMAGE = 2;
    public int NO_IMAGE = 3;
    public int MAX_LINES_SQUARE_IMAGE = 5;
    public int MAX_LINES_FULL_IMAGE = 2;
    public int MAX_LINES_NO_IMAGE = 5;
    public String logAppName = "NewsIt";
    public String logAppVersion = "1.0";

    //        public String DB_PATH = "/data/data/com.servme/databases/";
    public String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public String UNSUPPORTED_CODING_EXCEPTION = "103";
    public String IO_EXCEPTION = "102";
    public String CLIENT_PROTOCOL_EXCEPTION = "101";
    public String JSON_EXCEPTION = "104";

    //    public int mToastTime = Toast.LENGTH_SHORT;
    public int mToastTime = 500;
    public int NEWS_DISPLAY_TIME = 1000;
    //Boolean variables
    public boolean isDebug = true;


    //Keys for shared preferences
    public String PREFS_ALLNEWS = "allnews";
    public String PREFS_CATAGORIES = "catagories";
    public String PREFS_LANGUAGES = "languages";
    public String PREFS_SOURCES = "sources";
    public String PREFS_DEVICE_ID = "device_id";
    public String PREFS_MODEL = "model";
    public String PREFS_MANUFACTURER = "manufacturer";
    public String PREFS_OPERATING_SYSTEM = "operatingSystem";
    public String PREFS_IS_TABLET = "isTablet";
    public String PREFS_LATITUDE = "longitude";
    public String PREFS_LOGITUDE = "lattitude";
    public String PREFS_APP_ALREADY_INSTALLED = "appAlreadyInstalled";
    public String PREFS_FONT_SELECTED = "fontSelected";
    public String PREF_IS_NEWS_READ = "isNewsRead";
    public String PREFS_CATEGORY_SELECTED = "categorySelected";
    public String PREFS_LANGUAGE_SELECTED = "languageSelected";
    public String PREFS_SOURCE_SELECTED = "sourceSelected";
    public String PREFS_IFRAME = "iFrame";
    public String PREFS_ORIGINAL_HTML = "originalHTML";

    //    keys for font style CSS
    public String CSSFONTSMALL_NORMAL = "fontFamily1_Normal.css";
    public String CSSFONTMEDUIM_NORMAL = "fontFamily2_Normal.css";
    public String CSSFONTLARGE_NORMAL = "fontFamily3_Normal.css";

    //    keys for font style CSS
    public String CSSFONTSMALL_LARGE = "fontFamily1_Large.css";
    public String CSSFONTMEDUIM_LARGE = "fontFamily2_Large.css";
    public String CSSFONTLARGE_LARGE = "fontFamily3_Large.css";

   //    keys for parse
   //    Staging:
       public String PARSE_APP_ID = "yjfVUG9SN3I3pCRxA1wKIUYSsSQyeHUM8kZHy29S";
       public String PARSE_CLIENT_KEY = "HmcGEX6Xzn8hKM7668epsg3qBMiGZEqmLHnQyi2c";

    // Live:​

//    public String PARSE_APP_ID = "yjfVUG9SN3I3pCRxA1wKIUYSsSQyeHUM8kZHy29S";
//    public String PARSE_CLIENT_KEY = "HmcGEX6Xzn8hKM7668epsg3qBMiGZEqmLHnQyi2c";

    //    keys of parameter passed in Intent
    public String TIME_SPENT = "timeSpent";
    public String IS_NEWS_SHARED = "isShared";
    public String NEWS = "news";
    public String NEWS_ID = "newsId";
    public String FEED_ID = "feedId";
    public String TRACK_DETAILS = "trackDetails";
    public String IS_FULL_NEWS_VIEWED = "isFullNewsViewed";

//    Keys for JSON parameters
//    for send device Info

    public String JSON_KEY_HEIGHT = "height";
    public String JSON_KEY_WIDTH = "width";
    public String JSON_KEY_OPERATING_HOURS = "operatingSystem";
    public String JSON_KEY_MANUFACTURER = "manufacturer";
    public String JSON_KEY_MODEL = "model";
    public String JSON_KEY_TYPE = "type";
    public String JSON_KEY_LATITUDE = "latitude";
    public String JSON_KEY_LONGITUDE = "longitude";
    public String JSON_KEY_DEVICE_ID = "deviceId";

//    for send news tracking info

    public String JSON_KEY_TIME_SPENT = "timeSpent";
    public String JSON_KEY_IS_SHARED = "isShared";
    public String JSON_KEY_NEWS_ID = "newsId";
    public String JSON_KEY_FEED_ID = "feedId";
    public String JSON_KEY_IS_FULL_NEWS_VIEWED = "isFullNewsViewed";
}
