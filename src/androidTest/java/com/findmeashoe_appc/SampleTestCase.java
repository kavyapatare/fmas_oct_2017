package com.findmeashoe_appc;

import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.test.InstrumentationTestCase;

/**
 * Created by sayali on 14/4/16.
 */
public class SampleTestCase extends InstrumentationTestCase {
    private UiDevice uiDevice;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        uiDevice = UiDevice.getInstance(getInstrumentation());
    }

    public void testLogin() throws UiObjectNotFoundException{
        uiDevice.pressHome();
//        UiObject mMenuUIButton = new UiObject(new UiSelector().text("Apps"));
//        mMenuUIButton.clickAndWaitForNewWindow();

        UiObject mAppUiButton = new UiObject(new UiSelector().description("findmeashoe"));
        try{
            mAppUiButton.clickAndWaitForNewWindow();
        }catch (Exception e){
            e.printStackTrace();
        }

        UiObject mLocationLoaderTest = new UiObject(new UiSelector().resourceId("android:id/customPanel"));
        try{
            mLocationLoaderTest.waitUntilGone(50000);
        }catch (Exception e){
            e.printStackTrace();
        }

        UiObject mLoginButtonTest = new UiObject(new UiSelector().resourceId("com.findmeashoe_app_c:id/get_started_login_btn"));
        try{
            mLoginButtonTest.exists();
            mLoginButtonTest.clickAndWaitForNewWindow();

        }catch(Exception e){
//            assertTrue(mLoginButtonTest.exists());
        }

        UiObject mLoginEmailTextView = new UiObject(new UiSelector().resourceId("com.findmeashoe_app_c:id/login_user_email_field_tv"));
        try{
            mLoginEmailTextView.setText("a@a.com");
        }catch (Exception e){e.printStackTrace();}

        UiObject mLoginPasswordTextView = new UiObject(new UiSelector().resourceId("com.findmeashoe_app_c:id/login_user_password_field_tv"));
        try{
            mLoginPasswordTextView.setText("a");
        }catch (Exception e){e.printStackTrace();}

        UiObject mLoginSubmitButton = new UiObject(new UiSelector().resourceId("com.findmeashoe_app_c:id/login_start_btn"));
        try{
            mLoginSubmitButton.clickAndWaitForNewWindow();
        }catch (Exception e){e.printStackTrace();}

        UiObject mSeeCatalogButton = new UiObject(new UiSelector().resourceId("com.findmeashoe_app_c:id/see_catalog_btn"));
        try {
            mSeeCatalogButton.clickAndWaitForNewWindow();
        }catch (Exception e){e.printStackTrace();
        }
    }
}
