package com.findmeashoe_appc;

import com.findmeashoe_appc.utils.IAUtilities;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by sayali on 13/4/16.
 */
//@RunWith(MockitoJUnitRunner.class)
public class FMLoginActivityTestCase {

//    @Mock
//    FMLoginActivity fmLoginActivity;

    public FMLoginActivityTestCase(){
    }

//    public void test_addition_isCorrect() throws Exception {
//        Assert.assertEquals(4, 2 + 2);
//    }

    @Test
    public void test_isValidEmail() throws Exception{
//        fmLoginActivity.mLoginUserEmailField.setText("a@a.com");
//        fmLoginActivity.mLoginUserPasswordField.setText("a");
//        Assert.assertNotNull(fmLoginActivity.isValidLogin());
        Assert.assertTrue(new IAUtilities().validateEmailId("a@a.com",null));
    }

    @Test
    public void test_isValidMobileNumber() throws Exception{
        Assert.assertTrue(new IAUtilities().validateMobileNumber("1234567890",null));
    }

    @Test
    public void test_isValidPassword() throws Exception{
        Assert.assertTrue(new IAUtilities().validatePasswordMinimumLength("a",null));
    }

    @Test
    public void test_isBase64Decoded() throws Exception{
        Assert.assertNotNull(new IAUtilities().decodeBase64("dsfSHFSFDGHSH"));
    }

    @Test
    public void test_isDeviceName() throws Exception{
        Assert.assertNotNull(new IAUtilities().getDeviceName());
    }

    @Test
    public void test_isMD5() throws Exception{
        Assert.assertNotNull(new IAUtilities().md5("android"));
    }
}
