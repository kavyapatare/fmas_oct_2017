package com.findmeashoe_appc.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Xml;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomEditText;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;
import org.xmlpull.v1.XmlSerializer;

import java.io.IOException;
import java.io.StringWriter;

public class FMRegisterActivity extends AppCompatActivity {
    private static final String TAG = "FMRegisterActivity";
    private Context mContext;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private FMCustomer mFMCustomer;
    private IACustomEditText mRegisterUserFirstNameField;
    private IACustomEditText mRegisterUserLastNameField;
    private IACustomEditText mRegisterUserEmailField;
    private IACustomEditText mRegisterUserPasswordField;
    private IACustomEditText mRegisterUserConfirmPasswordField;
    private RadioButton mSelectedMaleGender;
    private RadioButton mSelectedFemaleGender;
    private LinearLayout mSelectedMaleGenderLayout;
    private LinearLayout mSelectedFemaleGenderLayout;
    private IACustomButton mRegisterLayoutStartButton;
    private LinearLayout mRegistrationLayout;
    private LinearLayout mRegistrationSuccessfulImageView;
    private Toolbar mToolbarSuccessfulRegistration;
    private IACustomTextView mToolBarSuccessfulRegistrationCustomerNameTextView;
    private LinearLayout mToolbarSuccessfulRegistrationBackButton;
    private LinearLayout mToolbarSuccessfulRegistrationMenuButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the register screen ui
     */
    private void initializeView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_register);
        mToolBarCustomerNameTextView = (IACustomTextView)mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mRegisterLayoutStartButton = (IACustomButton) findViewById(R.id.register_start_btn);
        mRegisterUserFirstNameField = (IACustomEditText) findViewById(R.id.register_user_first_name_field_tv);
        mRegisterUserLastNameField = (IACustomEditText) findViewById(R.id.register_user_last_name_field_tv);
        mRegisterUserEmailField = (IACustomEditText) findViewById(R.id.register_user_email_field_tv);
        mRegisterUserPasswordField = (IACustomEditText) findViewById(R.id.register_user_password_field_tv);
        mRegisterUserConfirmPasswordField = (IACustomEditText) findViewById(R.id.register_user_confirm_password_field_tv);
        mSelectedMaleGender = (RadioButton)findViewById(R.id.selected_male_rbtn);
        mSelectedFemaleGender = (RadioButton)findViewById(R.id.selected_female_rbtn);
        mSelectedMaleGenderLayout = (LinearLayout)findViewById(R.id.selected_male_ll);
        mSelectedFemaleGenderLayout = (LinearLayout)findViewById(R.id.selected_female_ll);
        mRegistrationLayout = (LinearLayout) findViewById(R.id.registration_ll);
        mRegistrationSuccessfulImageView = (LinearLayout) findViewById(R.id.successful_registration_ll);
        mToolbarSuccessfulRegistration = (Toolbar) findViewById(R.id.toolbar_successful_registration);
        mToolBarSuccessfulRegistrationCustomerNameTextView = (IACustomTextView)mToolbarSuccessfulRegistration.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarSuccessfulRegistrationBackButton = (LinearLayout) mToolbarSuccessfulRegistration.findViewById(R.id.back_button_ll);
        mToolbarSuccessfulRegistrationMenuButton = (LinearLayout) mToolbarSuccessfulRegistration.findViewById(R.id.menu_button_ll);
        mRegistrationSuccessfulImageView.setVisibility(View.GONE);

        mToolBarCustomerNameTextView.setText("Register");
        mFMCustomer = new FMCustomer();

        mRegisterLayoutStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRegisterLayoutStartButton.setEnabled(false);
                if (isValidRegistration()) {
                    new IAUtilities().hideKeyboard(v, mContext);
                    onRegisterButtonClick();
                } else {
                    //show appropriate error message
                    new IAUtilities().setSharedPreferences(getBaseContext(), null, "FMCustomer");
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mRegisterLayoutStartButton.setEnabled(true);
                    }
                },5000);
            }
        });
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mSelectedMaleGender.setChecked(true);
        mSelectedMaleGenderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedMaleGender.setChecked(true);
                if (mSelectedFemaleGender.isChecked()) {
                    mSelectedFemaleGender.setChecked(false);
                }
            }
        });
        mSelectedFemaleGenderLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedFemaleGender.setChecked(true);
                if (mSelectedFemaleGender.isChecked()) {
                    mSelectedMaleGender.setChecked(false);
                }
            }
        });
        mSelectedMaleGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedMaleGender.setChecked(true);
                if (mSelectedFemaleGender.isChecked()) {
                    mSelectedFemaleGender.setChecked(false);
                }
            }
        });
        mSelectedFemaleGender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedFemaleGender.setChecked(true);
                if (mSelectedFemaleGender.isChecked()) {
                    mSelectedMaleGender.setChecked(false);
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mRegistrationSuccessfulImageView.getVisibility() == View.VISIBLE){

        } else {
            super.onBackPressed();
        }
    }

    /**
     * called to redirect the user to the one more step screen
     */
    private void goToOneMoreStepActivity() {
        mRegistrationLayout.setVisibility(View.GONE);
        mRegistrationSuccessfulImageView.setVisibility(View.VISIBLE);
        FMCustomer fmCustomer = new FMCustomer();
        fmCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));
        if (!fmCustomer.getCustomerEmailId().equalsIgnoreCase("")){
            mToolBarSuccessfulRegistrationCustomerNameTextView.setText("Hello " + fmCustomer.getCustomerFirstName() + " !");
        }
        mToolbarSuccessfulRegistrationBackButton.setVisibility(View.GONE);
        mToolbarSuccessfulRegistrationMenuButton.setVisibility(View.GONE);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                setResult(RESULT_OK);
                finish();
            }
        },5000);
    }

    /**
     * called when register button is clicked
     */
    private void onRegisterButtonClick() {
        mFMCustomer.setCustomerFirstName(mRegisterUserFirstNameField.getText().toString());
        mFMCustomer.setCustomerLastName(mRegisterUserLastNameField.getText().toString());
        mFMCustomer.setCustomerEmailId(mRegisterUserEmailField.getText().toString());
        if (mSelectedMaleGender.isChecked()) {
            mFMCustomer.setCustomerGender("M");
        } else if (mSelectedFemaleGender.isChecked()) {
            mFMCustomer.setCustomerGender("W");
        }
        String mMD5PasswordString = getResources().getString(R.string.append_password_to_string) + mRegisterUserPasswordField.getText().toString();
        String mMD5EncodedString = new IAUtilities().md5(mMD5PasswordString);
        IAUtilities.showingLogs(TAG, " MD5 result = " + mMD5EncodedString, IAUtilities.LOGS_SHOWN);
        mFMCustomer.setCustomerPassword(mMD5EncodedString);
        new LoginAsyncTask(mFMCustomer,false).execute();

    }

    /**
     * check sign up validation
     * @return
     */
    private boolean isValidRegistration() {
        //check sign up validataion
        if (new IAUtilities().isNetworkAvailable(getApplicationContext())) {
            boolean mFirstNameMinimumLengthFlag = false;
            boolean mFirstNameMaximumLengthFlag = false;

            boolean mLastNameMinimumLengthFlag = false;
            boolean mLastNameMaximumLengthFlag = false;

            boolean mEmailFlag = false;

            boolean mPasswordMinimumLengthFlag = false;
            boolean mPasswordMaximumLengthFlag = false;

            boolean mConfirmPasswordMinimumLengthFlag = false;
            boolean mConfirmPasswordMaximumLengthFlag = false;

            String mRegisterFirstName = mRegisterUserFirstNameField.getText().toString();
            String mRegisterLastName = mRegisterUserLastNameField.getText().toString();
            String mRegisterEmail = mRegisterUserEmailField.getText().toString();
            String mSignUpPassword = mRegisterUserPasswordField.getText().toString();
            String mSignUpConfirmPassword = mRegisterUserConfirmPasswordField.getText().toString();

            mFirstNameMinimumLengthFlag = new IAUtilities().validateNameMinimumLength(mRegisterFirstName, mContext);
            mFirstNameMaximumLengthFlag = new IAUtilities().validateNameMaximumLength(mRegisterFirstName,mContext);

            mLastNameMinimumLengthFlag = new IAUtilities().validateNameMinimumLength(mRegisterLastName, mContext);
            mLastNameMaximumLengthFlag = new IAUtilities().validateNameMaximumLength(mRegisterLastName,mContext);

            mEmailFlag = new IAUtilities().validateEmailId(mRegisterEmail, mContext);

            mPasswordMinimumLengthFlag = new IAUtilities().validatePasswordMinimumLength(mSignUpPassword, mContext);
            mPasswordMaximumLengthFlag = new IAUtilities().validatePasswordMaximumLength(mSignUpPassword, mContext);

            mConfirmPasswordMinimumLengthFlag = new IAUtilities().validatePasswordMinimumLength(mSignUpConfirmPassword, mContext);
            mConfirmPasswordMaximumLengthFlag = new IAUtilities().validateEmailIdMaximumLength(mSignUpConfirmPassword, mContext);

            if (mSignUpPassword.isEmpty() ){
                openDialog(mContext.getResources().getString(R.string.please_enter_data_in_fields));
                return false;
            } else if (mSignUpConfirmPassword.isEmpty() ){
                openDialog(mContext.getResources().getString(R.string.please_enter_data_in_fields));
                return false;
            } else if (!mFirstNameMinimumLengthFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_first_name));
                return false;
            } else if (!mFirstNameMaximumLengthFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_only_up_to_25_characters));
                return false;
            } else if (!mLastNameMinimumLengthFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_last_name));
                return false;
            } else if (!mLastNameMaximumLengthFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_only_up_to_25_characters));
                return false;
            } else if (!mEmailFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_valid_email));
                return false;
            } else if (!mPasswordMinimumLengthFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_valid_password));
                return false;
            } else if (!mPasswordMaximumLengthFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_only_up_to_25_characters));
                return false;
            } else if (!mConfirmPasswordMinimumLengthFlag){
                openDialog(mContext.getResources().getString(R.string.please_reenter_valid_password));
                return false;
            } else if (!mConfirmPasswordMaximumLengthFlag) {
                openDialog(mContext.getResources().getString(R.string.please_enter_only_up_to_25_characters));
                return false;
            } else if (!mSignUpPassword.equals(mSignUpConfirmPassword)){
                openDialog(mContext.getResources().getString(R.string.mismatch_in_the_passwords));
                return false;
            }

            return mFirstNameMinimumLengthFlag && mFirstNameMaximumLengthFlag
                    && mLastNameMinimumLengthFlag && mLastNameMaximumLengthFlag
                    && mEmailFlag
                    && mPasswordMinimumLengthFlag && mPasswordMaximumLengthFlag
                    && mConfirmPasswordMinimumLengthFlag && mConfirmPasswordMaximumLengthFlag;
        } else{
            mRegisterLayoutStartButton.setEnabled(true);
            return false;
        }
    }

    /**
     * called to open a popup message dialog
     * @param mMessageString the message to be shown
     */
    public void openDialog(final String mMessageString) {
        final Dialog dialog = new Dialog(this); // context, this etc.
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.default_dialog);
        TextView mMessageContentView = (TextView) dialog.findViewById(R.id.message_dialog_tv);
        mMessageContentView.setText(mMessageString);
        if (!((AppCompatActivity)mContext).isFinishing()){
            dialog.show();
        }
        IACustomButton mBackToDashboardButton = (IACustomButton) dialog.findViewById(R.id.dialog_done_btn);
        mBackToDashboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mMessageString.contains("Successful")) {
                    IAUtilities.showingLogs(TAG, " mMessageString = " + mMessageString, IAUtilities.LOGS_SHOWN);
                    goToOneMoreStepActivity();
                }
                if (!((AppCompatActivity)mContext).isFinishing()){
                    dialog.dismiss();
                }
            }
        });
    }

    /**
     * used to call the Login api
     */
    private class LoginAsyncTask extends AsyncTask<Void, Void, FMResponse> {

        private ProgressDialog progressDialog;
        private FMCustomer mFMCustomer;
        private boolean mIsRegister;

        public LoginAsyncTask(FMCustomer mFMCustomer,boolean mIsRegister) {
            this.mFMCustomer = mFMCustomer;
            this.mIsRegister = mIsRegister;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMRegisterActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            if (!((AppCompatActivity)mContext).isFinishing()){
                progressDialog.show();
            }
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (mFMCustomer.getCustomerEmailId() != null && mFMCustomer.getCustomerPassword() != null) {
                    jsonObject.put("display", "full");
                    jsonObject.put("filter[email]", mFMCustomer.getCustomerEmailId());
                    jsonObject.put("output_format", "JSON");
                    jsonObject.put("ws_key", mContext.getResources().getString(R.string.ws_key));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse FMResponse = webConnection.getResponse(FMUrl.NEW_LOGIN_URL, jsonObject);
            IAUtilities.showingLogs(TAG, "LoginFmasResponse == " + FMResponse, IAUtilities.LOGS_SHOWN);

            return FMResponse;
        }


        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            if (!((AppCompatActivity)mContext).isFinishing()){
                progressDialog.dismiss();
            }
            final FMCustomer mTempFMCustomer = new FMCustomer(mFMCustomer);
            if (response.isSuccess()) {
                try{
                    String data = response.getResponseData();
                    Object json = new JSONTokener(data).nextValue();
                    if (json instanceof JSONObject){
                        //you have an object
                        JSONObject jsonObject = new JSONObject(String.valueOf(json));
                        IAUtilities.showingLogs(TAG, "jsonObject == " + jsonObject.toString(), IAUtilities.LOGS_SHOWN);

                            openDialog(mContext.getResources().getString(R.string.email_address_is_already_present));
                    } else if (json instanceof JSONArray){
                        //you have an array
                        JSONArray jsonArray = new JSONArray(json.toString());
                        IAUtilities.showingLogs(TAG, "jsonArray == " + jsonArray.toString(), IAUtilities.LOGS_SHOWN);

                        if (jsonArray.length() == 0){
                            mIsRegister = true;
                            new RegisterAsyncTask(mTempFMCustomer,mIsRegister).execute();
                        }
                    }
                } catch (JSONException e){
                    e.printStackTrace();
                }
                //go to next page
            } else {
                //show error message
                openDialog(mContext.getResources().getString(R.string.failed_response));
            }
        }
    }

    /**
     * used to call the Register api
     */
    private class RegisterAsyncTask extends AsyncTask<Void, Void, FMResponse> {

        private ProgressDialog progressDialog;
        private FMCustomer mTempFMCustomer;
        private boolean mIsRegister;

        public RegisterAsyncTask(FMCustomer mFMCustomer,boolean mIsRegister) {
            this.mTempFMCustomer = mFMCustomer;
            this.mIsRegister = mIsRegister;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMRegisterActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            if (!((AppCompatActivity)mContext).isFinishing()){
                progressDialog.show();
            }
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            FMWebConnection webConnection = new FMWebConnection();
            FMResponse FMResponse = webConnection.postCustomerRawData(FMUrl.NEW_REGISTER_URL, writeUsingXMLSerializer());
            IAUtilities.showingLogs(TAG, "RegisterFmasResponse == " + FMResponse, IAUtilities.LOGS_SHOWN);

            if (FMResponse.isSuccess()) {

                IAUtilities.showingLogs(TAG, "register response == " + FMResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
                mIsRegister = true;
                JSONObject jsonObject = new JSONObject();
                try {
                    if (mFMCustomer.getCustomerEmailId() != null && mFMCustomer.getCustomerPassword() != null) {
                        jsonObject.put("display", "full");
                        jsonObject.put("filter[email]", mFMCustomer.getCustomerEmailId());
                        jsonObject.put("filter[passwd]", mFMCustomer.getCustomerPassword());
                        jsonObject.put("output_format", "JSON");
                        jsonObject.put("ws_key", mContext.getResources().getString(R.string.ws_key));

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                FMWebConnection newwebConnection = new FMWebConnection();
                FMResponse newFMResponse = newwebConnection.getResponse(FMUrl.NEW_LOGIN_URL, jsonObject);
                IAUtilities.showingLogs(TAG, "RegisterLoginFmasResponse == " + FMResponse, IAUtilities.LOGS_SHOWN);

                return newFMResponse;
            } else {
                //show error message
                openDialog(mContext.getResources().getString(R.string.failed_response));
                return null;
            }
        }

        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            if (!((AppCompatActivity)mContext).isFinishing()){
                progressDialog.dismiss();
            }
            if (response.isSuccess()) {
                if (response.getResponseData().contains("customers")) {
                    try {
                        JSONObject mJsonOffersListResponseObject = new JSONObject(response.getResponseData());
                        JSONArray mJsonResultObjectsArray = mJsonOffersListResponseObject.getJSONArray("customers");
                        if (mJsonResultObjectsArray != null) {
                            FMCustomer mFMCustomer = new FMCustomer();
                            mFMCustomer.loadData(mJsonResultObjectsArray.getJSONObject(0).toString());
                            new IAUtilities().setSharedPreferences(mContext,mFMCustomer.getCustomerPassword(),"encryptedKey");
                            new IAUtilities().setSharedPreferences(mContext,mJsonResultObjectsArray.getJSONObject(0).toString(), "FMCustomer");
                            goToOneMoreStepActivity();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    openDialog(mContext.getResources().getString(R.string.the_email_address_or_the_password_you_entered_is_not_valid));
                }


            } else {
                //show error message
                openDialog(mContext.getResources().getString(R.string.failed_response));
            }
        }
    }

    /**
     * called to create an xml object string with the valid user details to call
     * the Prestashop login api
     * @return
     */
    private String writeUsingXMLSerializer() {
        XmlSerializer xmlSerializer = Xml.newSerializer();
        StringWriter writer = new StringWriter();

        try {

            xmlSerializer.setOutput(writer);
            // start DOCUMENT
            xmlSerializer.startDocument("UTF-8", true);
            // open tag: <prestashop>
            xmlSerializer.startTag("", "prestashop");
            xmlSerializer.attribute("", "xmlns:xlink", "http://www.w3.org/1999/xlink");
            // open tag: <customer>
            xmlSerializer.startTag("", "customer");

            // open tag: <passwd>
            xmlSerializer.startTag("", "passwd");
            xmlSerializer.text(mRegisterUserPasswordField.getText().toString());
            // close tag: </passwd>
            xmlSerializer.endTag("", "passwd");

            // open tag: <lastname>
            xmlSerializer.startTag("", "lastname");
            xmlSerializer.text(mRegisterUserLastNameField.getText().toString());
            // close tag: </lastname>
            xmlSerializer.endTag("", "lastname");

            // open tag: <firstname>
            xmlSerializer.startTag("", "firstname");
            xmlSerializer.text(mRegisterUserFirstNameField.getText().toString());
            // close tag: </firstname>
            xmlSerializer.endTag("", "firstname");

            // open tag: <email>
            xmlSerializer.startTag("", "email");
            xmlSerializer.text(mRegisterUserEmailField.getText().toString());
            // close tag: </email>
            xmlSerializer.endTag("", "email");

            // open tag: <birthday>
            xmlSerializer.startTag("", "birthday");
            xmlSerializer.text("1987-07-22");
            // close tag: </birthday>
            xmlSerializer.endTag("", "birthday");

            // open tag: <id_gender>
            xmlSerializer.startTag("", "id_gender");

            if (mSelectedMaleGender.isChecked()) {//value sent is numerical not alphabets (NO m,w)
                xmlSerializer.text("1");
            } else if (mSelectedFemaleGender.isChecked()) {
                xmlSerializer.text("2");
            }
            // close tag: </id_gender>
            xmlSerializer.endTag("", "id_gender");

            // open tag: <active>
            xmlSerializer.startTag("", "active");
            xmlSerializer.text(String.valueOf(1));
            // close tag: </active>
            xmlSerializer.endTag("", "active");

            // open tag: <is_guest>
            xmlSerializer.startTag("", "is_guest");
            xmlSerializer.text(String.valueOf(0));
            // close tag: </is_guest>
            xmlSerializer.endTag("", "is_guest");

            // open tag: <id_shop>
            xmlSerializer.startTag("", "id_shop");
            xmlSerializer.text(String.valueOf(1));
            // close tag: </id_shop>
            xmlSerializer.endTag("", "id_shop");

            // open tag: <id_shop_group>
            xmlSerializer.startTag("", "id_shop_group");
            xmlSerializer.text(String.valueOf(1));
            // close tag: </id_shop_group>
            xmlSerializer.endTag("", "id_shop_group");

            // open tag: <associations>
            xmlSerializer.startTag("", "associations");
            // open tag: <groups>
            xmlSerializer.startTag("", "groups");
            xmlSerializer.attribute("", "node_type", "groups");

            // open tag: <groups>
            xmlSerializer.startTag("", "groups");
            xmlSerializer.attribute("", "xlink:href", "http://findmeashoe.in/api/groups/3");

            // open tag: <id>
            xmlSerializer.startTag("", "id");
            xmlSerializer.text(String.valueOf(3));
            // close tag: </id>
            xmlSerializer.endTag("", "id");

            // close tag: </groups>
            xmlSerializer.endTag("", "groups");

            // close tag: </groups>
            xmlSerializer.endTag("", "groups");

            // close tag: </associations>
            xmlSerializer.endTag("", "associations");

            // close tag: </study>
            xmlSerializer.endTag("", "customer");

            // close tag: </record>
            xmlSerializer.endTag("", "prestashop");

            // end DOCUMENT
            xmlSerializer.endDocument();
        } catch (IOException e) {
            e.printStackTrace();
        }
        IAUtilities.showingLogs(TAG, "writer = " + writer.toString(), IAUtilities.LOGS_SHOWN);
        return writer.toString();
    }
}
