package com.findmeashoe_appc.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.LinearLayout;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomEditText;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * unused for now. Can be enabled to give the user additional
 * settings like changing the server ip
 */
public class FMSettingsActivity extends AppCompatActivity {
    public static final String TAG = "FMSettingsActivity";
    private Context mContext;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private FMCustomer mCurrentFMCustomer;
    private IACustomEditText mServerIPEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the settings screen ui
     */
    private void initializeView(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar_settings);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mServerIPEditText = (IACustomEditText) findViewById(R.id.server_ip_et);

        if (new IAUtilities().getSharedPreferences(mContext,"ServerIP").length() > 0){
            JSONObject jsonObject;
            try {
                jsonObject = new JSONObject(new IAUtilities().getSharedPreferences(mContext,"ServerIP"));
                FMUrl.CURRENT_IP_ADDRESS = jsonObject.getString("CurrentIp");
                if (FMUrl.CURRENT_IP_ADDRESS.equalsIgnoreCase("")){
                    FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
                }
                mServerIPEditText.setText(FMUrl.CURRENT_IP_ADDRESS);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
            mServerIPEditText.setText(FMUrl.CURRENT_IP_ADDRESS);
        }

        mServerIPEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("CurrentIp",s);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new IAUtilities().setSharedPreferences(mContext, jsonObject.toString(), "ServerIP");
                IAUtilities.showingLogs(TAG, "Json Object = " + jsonObject.toString(), IAUtilities.LOGS_SHOWN);
                FMUrl.CURRENT_IP_ADDRESS = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        mCurrentFMCustomer = new FMCustomer();
        mCurrentFMCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));

        mToolBarCustomerNameTextView.setText("Hello " + mCurrentFMCustomer.getCustomerFirstName() + " !");
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });
    }
}
