package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;
import com.parse.ParseObject;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import org.json.JSONException;
import org.json.JSONObject;


public class FMOnSuccessfulResponseActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private Context mContext;
    private String APP_ID;
    private FMCustomer mCurrentFMCustomer;
    private IACustomTextView mFootHeightField;
    private IACustomTextView mFootWidthField;
    private IACustomTextView mFootSizeTextField;
    private IACustomTextView mToeBoxTypeTextView;
    private IACustomTextView mToeAngleTypeTextView;
    private String mLocation;
    private String mToeBoxType;
    private String mSmallToeAngle;
    private String mFootHeight;
    private String mFootWidth;
    private String mFootSize;
    private ImageView mToeAngleTypeImageView;
    private ImageView mToeBoxTypeImageView;
    private Bitmap mToeAngleTypeBitmap;
    private Bitmap mToeBoxTypeBitmap;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private LinearLayout mToolbarMenuButton;
    private IACustomButton mRetakePhotosButton;
    private IACustomButton mSeeCatalogButton;
    private JSONObject mFootResultJsonObject;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_successful_response);
        mContext = this;
        initializeView();
        APP_ID = getApplicationContext().getResources().getString(R.string.APP_ID);
        checkForUpdates();
    }

    /**
     * called to initialize and populate the successful result screen with
     * the appropriate values for the user
     */
    private void initializeView() {
        mLocation = new IAUtilities().getSharedPreferences(mContext, "CountryCode");
        try {
            mFootResultJsonObject = new JSONObject(new IAUtilities().getSharedPreferences(mContext, "FootResult"));
            mSmallToeAngle = mFootResultJsonObject.getString("toe_angle");
            mToeBoxType = mFootResultJsonObject.getString("toe_shape");
            if (mLocation.equalsIgnoreCase("IN") || mLocation.equalsIgnoreCase("UK")) {
                mFootSize = mFootResultJsonObject.getString("ukSize");
            } else if (mLocation.equalsIgnoreCase("US") || mLocation.equalsIgnoreCase("CA")) {
                mFootSize = mFootResultJsonObject.getString("usSize");
            }
            mFootHeight = mFootResultJsonObject.getString("L1");
            mFootWidth = mFootResultJsonObject.getString("W1");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mToolbar = (Toolbar) findViewById(R.id.toolbar_on_successful_response);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mToeAngleTypeImageView = (ImageView) findViewById(R.id.toe_angle_type_iv);
        mToeBoxTypeImageView = (ImageView) findViewById(R.id.toe_box_type_iv);
        mFootHeightField = (IACustomTextView) findViewById(R.id.foot_height_tv);
        mFootWidthField = (IACustomTextView) findViewById(R.id.foot_width_tv);
        mFootSizeTextField = (IACustomTextView) findViewById(R.id.foot_size_tv);
        mRetakePhotosButton = (IACustomButton) findViewById(R.id.retake_photos_btn);
        mSeeCatalogButton = (IACustomButton) findViewById(R.id.see_catalog_btn);
        mToeAngleTypeTextView = (IACustomTextView) findViewById(R.id.toe_angle_tv);
        mToeBoxTypeTextView = (IACustomTextView) findViewById(R.id.toe_box_tv);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        int mFootWidthInteger =(int) Math.floor(Double.parseDouble(mFootWidth));
        double mFootWidthFraction = Double.parseDouble(mFootWidth) - mFootWidthInteger;
        String mFootWidthInMm;
        if (mFootWidthFraction >= 0.5){
            mFootWidthInMm = String.format("%.0fmm", Math.ceil(Double.parseDouble(mFootWidth)));
        } else {
            mFootWidthInMm = String.format("%.0fmm", Math.floor(Double.parseDouble(mFootWidth)));
        }
        SpannableString mSpannableFootWidthString=  new SpannableString(mFootWidthInMm);
        mSpannableFootWidthString.setSpan(new RelativeSizeSpan(0.5f), mFootWidthInMm.length() - 2, mFootWidthInMm.length(), 0); // set size
        mFootWidthField.setText(mSpannableFootWidthString);

        int mFootHeightInteger =(int) Math.floor(Double.parseDouble(mFootHeight));
        double mFootHeightFraction = Double.parseDouble(mFootHeight) - mFootHeightInteger;
        String mFootHeightInMm;
        if (mFootHeightFraction >= 0.5){
            mFootHeightInMm = String.format("%.0fmm", Math.ceil(Double.parseDouble(mFootHeight)));
        } else {
            mFootHeightInMm = String.format("%.0fmm", Math.floor(Double.parseDouble(mFootHeight)));
        }
        SpannableString mSpannableFootHeightString=  new SpannableString(mFootHeightInMm);
        mSpannableFootHeightString.setSpan(new RelativeSizeSpan(0.5f), mFootHeightInMm.length() - 2, mFootHeightInMm.length(), 0); // set size
        mFootHeightField.setText(mSpannableFootHeightString);

        int mFootSizeInteger =(int) Math.floor(Double.parseDouble(mFootSize));
        double mFootSizeFraction = Double.parseDouble(mFootSize) - mFootSizeInteger;

        if (mFootSizeFraction >= 0.75){
            mFootSizeTextField.setText(String.format(mLocation + " -right foot: " + "%d¾",mFootSizeInteger));
        } else if (mFootSizeFraction >= 0.5){
            mFootSizeTextField.setText(String.format(mLocation + " -right foot: " + "%d½",mFootSizeInteger));
        } else if (mFootSizeFraction >= 0.25){
            mFootSizeTextField.setText(String.format(mLocation + " -right foot: " + "%d¼",mFootSizeInteger));
        } else {
            mFootSizeTextField.setText(mLocation + " -right foot: " + mFootSize);
        }

        mCurrentFMCustomer = new FMCustomer();
        mCurrentFMCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));

        mToolBarCustomerNameTextView.setText("Hello " + mCurrentFMCustomer.getCustomerFirstName() + " !");

        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });

        mRetakePhotosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRetakePhotosButton.setEnabled(false);
                goToChoosePaperTypeActivity();
                //goToGetStartedActivity();
                mRetakePhotosButton.setEnabled(true);
            }
        });

        mSeeCatalogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSeeCatalogButton.setEnabled(false);
                goToSeeCatalogActivity();
                mSeeCatalogButton.setEnabled(true);
            }
        });

        if (mSmallToeAngle.equalsIgnoreCase(String.valueOf(2))) {
            mToeAngleTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_angle_outward);
            mToeAngleTypeTextView.setText(mContext.getResources().getString(R.string.outward));
        } else if (mSmallToeAngle.equalsIgnoreCase(String.valueOf(3))) {
            mToeAngleTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_angle_inward);
            mToeAngleTypeTextView.setText(mContext.getResources().getString(R.string.inward));
        } else if (mSmallToeAngle.equalsIgnoreCase(String.valueOf(1))) {
            mToeAngleTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_angle_straight);
            mToeAngleTypeTextView.setText(mContext.getResources().getString(R.string.straight));
        } else {
            mToeAngleTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_angle_outward);
            mToeAngleTypeTextView.setText(mContext.getResources().getString(R.string.outward));
        }
        mToeAngleTypeImageView.setImageBitmap(mToeAngleTypeBitmap);

        if (mToeBoxType.equalsIgnoreCase(String.valueOf(2))) {
            mToeBoxTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_box_half_tapered);
            mToeBoxTypeTextView.setText(mContext.getResources().getString(R.string.half_tapered));
        } else if (mToeBoxType.equalsIgnoreCase(String.valueOf(1))) {
            mToeBoxTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_box_tapered);
            mToeBoxTypeTextView.setText(mContext.getResources().getString(R.string.tapered));
        } else if (mToeBoxType.equalsIgnoreCase(String.valueOf(5))) {
            mToeBoxTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_box_squared);
            mToeBoxTypeTextView.setText(mContext.getResources().getString(R.string.square));
        } else {
            mToeBoxTypeBitmap = BitmapFactory.decodeResource(mContext.getResources(),
                    R.drawable.toe_box_half_tapered);
            mToeBoxTypeTextView.setText(mContext.getResources().getString(R.string.half_tapered));
        }
        mToeBoxTypeImageView.setImageBitmap(mToeBoxTypeBitmap);
    }

    /**
     * called to register the crash analytics sdk HockeyApp to check for
     * crashes
     */
    private void checkForCrashes() {
        CrashManager.register(this, APP_ID);
    }

    /**
     * called to register the crash analytics sdk HockeyApp to check for
     * updates after a crash
     */
    private void checkForUpdates() {
        UpdateManager.register(this, APP_ID);
    }

    @Override
    protected void onPause() {
        super.onPause();
        UpdateManager.unregister();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * called to redirect the user to the get started screen for the
     * retake workflow
     */
    private void goToGetStartedActivity()
    {
        setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images)));
        finish();
    }
    private void goToChoosePaperTypeActivity(){
       Intent i=new Intent(FMOnSuccessfulResponseActivity.this,FMChoosePaperTypeActivity.class);
       startActivity(i);
    }
    /**
     * called to take the user to the products list screen
     */
    private void goToSeeCatalogActivity() {
        Intent intent = new Intent(FMOnSuccessfulResponseActivity.this, FMShowCategoryActivity.class);
        startActivity(intent);
    }
}
