package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMError;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;
import com.parse.ParseInstallation;
import com.parse.ParseObject;

import java.util.ArrayList;

public class FMOnUploadUnsuccessfulActivity extends AppCompatActivity {
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private FMCustomer mCurrentFMCustomer;
    private Context mContext;
    private IACustomButton mRetakePhotosButton;
    private IACustomButton mResendPhotosButton;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private LinearLayout mToolbarMenuButton;
    private ImageView mErrorImageView;
    private IACustomTextView mErrorCodeTextViewField;
    private IACustomTextView mErrorTextViewField;
    private IACustomTextView mErrorExtensionTextViewField;
    private ArrayList<FMError> mErrorsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_on_upload_unsuccessful);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the upload unsuccessful screen
     */
    private void initializeView(){
        Intent intent = getIntent();
        String mErrorCode = intent.getStringExtra("error code");
        String mErrorRemark = intent.getStringExtra("remark");
        boolean mIsCancelled = intent.getBooleanExtra("is cancelled",false);

        mErrorsList = new ArrayList<>();

        mErrorsList.add(new FMError("ERR_000","success",-1,""));
        mErrorsList.add(new FMError("ERR_DB1",mContext.getResources().getString(R.string.oops),R.drawable.err_2,mContext.getResources().getString(R.string.sorry_something_wrong)));
        mErrorsList.add(new FMError("ERR_DB2",mContext.getResources().getString(R.string.oops),R.drawable.err_2,mContext.getResources().getString(R.string.sorry_something_wrong)));
        mErrorsList.add(new FMError("ERR_DJ1",mContext.getResources().getString(R.string.oops),R.drawable.err_2,mContext.getResources().getString(R.string.sorry_something_wrong)));
        mErrorsList.add(new FMError("ERR_CN1",mContext.getResources().getString(R.string.oops),R.drawable.err_2,mContext.getResources().getString(R.string.sorry_something_wrong)));

        mErrorsList.add(new FMError("ERR_121",mContext.getResources().getString(R.string.paper_edge_missing),R.drawable.err_3,mContext.getResources().getString(R.string.oops_paper_needs_to_fit)));
        mErrorsList.add(new FMError("ERR_221",mContext.getResources().getString(R.string.paper_edge_missing),R.drawable.err_3,mContext.getResources().getString(R.string.oops_paper_needs_to_fit)));

        mErrorsList.add(new FMError("ERR_112",mContext.getResources().getString(R.string.paper_missing),R.drawable.err_4,mContext.getResources().getString(R.string.paper_should_fit_in_green_rectangle)));
        mErrorsList.add(new FMError("ERR_212",mContext.getResources().getString(R.string.paper_missing),R.drawable.err_4,mContext.getResources().getString(R.string.paper_should_fit_in_green_rectangle)));

        mErrorsList.add(new FMError("ERR_131",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_231",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_132",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_232",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_133",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_233",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_134",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_234",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_141",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_142",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_144",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_251",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));
        mErrorsList.add(new FMError("ERR_252",mContext.getResources().getString(R.string.blurred),R.drawable.err_5,mContext.getResources().getString(R.string.uh_oh_hands_shook)));

        mErrorsList.add(new FMError("ERR_111",mContext.getResources().getString(R.string.camera_too_far),R.drawable.err_6,mContext.getResources().getString(R.string.please_hold_camera_lower)));
        mErrorsList.add(new FMError("ERR_211",mContext.getResources().getString(R.string.camera_too_far),R.drawable.err_6,mContext.getResources().getString(R.string.please_hold_camera_lower)));
        mErrorsList.add(new FMError("ERR_213",mContext.getResources().getString(R.string.camera_too_far),R.drawable.err_6,mContext.getResources().getString(R.string.please_hold_camera_lower)));
        mErrorsList.add(new FMError("ERR_143",mContext.getResources().getString(R.string.camera_too_far),R.drawable.err_6,mContext.getResources().getString(R.string.please_hold_camera_lower)));
        mErrorsList.add(new FMError("ERR_999",mContext.getResources().getString(R.string.camera_too_far),R.drawable.err_6,mContext.getResources().getString(R.string.please_hold_camera_lower)));

        mErrorsList.add(new FMError("ERR_119",mContext.getResources().getString(R.string.oops),R.drawable.err_7,mContext.getResources().getString(R.string.uh_oh_something_is_not_right_here)));
        mErrorsList.add(new FMError("ERR_219",mContext.getResources().getString(R.string.oops),R.drawable.err_7,mContext.getResources().getString(R.string.uh_oh_something_is_not_right_here)));

        mToolbar = (Toolbar) findViewById(R.id.toolbar_on_upload_unsuccessful);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mResendPhotosButton = (IACustomButton) findViewById(R.id.resend_photos_btn);
        mRetakePhotosButton = (IACustomButton) findViewById(R.id.retake_photos_btn);
        mErrorImageView = (ImageView) findViewById(R.id.error_iv);
        mErrorCodeTextViewField = (IACustomTextView) findViewById(R.id.error_code_tv);
        mErrorTextViewField = (IACustomTextView) findViewById(R.id.error_tv);
        mErrorExtensionTextViewField = (IACustomTextView) findViewById(R.id.error_extension_tv);

        mResendPhotosButton.setVisibility(View.GONE);
        mErrorCodeTextViewField.setVisibility(View.GONE);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        mCurrentFMCustomer = new FMCustomer();
        mCurrentFMCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));

        mToolBarCustomerNameTextView.setText("Hello " + mCurrentFMCustomer.getCustomerFirstName() + " !");
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });


        mResendPhotosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mResendPhotosButton.setEnabled(false);
                goToUploadImagesRevisedActivity();
                mResendPhotosButton.setEnabled(true);
            }
        });

        mRetakePhotosButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRetakePhotosButton.setEnabled(false);
                goToGetStartedActivity();
                mRetakePhotosButton.setEnabled(true);
            }
        });
        if (mErrorCode != null){
            checkErrorType(mErrorCode, mErrorRemark);
            if (!mIsCancelled){
                sendParseObject(mErrorCode);
            } else{

                mErrorCodeTextViewField.setVisibility(View.GONE);
                mErrorExtensionTextViewField.setVisibility(View.VISIBLE);
                mErrorTextViewField.setText(mContext.getResources().getString(R.string.sending_cancelled));
                mErrorTextViewField.setVisibility(View.VISIBLE);
                mErrorExtensionTextViewField.setText(getResources().getString(R.string.we_did_not_receive));
            }
        }
    }

    /**
     * called to check the appropriate error code and the corresponding remarks
     * @param mErrorCode
     * @param mErrorRemark
     */
    private void checkErrorType(String mErrorCode,String mErrorRemark){
        int mCount;
        for (mCount = 0;mCount < mErrorsList.size();mCount++){
            if (mErrorCode.equalsIgnoreCase(mErrorsList.get(mCount).getErrorCode())){
                break;
            }
        }
        if (mErrorRemark.length() <= 0
                || mCount == mErrorsList.size()
                || mErrorCode.equalsIgnoreCase(getResources().getString(R.string.network_error))){

            mErrorTextViewField.setText(getResources().getString(R.string.no_network));
            mErrorExtensionTextViewField.setText(getResources().getString(R.string.oops_please_check_connectivity));
            mErrorImageView.setImageResource(R.drawable.err_1);
            if (!new IAUtilities().isNetworkAvailable(mContext)){
                mErrorExtensionTextViewField.setText(getResources().getString(R.string.oops_please_check_connectivity));
                mErrorExtensionTextViewField.setVisibility(View.VISIBLE);
                mResendPhotosButton.setVisibility(View.VISIBLE);
                mErrorImageView.setImageResource(R.drawable.err_1);
            }

            if (mErrorCode.length() > 0 && new IAUtilities().isNetworkAvailable(mContext)){
                if (mErrorCode.equalsIgnoreCase("ERR_HTML")){
                    mErrorCodeTextViewField.setVisibility(View.GONE);
                }else{
                    mErrorCodeTextViewField.setText(mErrorCode);
                    mErrorCodeTextViewField.setVisibility(View.VISIBLE);
                }
            }

        } else{
            mErrorTextViewField.setText(mErrorRemark);
            mErrorImageView.setImageResource(mErrorsList.get(mCount).getErrorImage());
            mErrorExtensionTextViewField.setText(mErrorsList.get(mCount).getErrorDescription());
            mErrorExtensionTextViewField.setVisibility(View.VISIBLE);
        }


    }

    /**
     * called to send the parse object to the parse server or save it
     * in background
     * @param mErrorCode
     */
    private void sendParseObject(String mErrorCode){
        ParseObject mFeedbackObject = new ParseObject("ErrorLogs");

        mFeedbackObject.put("DeviceInfo",new IAUtilities().getDeviceName() + " Android Version : " + Build.VERSION.RELEASE);
        mFeedbackObject.put("DeviceType","android");
        mFeedbackObject.put("InstallationId", ParseInstallation.getCurrentInstallation().getInstallationId());
        mFeedbackObject.put("EmailId",mCurrentFMCustomer.getCustomerEmailId());
        mFeedbackObject.put("UserName",mCurrentFMCustomer.getCustomerFirstName());
        if (!new IAUtilities().isNetworkAvailable(mContext)){
            mFeedbackObject.put("ErrorCode",getResources().getString(R.string.network_error));
            mFeedbackObject.put("ErrorRemark",mErrorTextViewField.getText().toString() + mErrorExtensionTextViewField.getText().toString());
            mFeedbackObject.saveEventually();
        }else{
            mFeedbackObject.put("ErrorCode",mErrorCode);
            mFeedbackObject.put("ErrorRemark",mErrorTextViewField.getText().toString());
            mFeedbackObject.saveInBackground();
        }


    }

    @Override
    public void onBackPressed() {
        setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_images)));
        finish();
    }

    /**
     * called to redirect the user to the get started screen
     */
    private void goToGetStartedActivity() {
        setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_images)));
        finish();
    }

    /**
     * called to redirect the user to re upload the images
     */
    private void goToUploadImagesRevisedActivity(){
        setResult(Integer.parseInt(mContext.getResources().getString(R.string.resend_images)));
        finish();
    }
}
