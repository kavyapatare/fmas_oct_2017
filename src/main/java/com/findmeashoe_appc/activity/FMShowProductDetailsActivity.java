package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.adapters.FMProductDetailsPagerAdapter;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMProduct;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;

import java.util.ArrayList;

public class FMShowProductDetailsActivity extends AppCompatActivity {
    private static final String TAG = FMShowProductDetailsActivity.class.getSimpleName();
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private Context mContext;
    private FMCustomer mCurrentCustomer;
    private ViewPager mProductDetailsPager;
    private FMProductDetailsPagerAdapter mProductDetailsPagerAdapter;
    private IACustomButton mProductBuyMeButton;
    private ArrayList<FMProduct> mProductsList;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private ImageView mPreviousProductImageView;
    private ImageView mNextProductImageView;
    private int index = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_product_details);
        initializeView();
    }

    /**
     * called to initialize the products details screen ui
     */
    private void initializeView(){
        mContext = this;
        Bundle extras = getIntent().getExtras();
        mProductsList = (ArrayList<FMProduct>) extras.getSerializable("products");
        index = extras.getInt("current");
        mToolbar = (Toolbar) findViewById(R.id.toolbar_see_products);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mProductDetailsPager = (ViewPager) findViewById(R.id.product_details_pager);
        mProductBuyMeButton = (IACustomButton) findViewById(R.id.product_details_buy_me_btn);
        mPreviousProductImageView = (ImageView) findViewById(R.id.product_details_previous_product_btn);
        mNextProductImageView = (ImageView) findViewById(R.id.product_details_next_product_btn);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        mCurrentCustomer = new FMCustomer();
        mCurrentCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));
        if (!mCurrentCustomer.getCustomerEmailId().equalsIgnoreCase("")) {
            mToolBarCustomerNameTextView.setText("Hello " + mCurrentCustomer.getCustomerFirstName() + " !");
        }

        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });

        mProductBuyMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent;
                String mCurrentProductLink = Html.fromHtml(mProductsList.get(index).getProductBuyLink()).toString();
                    if(mCurrentProductLink.startsWith("http://")){
                        browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mCurrentProductLink));
                    }else{
                        browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://" + mCurrentProductLink));
                    }
                    startActivity(browserIntent);
            }
        });

        mProductDetailsPagerAdapter = new FMProductDetailsPagerAdapter(getSupportFragmentManager(),mProductsList);
        mProductDetailsPager.setAdapter(mProductDetailsPagerAdapter);
        mProductDetailsPager.setCurrentItem(index);

        if (index == 0){
            mPreviousProductImageView.setVisibility(View.GONE);
        } else if (index == mProductsList.size() - 1){
            mNextProductImageView.setVisibility(View.GONE);
        }

        if (mProductsList.get(index).getProductBuyLink().isEmpty()){
            mProductBuyMeButton.setEnabled(false);
        } else {
            mProductBuyMeButton.setEnabled(true);
        }


        mPreviousProductImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProductDetailsPager.setCurrentItem(mProductDetailsPager.getCurrentItem() - 1);
            }
        });

        mNextProductImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mProductDetailsPager.setCurrentItem(mProductDetailsPager.getCurrentItem() + 1);
            }
        });

        mProductDetailsPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                index = position;
                if(index == 0) {
                    mPreviousProductImageView.setVisibility(View.GONE);
                    mNextProductImageView.setVisibility(View.VISIBLE);
                } else if(index == mProductsList.size() - 1) {
                    mPreviousProductImageView.setVisibility(View.VISIBLE);
                    mNextProductImageView.setVisibility(View.GONE);
                } else {
                    mPreviousProductImageView.setVisibility(View.VISIBLE);
                    mNextProductImageView.setVisibility(View.VISIBLE);
                }
                if (mProductsList.get(position).getProductBuyLink().isEmpty()){
                    mProductBuyMeButton.setEnabled(false);
                } else {
                    mProductBuyMeButton.setEnabled(true);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

    }
}
