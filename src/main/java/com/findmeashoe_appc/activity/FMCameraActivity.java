package com.findmeashoe_appc.activity;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.fragments.FMCamera2BasicFragment;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.utils.SensorFusion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import static com.findmeashoe_appc.activity.FMCameraActivityForDoublePaper.HEIGHT_BALL;
import static java.lang.Math.abs;
import static java.lang.Math.floor;

public class FMCameraActivity extends AppCompatActivity implements Camera.PictureCallback, SurfaceHolder.Callback, View.OnClickListener, SensorEventListener {

    public static final int IS_HYSTERESIS_ON=0;
    public boolean isInside=false;
    public static int WIDTH_BALL;
    public static int HEIGHT_BALL;
    public int WIDTH ,LEFT_VIEW_WIDTH,RIGHT_VIEW_WIDTH,LEFT_OF_RIGHT_VIEW,CROP_VIEW_WIDTH,CROP_VIEW_HEIGHT,VERTICAL_GAUZE_HEIGHT,VERTICAL_GAUZE_WIDTH;
    public int HEIGHT,CAMERA_PREVIEW_WIDTH,CAMERA_PREVIEW_HEIGHT,CAMERA_PREVIEW_TOP,LEFT_OF_CROP_VIEW,TOP_OF_CROP_VIEW,HORIZONTAL_GAUZE_HEIGHT,HORIZONTAL_GAUZE_WIDTH ;
    public int captureRegionWidth,captureRegionHeight;
    private TextView diffInValues;
    private boolean ready = false;
    private float[] accelValues = new float[3];
    private float[] compassValues = new float[3];
    private float[] gyroValues = new float[3];

    private float[] inR = new float[9];
    private float[] inclineMatrix = new float[9];
    private float[] outR = new float[9];
    private float[] orientationValues = new float[3];
    private ArrayList<Float> mPitchRecords;
    private ArrayList<Float> mRollRecords;
    public static String tvReading=null,tsvReading=null,svReading=null;
    public String allReading;

    private float prevX = 0.0f,prevY = 0.0f;
    private int counter;

    Display mDisplay;
    float ballx, bally;
    float minXVertical = HORIZONTAL_GAUZE_WIDTH/2 - 5;
    float minYHorizontal =VERTICAL_GAUZE_HEIGHT/2 - 5;
    float minXHorizontal = 5;
    float prevPitch=0.0f,prevRoll=0.0f;
    long current_time,prev_time=0;

    float sideViewlinePos=0.4f;
    public static final double TO_DEGREES = 180 / Math.PI;
    ViewTreeObserver obs,leftViewObs,rightViewObs,cameraViewObs,cropViewObs;

    private Sensor accel;
    private Sensor compass;
    private Sensor gyro;
    private Sensor orient;

    private static final String TAG = "FMCameraActivity";
    private static final String KEY_IS_CAPTURING = "is_capturing";
    private Context mContext;
    // for camera
    private Camera mCamera;
    private SurfaceView mCameraPreview;
    private byte[] mCameraData;
    private boolean mIsCapturing;
    private Bitmap mCameraBitmap;
    private Bitmap mCroppedBitmapBmp;
    private FMCamera2BasicFragment fmCamera2BasicFragment;
    private boolean mIsSafeToCapturePicture = false;
    private AudioManager mCameraShutterAudioManager;

    // for the screen overlay
    private ImageView mCaptureImageButton;
    private LinearLayout mBackButton;
    private ImageView mCaptureTopViewButton;
    private ImageView mCaptureAngleViewButton;
    private ImageView mCaptureSideViewButton;
    private RelativeLayout mHorizontalGuaze;
    private RelativeLayout mVerticalGuaze,mVerticalLowerGuaze;
    private RelativeLayout mCropViewLayout;
    private RelativeLayout sideViewLine;
    private RelativeLayout    innerRegionOfInterest,innerRegionOfInterest2;
    private Camera.Parameters parameters;
    private String mFileStoragePath;
    private RelativeLayout mMainCameraOverlayLayout;
    private int mCurrentButtonIndex;
    private boolean mRetakeFlagEnabled;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private LinearLayout mMenuButton;
    private int currentApiVersion;

    // for sensor
    private SensorManager mSensorManager;
    private final float[] mAccelerometerReading = new float[3];
    private final float[] mMagnetometerReading = new float[3];

    private final float[] mRotationMatrix = new float[9];
    private final float[] mOrientationAngles = new float[3];

    public boolean isGyroPresent=false;

    private Sensor mMagnetometerSensor;
    private Sensor mAccelerometerSensor;

    private TextView mSensorChangedTextView1;
    private TextView mSensorChangedTextView2;
    private TextView mSensorChangedTextView3;
    private TextView mSensorChangedTextView4;

    private View mSensorChangeValueImageView,mSensorChangeValueImageView2,mSensorChangeValueImageView3;
    //    private View mSensorChangeValueImageView;
    private static final double RAD_TO_DEG = 180.0f / Math.PI;
    private static int SENSOR_DELAY =20000;
    public int animDuration=210;

    private double pitch = 0,prev_pitch=0;
    private double roll = 0,prev_roll=0;
    private double yaw = 0;
    private float stepMoveFactor = 25.0f;
    private int moveX = 0;
    private int moveY = 0;
    private int x = 0;
    private int y = 0;

    private int mMinXBoundForVerticalGuazeBall = 0;
    private static int mMaxXBoundForVerticalGuazeBall = 0;
    private static int mMinYBoundForVerticalGuazeBall= 0;
    private static int mMaxYBoundForVerticalGuazeBall = 0;

    private static int xCenter = 0;
    private static int yCenter = 0;


    private View mOverlayViewLeft;
    private View mOverlayViewRight;
    private DisplayMetrics displayMetrics;

    /**
     * This variables used for calculate valid pitch and roll
     * using this variables we have to change the color of ball and the color pitch and roll text
     */
    private float optimalPitch = 0.0f, optimalRoll = 0.0f, maxOffPitch = 0.0f, maxOffRoll = 0.0f, criticalPitch = 0.0f, criticalRoll = 0.0f;
    private float validPitch = 0.0f, validRoll = 0.0f;
    private float validPitch_max = 0.0f, validPitch_min = 0.0f;
    private float validRoll_max = 0.0f, validRoll_min = 0.0f;

    private float optimalPitch_tv = -4.0f;
    private float optimalRoll_tv = 2.0f;
    private float maxOffPitch_tv = 20.0f;
    private float maxOffRoll_tv = 15.0f;
    private float criticalPitch_tv = 10.0f;
    private float criticalRoll_tv = 5.0f;
    private float validPitch_tv = 3.0f;
    private float validRoll_tv = 3.0f;


    private float optimalPitch_tsv = -36.0f;
    //    self.optimalRoll_tsv = 4.0;
    private float optimalRoll_tsv = 3.0f;
    private float maxOffPitch_tsv = 30.0f;
    private float maxOffRoll_tsv = 15.0f;
    private float criticalPitch_tsv = 10.0f;
    private float criticalRoll_tsv = 5.0f;
    private float validPitch_tsv = 2.5f;
    private float validRoll_tsv = 3.0f;


    private float optimalPitch_sv = -2.0f;
    private float optimalRoll_sv = -4.0f;
    private float maxOffPitch_sv = 20.0f;
    private float maxOffRoll_sv = 15.0f;
    private float criticalPitch_sv = 10.0f;
    private float criticalRoll_sv = 5.0f;
    private float validPitch_sv = 3.0f;
    private float validRoll_sv = 3.0f;

    // End of the values


    /**
     * Sensor Fusion class instance
     */
    private SensorFusion sensorFusion;
    private SensorManager sensorManager = null;

    /**
     * Decimal Format to show the pitch and roll values
     */
    private DecimalFormat d = new DecimalFormat("#.##");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        WindowManager window = (WindowManager) this.getSystemService(WINDOW_SERVICE);
        mDisplay = window.getDefaultDisplay();
        setContentView(R.layout.activity_camera);

        // initFusionSensor();

        sensorManager= (SensorManager) this.getSystemService(SENSOR_SERVICE);
        SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, outR);

        initializeView(savedInstanceState);
        if (mIsCapturing)
            startOldCameraFunction();
        initializeSensor();
        registerSensorManagerListeners();
    }

    @Override
    public void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }


    /**
     * called to initialize the camera screen and the screen ui
     *

     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void initializeView(Bundle savedInstanceState) {
        currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.KITKAT) {
            final int flags = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;


            getWindow().getDecorView().setSystemUiVisibility(flags);
            final View decorView = getWindow().getDecorView();
            decorView
                    .setOnSystemUiVisibilityChangeListener(new View.OnSystemUiVisibilityChangeListener() {

                        @Override
                        public void onSystemUiVisibilityChange(int visibility) {
                            if ((visibility & View.SYSTEM_UI_FLAG_FULLSCREEN) == 0) {
                                decorView.setSystemUiVisibility(flags);
                            }
                        }
                    });
        }

        mPitchRecords = new ArrayList<>();
        mRollRecords = new ArrayList<>();

        mMainCameraOverlayLayout = (RelativeLayout) findViewById(R.id.main_camera_overlay_rl);

        mCameraPreview = (SurfaceView) findViewById(R.id.preview_view);
        mCaptureImageButton = (ImageView) findViewById(R.id.capture_image_btn);
        mCaptureTopViewButton = (ImageView) findViewById(R.id.top_view_btn);
        mCaptureAngleViewButton = (ImageView) findViewById(R.id.angle_view_btn);
        mCaptureSideViewButton = (ImageView) findViewById(R.id.side_view_btn);
        mBackButton = (LinearLayout) findViewById(R.id.back_button_ll);
        mCropViewLayout = (RelativeLayout) findViewById(R.id.crop_view_rl);
        mVerticalGuaze = (RelativeLayout) findViewById(R.id.vertical_guaze_view);
        mVerticalLowerGuaze = (RelativeLayout) findViewById(R.id.vertical_guaze__lower_view);
        mHorizontalGuaze = (RelativeLayout) findViewById(R.id.horizontal_guaze_view);
        innerRegionOfInterest = (RelativeLayout) findViewById(R.id.inner_view_rl);
        innerRegionOfInterest2 = (RelativeLayout) findViewById(R.id.inner_view_rl2);
        sideViewLine = (RelativeLayout) findViewById(R.id.lineView);
        mMenuButton = (LinearLayout) findViewById(R.id.menu_button_ll);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
        mFileStoragePath = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                File.separator + "com.findmeashoe_appc.camera" + File.separator;

        IAUtilities.showingLogs(TAG, "mFileStoragePath = " + mFileStoragePath, IAUtilities.LOGS_SHOWN);
        final SurfaceHolder surfaceHolder = mCameraPreview.getHolder();
        surfaceHolder.addCallback(this);
        surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        mCaptureImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCaptureImageButton.setEnabled(false);
                if (mCaptureTopViewButton.isEnabled() && mCaptureAngleViewButton.isEnabled() && mCaptureSideViewButton.isEnabled()) {
                    mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.next_button_selector));
                    goToUploadImagesRevisedActivity();
                } else {
                    captureImage();
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mCaptureImageButton.setEnabled(true);
                    }
                }, 3000);
            }
        });
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mIsCapturing = true;

        mMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });


        mCaptureTopViewButton.setOnClickListener(this);
        mCaptureAngleViewButton.setOnClickListener(this);
        mCaptureSideViewButton.setOnClickListener(this);

        IAUtilities.showingLogs(TAG, "CurrentFileIndexNo = " + new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo"), IAUtilities.LOGS_SHOWN);

        if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(1))) {
            mCurrentButtonIndex = 1;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(true);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);

        } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(2))) {
            mCurrentButtonIndex = 2;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(true);
            mCaptureSideViewButton.setEnabled(false);
        } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(3))) {
            mCurrentButtonIndex = 3;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(true);
        } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(4))) {
            mCurrentButtonIndex = 1;
            mRetakeFlagEnabled = true;
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(true);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
        } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(5))) {
            mCurrentButtonIndex = 2;
            mRetakeFlagEnabled = true;
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(true);
            mCaptureSideViewButton.setEnabled(false);
        } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(6))) {
            mCurrentButtonIndex = 3;
            mRetakeFlagEnabled = true;
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(true);
        } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(7))) {
//            mCurrentButtonIndex = 4;
//            mRetakeFlagEnabled = true;
//            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
//            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
//            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
//            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.next_button_selector));
//            mCaptureTopViewButton.setEnabled(true);
//            mCaptureAngleViewButton.setEnabled(true);
//            mCaptureSideViewButton.setEnabled(true);
            mCurrentButtonIndex = 1;
            mRetakeFlagEnabled = false;
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCaptureTopViewButton.setEnabled(true);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
        }


        mMainCameraOverlayLayout.setVisibility(View.VISIBLE);

    }

    /**
     * unused due to very less number camera 2 api hardware devices
     *
     * @param savedInstanceState
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public void startNewCameraFunction(Bundle savedInstanceState) {
        if (null == savedInstanceState) {
            fmCamera2BasicFragment = FMCamera2BasicFragment.newInstance();
            getFragmentManager().beginTransaction()
                    .replace(R.id.camera_frame, fmCamera2BasicFragment)
                    .commit();
        }
    }

    /**
     * @param context
     * @return the navigation bar height
     */
    public int getNavBarHeight(Context context) {
        int result = 0;
        boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);

        if (!hasMenuKey && !hasBackKey) {
            Resources resources = context.getResources();

            int orientation = getResources().getConfiguration().orientation;
            int resourceId;
            if (isTablet(context)) {
                resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_height_landscape", "dimen", "android");
            } else {
                resourceId = resources.getIdentifier(orientation == Configuration.ORIENTATION_PORTRAIT ? "navigation_bar_height" : "navigation_bar_width", "dimen", "android");
            }

            if (resourceId > 0) {
                return getResources().getDimensionPixelSize(resourceId);
            }
        }
        return result;
    }

    /**
     * called to check whether the device is a tablet or not
     *
     * @param context
     * @return
     */
    private boolean isTablet(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    /**
     * called to calculate the aspect ratio of the size
     *
     * @param mAspectRatioSize
     * @return
     */
    private double calculateAspectRatio(Camera.Size mAspectRatioSize) {
        double mAspectRatio = (double) mAspectRatioSize.width / mAspectRatioSize.height;
        return mAspectRatio;
    }

    /**
     * called to start the camera
     */
    public void startOldCameraFunction() {
        setWidthAccToHeight();
        if (mCamera == null) {
            try {

                mCamera = Camera.open(0);

                DisplayMetrics displaymetrics = new DisplayMetrics();
                getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
                IAUtilities.showingLogs(TAG, "device width = " + displaymetrics.widthPixels, IAUtilities.LOGS_SHOWN);
                parameters = mCamera.getParameters();
                List<Camera.Size> mPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();

                Collections.sort(mPreviewSizes, new Comparator<Camera.Size>() {
                    @Override
                    public int compare(Camera.Size size, Camera.Size t1) {
                        return size.height * size.width - t1.height * t1.width;
                    }
                });

                Camera.Size mOptimalPreviewSize = mCamera.new Size(0, 0);
                int mCount;
                for (mCount = 0; mCount < mPreviewSizes.size(); mCount++) {
                    mOptimalPreviewSize.width = mPreviewSizes.get(mCount).width;
                    mOptimalPreviewSize.height = mPreviewSizes.get(mCount).height;
                    if (mPreviewSizes.get(mCount).width > (600 * displaymetrics.scaledDensity)) {
                        mOptimalPreviewSize = mPreviewSizes.get(mCount);
                        IAUtilities.showingLogs(TAG, "optimal preview width = " + mOptimalPreviewSize.width + " , optimal preview height = " + mOptimalPreviewSize.height, IAUtilities.LOGS_SHOWN);
                        break;
                    }
                }

                double mPreviewSizeAspectRatio = calculateAspectRatio(mOptimalPreviewSize);
                IAUtilities.showingLogs(TAG, "mPreviewSizeAspectRatio = " + mPreviewSizeAspectRatio, IAUtilities.LOGS_SHOWN);
                FrameLayout.LayoutParams mCameraPreviewParams = new
                        FrameLayout.LayoutParams((int) (mPreviewSizeAspectRatio * getWindowManager().getDefaultDisplay().getHeight())
                        , getWindowManager().getDefaultDisplay().getHeight());

                mCameraPreview.setLayoutParams(mCameraPreviewParams);

                parameters.setPreviewSize(mOptimalPreviewSize.width, mOptimalPreviewSize.height);
                List<Camera.Size> mPictureSizes = mCamera.getParameters().getSupportedPictureSizes();

                Collections.sort(mPictureSizes, new Comparator<Camera.Size>() {
                    @Override
                    public int compare(Camera.Size size, Camera.Size t1) {
                        return size.height * size.width - t1.height * t1.width;
                    }
                });

                Camera.Size mOptimalPictureSize = mCamera.new Size(0, 0);
                for (mCount = 0; mCount < mPictureSizes.size(); mCount++) {
                    mOptimalPictureSize.width = mPictureSizes.get(mCount).width;
                    mOptimalPictureSize.height = mPictureSizes.get(mCount).height;
                    IAUtilities.showingLogs(TAG, "optimal image width = " + mOptimalPictureSize.width + " , optimal image height = " + mOptimalPictureSize.height, IAUtilities.LOGS_SHOWN);

                    if (mPictureSizes.get(mCount).width > 1000
                            && mPreviewSizeAspectRatio <= calculateAspectRatio(mOptimalPictureSize)) {
                        mOptimalPictureSize = mPictureSizes.get(mCount);
                        break;
                    }
                }
                parameters.setPictureSize(mOptimalPictureSize.width, mOptimalPictureSize.height);

                parameters.setColorEffect(Camera.Parameters.EFFECT_NONE);

                mCamera.setParameters(parameters);
                List<String> focusModes = parameters.getSupportedFocusModes();
                if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
                    parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                }
                if (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
                    if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(7))) {
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                    } else {
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    }
                } else {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }
                mCamera.setParameters(parameters);
                mCamera.setPreviewDisplay(mCameraPreview.getHolder());
                if (mIsCapturing) {
                    mCamera.startPreview();
                }

            } catch (IOException e) {
                Toast.makeText(FMCameraActivity.this, "Unable to open camera.", Toast.LENGTH_LONG)
                        .show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putBoolean(KEY_IS_CAPTURING, mIsCapturing);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mIsCapturing = savedInstanceState.getBoolean(KEY_IS_CAPTURING, mCameraData == null);
        sensorManager= (SensorManager) this.getSystemService(SENSOR_SERVICE);
        SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, outR);
        startOldCameraFunction();
        initializeSensor();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * called when a picture is captured
     *
     * @param data   the byte array containing the image data
     * @param camera
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onPictureTaken(byte[] data, Camera camera) {
        mCameraData = data;
        if (!mRetakeFlagEnabled) {
            if (mCurrentButtonIndex == 1) {
                mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                saveImageTypeFile(mCurrentButtonIndex);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(2), "CurrentFileIndexNo");
                gotoPreviousActivity();
            } else if (mCurrentButtonIndex == 2) {
                mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                saveImageTypeFile(mCurrentButtonIndex);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(3), "CurrentFileIndexNo");
                gotoPreviousActivity();
            } else if (mCurrentButtonIndex == 3) {
                mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                saveImageTypeFile(mCurrentButtonIndex);
                mRetakeFlagEnabled = true;
                mCaptureTopViewButton.setEnabled(true);
                mCaptureAngleViewButton.setEnabled(true);
                mCaptureSideViewButton.setEnabled(true);
                mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.next_button_selector));
                if (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                } else {
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                }
                mCamera.setParameters(parameters);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(7), "CurrentFileIndexNo");
            }
        } else {
            if (mCurrentButtonIndex == 1) {
                mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                saveImageTypeFile(mCurrentButtonIndex);
                mCaptureTopViewButton.setEnabled(true);
                mCaptureAngleViewButton.setEnabled(true);
                mCaptureSideViewButton.setEnabled(true);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(7), "CurrentFileIndexNo");
            } else if (mCurrentButtonIndex == 2) {
                mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                saveImageTypeFile(mCurrentButtonIndex);
                mCaptureTopViewButton.setEnabled(true);
                mCaptureAngleViewButton.setEnabled(true);
                mCaptureSideViewButton.setEnabled(true);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(7), "CurrentFileIndexNo");
            } else if (mCurrentButtonIndex == 3) {
                mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.retake_picture_selector));
                saveImageTypeFile(mCurrentButtonIndex);
                mCaptureTopViewButton.setEnabled(true);
                mCaptureAngleViewButton.setEnabled(true);
                mCaptureSideViewButton.setEnabled(true);
                new IAUtilities().setSharedPreferences(mContext, String.valueOf(7), "CurrentFileIndexNo");
            }
            if (mContext.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH)) {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            } else {
                parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
            }
            mCamera.setParameters(parameters);
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.next_button_selector));
        }
        if (mCurrentButtonIndex >= 0 && mCurrentButtonIndex <= 3) {
            mCurrentButtonIndex++;
        } else {
            mCurrentButtonIndex = 0;
        }
        allReading="[ "+tvReading+"; "+tsvReading+"; "+svReading+" ]";
        Log.d("TAG",allReading);
    }

    /**
     * called to check which of the views on the screen overlay have been called
     *
     * @param view
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View view) {
        if (view == mCaptureTopViewButton && mRetakeFlagEnabled) {
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 1;
            new IAUtilities().setSharedPreferences(mContext, String.valueOf(mCurrentButtonIndex + 3), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureAngleViewButton && mRetakeFlagEnabled) {
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 2;
            new IAUtilities().setSharedPreferences(mContext, String.valueOf(mCurrentButtonIndex + 3), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureSideViewButton && mRetakeFlagEnabled) {
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 3;
            new IAUtilities().setSharedPreferences(mContext, String.valueOf(mCurrentButtonIndex + 3), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureTopViewButton) {
            mCaptureTopViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 1;
            new IAUtilities().setSharedPreferences(mContext, String.valueOf(mCurrentButtonIndex), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureAngleViewButton) {
            mCaptureAngleViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureSideViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 2;
            new IAUtilities().setSharedPreferences(mContext, String.valueOf(mCurrentButtonIndex), "CurrentFileIndexNo");
            gotoPreviousActivity();
        } else if (view == mCaptureSideViewButton) {
            mCaptureSideViewButton.setBackground(mContext.getResources().getDrawable(R.drawable.take_picture_selector));
            mCaptureTopViewButton.setEnabled(false);
            mCaptureAngleViewButton.setEnabled(false);
            mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.camera_button_selector));
            mCurrentButtonIndex = 3;
            new IAUtilities().setSharedPreferences(mContext, String.valueOf(mCurrentButtonIndex), "CurrentFileIndexNo");
            gotoPreviousActivity();
        }
    }

    /**
     * called to navigate to the Image Uploading activity
     */
    private void goToUploadImagesRevisedActivity() {
        Intent intent = new Intent(FMCameraActivity.this, FMUploadImagesRevisedActivity.class);
        intent.putExtra("image_resolution", mCamera.getParameters().getPictureSize().width + " x " + mCamera.getParameters().getPictureSize().height);
        IAUtilities.showingLogs(TAG, "image_resolution = " + mCamera.getParameters().getPictureSize().width + " x " + mCamera.getParameters().getPictureSize().height, IAUtilities.LOGS_SHOWN);
        intent.putExtra("zoom_level", mCamera.getParameters().getMaxZoom());
        IAUtilities.showingLogs(TAG, "zoom_level = " + mCamera.getParameters().getMaxZoom(), IAUtilities.LOGS_SHOWN);
        intent.putExtra("allReading",allReading);
        startActivityForResult(intent, Integer.parseInt(mContext.getResources().getString(R.string.upload_images_revised)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onactivity result -- Camera activity");
        if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.upload_images_revised))) {
            if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result))) {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result)), data);
                finish();
//                System.exit(0);
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.retake_images))) {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images)));
                finish();
//                System.exit(0);
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images))) {
                Log.d(TAG, "onactivity result -- Camera activity retake photos");
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images)));
                finish();
//                System.exit(0);
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code))) {

            }
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

        if (mCamera != null) {
            try {
                mCamera.setPreviewDisplay(holder);
                if (mIsCapturing) {
                    mCamera.startPreview();
                    mIsSafeToCapturePicture = true;
                }
            } catch (IOException e) {
                Toast.makeText(FMCameraActivity.this, "Unable to start camera preview.", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    /**
     * called to run the image capture functionality
     */
    private void captureImage() {

        if (mCaptureTopViewButton.isEnabled()
                && mCaptureAngleViewButton.isEnabled()
                && mCaptureSideViewButton.isEnabled()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.next_button_selector));
            }
            Toast.makeText(mContext, "Please select a view to retake", Toast.LENGTH_SHORT).show();
        } else {
            if (mCamera != null) {
                Camera.Parameters p = mCamera.getParameters();
                if (p != null) {
                    List<String> focusModes = p.getSupportedFocusModes();
                    if (focusModes != null && focusModes.contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
                        //Phone supports autofocus!
                        mCamera.autoFocus(new Camera.AutoFocusCallback() {
                            @Override
                            public void onAutoFocus(boolean success, Camera camera) {
                                if (mIsSafeToCapturePicture) {
                                    mCamera.startPreview();
                                    mCamera.takePicture(shutterCallback, null, FMCameraActivity.this);
                                    mIsSafeToCapturePicture = false;
                                }
                            }
                        });

                    } else {
                        //Phone does not support autofocus!
                        if (mIsSafeToCapturePicture) {
                            mCamera.startPreview();
                            mCamera.takePicture(shutterCallback, null, FMCameraActivity.this);
                            mIsSafeToCapturePicture = false;
                        } else {
                            Toast.makeText(mContext, "The Camera is busy. Please wait...", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }

        }
    }

    private final Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            mCameraShutterAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mCameraShutterAudioManager.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
        }
    };

    /**
     * unused for now. Called if camera preview is not started properly.
     */
    private void setupImageCapture() {

        if (mCaptureTopViewButton.isEnabled() && mCaptureAngleViewButton.isEnabled() && mCaptureSideViewButton.isEnabled()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mCaptureImageButton.setBackground(mContext.getResources().getDrawable(R.drawable.next_button_selector));
            }
            Toast.makeText(mContext, "Please select a view to retake", Toast.LENGTH_SHORT).show();

        } else {
            mCamera.startPreview();
        }
    }

    /**
     * called to save the captured image according to the type of image
     * selected by the user
     *
     * @param type
     */
    private void saveImageTypeFile(int type) {
        if (mCameraBitmap != null) {
            mCameraBitmap.recycle();
            mCameraBitmap = null;
        }
        if (mCameraData != null) {
            mCameraBitmap = BitmapFactory.decodeByteArray(mCameraData, 0, mCameraData.length);
            IAUtilities.showingLogs(TAG, "mCameraBitmap.getWidth() = " + mCameraBitmap.getWidth()
                    + " mCameraBitmap.getHeight() = " + mCameraBitmap.getHeight(), IAUtilities.LOGS_SHOWN);
            DisplayMetrics metrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(metrics);

            mCroppedBitmapBmp = calculateActualCoordinates(mCameraBitmap, mCropViewLayout, mCamera.getParameters().getPictureSize());

            File saveFile = openFileForImage(type);
            if (saveFile != null) {
                saveImageToFile(saveFile);
                mCroppedBitmapBmp.recycle();
                mCameraBitmap.recycle();
                onPause();
                onResume();
            } else {
                Toast.makeText(FMCameraActivity.this, "Unable to open file for saving image.",
                        Toast.LENGTH_SHORT).show();
                mCroppedBitmapBmp.recycle();
                mCameraBitmap.recycle();
            }
        }
    }

    /**
     * called to calculate the actual coordinates to be used for cropping
     *
     * @param mCameraBitmap       original bitmap to be cropped
     * @param mCropViewLayout     crop viewfinder to be used as cropping reference
     * @param mOptimalPictureSize the image size of the original image to be cropped
     * @return cropped bitmap
     */
    private Bitmap calculateActualCoordinates(Bitmap mCameraBitmap, RelativeLayout mCropViewLayout, Camera.Size mOptimalPictureSize) {
        int mXCoordinateStart = 0;
        int mYCoordinateStart = 0;
        int mWidth = 0;
        int mHeight = 0;

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        IAUtilities.showingLogs(TAG, " crop mCropViewLayout width = " + mCropViewLayout.getWidth() + " height = " + mCropViewLayout.getHeight()
                + " left = " + mCropViewLayout.getLeft() + " top = " + mCropViewLayout.getTop(), IAUtilities.LOGS_SHOWN);

        IAUtilities.showingLogs(TAG, " displayMetrics density = " + displayMetrics.density + " densityDpi = " + displayMetrics.densityDpi
                + " scaledDensity = " + displayMetrics.scaledDensity, IAUtilities.LOGS_SHOWN);

        float mScaleFactor = 0;
        mScaleFactor = (float) mOptimalPictureSize.height / displayMetrics.heightPixels;
        IAUtilities.showingLogs(TAG, "mScaleFactor = " + mScaleFactor +
                " mOptimalPictureSize height = " + mOptimalPictureSize.height +
                " displayMetrics heightPixels = " + displayMetrics.heightPixels, IAUtilities.LOGS_SHOWN);

        mWidth = (int) (mCropViewLayout.getWidth() + mCropViewLayout.getWidth() * 0.4f);
        mHeight = (int) (mCropViewLayout.getHeight() + mCropViewLayout.getHeight() * 0.4f);
        mWidth = (int) (mWidth * mScaleFactor);
        mHeight = (int) (mHeight * mScaleFactor);

        if (mWidth % 2 != 0) {
            mWidth++;
        }
        if (mHeight % 2 != 0) {
            mHeight++;
        }
        if (mWidth > mCameraBitmap.getWidth()) {
            mWidth = mCameraBitmap.getWidth();
        }
        if (mHeight > mCameraBitmap.getHeight()) {
            mHeight = mCameraBitmap.getHeight();
        }

        mXCoordinateStart = (int) (mCameraBitmap.getWidth() / 2.0f - mWidth / 2.0f);
        mYCoordinateStart = (int) (mCameraBitmap.getHeight() / 2.0f - mHeight / 2.0f);

        if (mXCoordinateStart < 0) {
            mXCoordinateStart = 0;
        }
        if (mYCoordinateStart < 0) {
            mYCoordinateStart = 0;
        }

        Log.d(TAG, " crop mXCoordinateStart = " + mXCoordinateStart + " mYCoordinateStart = "
                + mYCoordinateStart + " mWidth = " + mWidth + " mHeight = " + mHeight);

        return Bitmap.createBitmap(mCameraBitmap, mXCoordinateStart, mYCoordinateStart, mWidth, mHeight);
    }


    /**
     * open the file to be stored as image
     *
     * @param type type of the image
     * @return file object
     */
    private File openFileForImage(int type) {
        File imageDirectory = null;
        String storageState = Environment.getExternalStorageState();
        if (storageState.equals(Environment.MEDIA_MOUNTED)) {
            imageDirectory = new File(
                    mFileStoragePath);
            if (!imageDirectory.exists() && !imageDirectory.mkdirs()) {
                imageDirectory = null;
            } else {
                return new File(imageDirectory.getPath() +
                        File.separator + "image_" +
                        type + ".png");
            }
        }
        return null;
    }

    /**
     * save the image data to the file object
     *
     * @param file
     */
    private void saveImageToFile(File file) {
        if (mCameraBitmap != null) {
            FileOutputStream outStream = null;
            try {
                outStream = new FileOutputStream(file);
                if (!mCroppedBitmapBmp.compress(Bitmap.CompressFormat.PNG, 100, outStream)) {
                    Toast.makeText(FMCameraActivity.this, "Unable to save image to file.",
                            Toast.LENGTH_SHORT).show();
                } else {

                }
                outStream.close();
            } catch (Exception e) {
                Toast.makeText(FMCameraActivity.this, "Unable to save image to file.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * called to set the cropping viewfinder dimensions according to
     * the device dimensions
     */
    private void setWidthAccToHeight() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mCropViewLayout.getLayoutParams();
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        float mVerticalHeight = metrics.heightPixels - 20 * metrics.scaledDensity;
        mVerticalHeight *=0.9;
        float mScaleFactor = 0;
        mScaleFactor = 215.9f / 279.4f;

        float mWidth = mScaleFactor * mVerticalHeight;

        params.height = (int) mVerticalHeight;
        params.width = (int) mWidth;

        /*mCropViewLayout.setX(0);
        mCropViewLayout.setY(10);*/
        mCropViewLayout.setLayoutParams(params);

    }

    /**
     * can be usedd to get the status bar height of the device
     *
     * @return
     */
    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        IAUtilities.showingLogs(TAG, "Preview result = " + result, IAUtilities.LOGS_SHOWN);
        return result;
    }

    /**
     * called to go to the previous activity
     */
    private void gotoPreviousActivity() {
        setResult(Integer.parseInt(mContext.getResources().getString(R.string.reinitialize_activity)));
        finish();
        System.exit(0);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("TAG","in onResume()");
//        mSensorManager.registerListener(this, mAccelerometerSensor, SensorManager.SENSOR_DELAY_UI);
//        mSensorManager.registerListener(this, mMagnetometerSensor, SensorManager.SENSOR_DELAY_UI);
//        sensorManager= (SensorManager) this.getSystemService(SENSOR_SERVICE);
//        SensorManager.remapCoordinateSystem(inR, SensorManager.AXIS_Y, SensorManager.AXIS_MINUS_X, outR);
        if (mIsCapturing)
            startOldCameraFunction();
        initializeSensor();
        registerSensorManagerListeners();


    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this, accel);
        sensorManager.unregisterListener(this, compass);
        sensorManager.unregisterListener(this, orient);
        //mSensorManager.unregisterListener(this);
        sensorManager.unregisterListener(this);
        if (mCamera != null) {
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
        }
    }

    // new sensor code

    /*  private float mAverageDpi = 0.0f;
      private float mXCalculatedStepMoveFactor = 0.0f;
      private float mYCalculatedStepMoveFactor = 0.0f;
  */
    private void initializeSensor() {

        mOverlayViewLeft = findViewById(R.id.left_black_region_v);
        mOverlayViewRight = findViewById(R.id.right_black_region_v);
        mSensorChangeValueImageView = findViewById(R.id.sensor_change_value_iv);
        mSensorChangeValueImageView2 = findViewById(R.id.sensor_change_value_iv2);
       // mSensorChangeValueImageView3 = findViewById(R.id.sensor_change_value_iv);
        mSensorChangedTextView1 = (TextView) findViewById(R.id.pitch_value_tv);
        mSensorChangedTextView2 = (TextView) findViewById(R.id.roll_value_tv);
        mSensorChangedTextView3 = (TextView) findViewById(R.id.time_delay_value_tv);
        mSensorChangedTextView4 = (TextView) findViewById(R.id.diff_in_val_tv);
        mSensorChangedTextView3.setVisibility(View.GONE);
        obs=mSensorChangeValueImageView.getViewTreeObserver();
        obs.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                WIDTH_BALL=mSensorChangeValueImageView.getWidth();
                HEIGHT_BALL=mSensorChangeValueImageView.getHeight();
               /* WIDTH_BALL2=mSensorChangeValueImageView2.getHeight();
                HEIGHT_BALL2=mSensorChangeValueImageView2.getHeight();*/
                return true;
            }
        });
        initializeViewBoundsForBall();

//        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

        //      mMagnetometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        //    mAccelerometerSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

       /* displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        Log.i(TAG, " displayMetrics = " + displayMetrics.widthPixels + " " + displayMetrics.heightPixels);*/

       /* mAverageDpi = (displayMetrics.xdpi + displayMetrics.ydpi) / 2.0f;
        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
//                Log.i(TAG, "mAverageDpi = " + mAverageDpi);
//                Log.i(TAG, "mSensorChangeValueImageView.getWidth() = " + mSensorChangeValueImageView.getWidth());
//                Log.i(TAG, "mXCalculatedStepMoveFactor = " + (mAverageDpi / mSensorChangeValueImageView.getWidth() * 5));
                mXCalculatedStepMoveFactor = mAverageDpi / mSensorChangeValueImageView.getWidth() * 5.0f;
                mYCalculatedStepMoveFactor = mAverageDpi / mSensorChangeValueImageView.getHeight() * 3.0f;
                initializeViewBoundsForBall();
            }
        }, 400);*/
    }


    private void initializeViewBoundsForBall() {

//        mOverlayViewLeft.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
//            @Override
//            public void onGlobalLayout() {
//                mOverlayViewLeft.getViewTreeObserver().removeOnGlobalLayoutListener(this);
//                LEFT_VIEW_WIDTH = mOverlayViewLeft.getWidth(); //height is ready
//
//
//            }
//        });

        mOverlayViewLeft.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                LEFT_VIEW_WIDTH = mOverlayViewLeft.getWidth();
                return true;
            }
        });

        mOverlayViewRight.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mOverlayViewRight.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                RIGHT_VIEW_WIDTH=mOverlayViewRight.getWidth();
                LEFT_OF_RIGHT_VIEW=mOverlayViewRight.getLeft();
            }
        });

        mCameraPreview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mCameraPreview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                CAMERA_PREVIEW_HEIGHT=mCameraPreview.getHeight();
                CAMERA_PREVIEW_WIDTH=mCameraPreview.getWidth();
                CAMERA_PREVIEW_TOP=mCameraPreview.getTop();
            }
        });

        cropViewObs=mCropViewLayout.getViewTreeObserver();
        mCropViewLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mCropViewLayout.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                LEFT_OF_CROP_VIEW=mCropViewLayout.getLeft();
                CROP_VIEW_WIDTH=mCropViewLayout.getWidth();
                TOP_OF_CROP_VIEW= mCropViewLayout.getTop();
                CROP_VIEW_HEIGHT=mCropViewLayout.getHeight();
            }
        });
        WIDTH=getScreenWidth();
        HEIGHT=getScreenHeight();

        mHorizontalGuaze.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mHorizontalGuaze.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                HORIZONTAL_GAUZE_HEIGHT=mHorizontalGuaze.getHeight();
                HORIZONTAL_GAUZE_WIDTH=mHorizontalGuaze.getWidth();
            }
        });

        mVerticalGuaze.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                mVerticalGuaze.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                VERTICAL_GAUZE_HEIGHT=mVerticalGuaze.getHeight();
                VERTICAL_GAUZE_WIDTH=mVerticalGuaze.getWidth();
            }
        });



        mMinXBoundForVerticalGuazeBall= 0;
        mMinYBoundForVerticalGuazeBall= 0;
        mMaxXBoundForVerticalGuazeBall= 0;
        mMaxYBoundForVerticalGuazeBall= VERTICAL_GAUZE_HEIGHT - HEIGHT_BALL ;

        /*xCenter = (LEFT_OF_CROP_VIEW + CROP_VIEW_WIDTH/ 2 - WIDTH_BALL / 2);
        yCenter = (TOP_OF_CROP_VIEW + CROP_VIEW_HEIGHT/ 2 - HEIGHT_BALL / 2);
*/


        xCenter=WIDTH/2;
        yCenter=HEIGHT/2;
        mSensorChangeValueImageView.setX(0);
        mSensorChangeValueImageView.setY(0);

        Log.d(TAG,""+mSensorChangeValueImageView.getX());
        Log.d(TAG,""+mSensorChangeValueImageView.getX());

    }
    /*private void initializeViewBoundsForBall() {

        mMinXBound = mOverlayViewLeft.getWidth();
        mMinYBound = mCameraPreview.getTop();

        mMaxXBound = (mOverlayViewRight.getLeft() - mSensorChangeValueImageView.getWidth());
        mMaxYBound = (mCameraPreview.getTop() + mCameraPreview.getHeight() - mSensorChangeValueImageView.getHeight());

        xCenter = (mCropViewLayout.getLeft() + mCropViewLayout.getWidth() / 2 - mSensorChangeValueImageView.getWidth() / 2);
        yCenter = (mCropViewLayout.getTop() + mCropViewLayout.getHeight() / 2 - mSensorChangeValueImageView.getHeight() / 2);

    }*/


    /* public void updateOrientationAngles() {

        // Update rotation matrix, which is needed to update orientation angles.
        mSensorManager.getRotationMatrix(mRotationMatrix, null, mAccelerometerReading, mMagnetometerReading);

        // "mRotationMatrix" now has up-to-date information.
        mSensorManager.getOrientation(mRotationMatrix, mOrientationAngles);

        // "mOrientationAngles" now has up-to-date information.
        Log.i(TAG, " mOrientationAngles" + Arrays.toString(mOrientationAngles));

        pitch = mOrientationAngles[2] * RAD_TO_DEG;
        Log.i(TAG, "pitch " + pitch);
        roll = (mOrientationAngles[1] * RAD_TO_DEG);
        Log.i(TAG, "roll " + roll);
        yaw = mOrientationAngles[0] * RAD_TO_DEG;
        Log.i(TAG, "yaw " + yaw);

        if (mCurrentButtonIndex == 1) {
            optimalPitch = optimalPitch_tv;
            optimalRoll = optimalRoll_tv;
            maxOffPitch = maxOffPitch_tv;
            maxOffRoll = maxOffRoll_tv;
            criticalPitch = criticalPitch_tv;
            criticalRoll = criticalRoll_tv;

            validPitch = validPitch_tv;
            validRoll = validRoll_tv;
        } else if (mCurrentButtonIndex == 2) {
            optimalPitch = optimalPitch_tsv;
            optimalRoll = optimalRoll_tsv;
            maxOffPitch = maxOffPitch_tsv;
            maxOffRoll = maxOffRoll_tsv;
            criticalPitch = criticalPitch_tsv;
            criticalRoll = criticalRoll_tsv;

            validPitch = validPitch_tsv;
            validRoll = validRoll_tsv;
        } else if (mCurrentButtonIndex == 3) {
            optimalPitch = optimalPitch_sv;
            optimalRoll = optimalRoll_sv;
            maxOffPitch = maxOffPitch_sv;
            maxOffRoll = maxOffRoll_sv;
            criticalPitch = criticalPitch_sv;
            criticalRoll = criticalRoll_sv;

            validPitch = validPitch_sv;
            validRoll = validRoll_sv;
        }

        validPitch_max = optimalPitch + validPitch;
        validPitch_min = optimalPitch - validPitch;

        validRoll_max = optimalRoll + validRoll;
        validRoll_min = optimalRoll - validRoll;

        if (pitch >= validPitch_min && pitch <= validPitch_max) {
            mSensorChangedTextView2.setTextColor(Color.parseColor("#5fc260"));
        } else {
            mSensorChangedTextView2.setTextColor(Color.parseColor("#E60000"));
        }

        if (roll >= validRoll_min && roll <= validRoll_max) {
            mSensorChangedTextView1.setTextColor(Color.parseColor("#5fc260"));
        } else {
            mSensorChangedTextView1.setTextColor(Color.parseColor("#E60000"));
        }

        mSensorChangedTextView1.setText(String.format(Locale.getDefault(), "p : %.1f", roll));
        mSensorChangedTextView2.setText(String.format(Locale.getDefault(), "r : %.1f", -pitch));

        moveX = (int) (xCenter + (roll * mYCalculatedStepMoveFactor));
        moveY = (int) (yCenter + (pitch * mXCalculatedStepMoveFactor));
        Log.i(TAG, "moveX " + moveX);
        Log.i(TAG, "moveY " + moveY);

        if (moveX > mMinXBound && moveX < mMaxXBound) {
            x = moveX;
        } else if (moveX <= mMinXBound) {
            x = mMinXBound;
        } else if (moveX >= mMaxXBound) {
            x = mMaxXBound;
        }

        if (moveY > mMinYBound && moveY < mMaxYBound) {
            y = moveY;
        } else if (moveY <= mMinYBound) {
            y = mMinYBound;
        } else if (moveY >= mMaxYBound) {
            y = mMaxYBound;
        }

//        float lastX = mSensorChangeValueImageView.getX();
//        float lastY = mSensorChangeValueImageView.getY();
//        Log.i(TAG, "lastX = " + lastX + " lastY = " + lastY);
//        TranslateAnimation anim = new TranslateAnimation(lastX, x, lastY, y);
//        anim.setDuration(100);
//        anim.setFillAfter(true);
//        mSensorChangeValueImageView.startAnimation(anim);

        Log.i(TAG, " old x = " + mSensorChangeValueImageView.getX() + " old y = " + mSensorChangeValueImageView.getY());

        ObjectAnimator animX = ObjectAnimator.ofFloat(mSensorChangeValueImageView, "x", x);
        ObjectAnimator animY = ObjectAnimator.ofFloat(mSensorChangeValueImageView, "y", y);

        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY);
        animSetXY.setDuration(100);
        animSetXY.start();

        if (x < mMinXBound) {
            mSensorChangeValueImageView.setX(mMinXBound);
        } else if (x > mMaxXBound) {
            mSensorChangeValueImageView.setX(mMaxXBound);
        }

        if (y < mMinYBound) {
            mSensorChangeValueImageView.setY(mMinYBound);
        } else if (y > mMaxYBound) {
            mSensorChangeValueImageView.setY(mMaxYBound);
        }

//        if (Math.abs(x - mSensorChangeValueImageView.getX()) > 15) {
//            animSetXY.start();
//        } else {
////            mSensorChangeValueImageView.setX(x);
//        }

//        if (Math.abs(y - mSensorChangeValueImageView.getY()) > 15) {
//            animSetXY.start();
//        } else {
////            mSensorChangeValueImageView.setY(y);
//        }

//        float xCurrentPos = mSensorChangeValueImageView.getLeft();
//        float yCurrentPos = mSensorChangeValueImageView.getTop();
//
//        Animation anim= new TranslateAnimation(xCurrentPos, x, yCurrentPos, y);
//        anim.setDuration(1000);
//        anim.setFillAfter(true);
//        anim.setFillEnabled(true);
//        animSurprise2Movement.setAnimationListener(new Animation.AnimationListener() {
//
//            @Override
//            public void onAnimationStart(Animation arg0) {}
//
//            @Override
//            public void onAnimationRepeat(Animation arg0) {}
//
//            @Override
//            public void onAnimationEnd(Animation arg0) {
//                xCurrentPos -= 150;
//            }
//        });
//        mSensorChangeValueImageView.startAnimation(anim);

//        if (Math.abs(x - mSensorChangeValueImageView.getX()) > 5) {
//            mSensorChangeValueImageView.setX(x);
//        }
//        if (Math.abs(y - mSensorChangeValueImageView.getY()) > 5) {
//            mSensorChangeValueImageView.setY(y);
//        }
//        mSensorChangeValueImageView.invalidate();
    }

*/
    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }
    @Override
    public void onSensorChanged(SensorEvent event) {
//        if (event.sensor == mAccelerometerSensor) {
//            System.arraycopy(event.values, 0, mAccelerometerReading,
//                    0, mAccelerometerReading.length);
//        } else if (event.sensor == mMagnetometerSensor) {
//            System.arraycopy(event.values, 0, mMagnetometerReading,
//                    0, mMagnetometerReading.length);
//        }

        //updateOrientationAngles();
       /*
       switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                sensorFusion.setAccel(event.values);
                sensorFusion.calculateAccMagOrientation();
                break;
            case Sensor.TYPE_GYROSCOPE:
                sensorFusion.gyroFunction(event);
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                sensorFusion.setMagnet(event.values);
                break;
        }
        updateOrientationDisplay();*/
        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:

                for (int i = 0; i < 3; i++) {
                    accelValues[i] = event.values[i];
                }
                Log.d("TAG", "In accelerometer" + accelValues[0]);
                if (accelValues[0] != 0)
                    ready = true;
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                for (int i = 0; i < 3; i++) {
                    compassValues[i] = event.values[i];
                }
                Log.d("TAG", "In magnetic field" + compassValues[2]);

                mSensorManager.getRotationMatrix(mRotationMatrix, null, accelValues, compassValues);
                if (compassValues[2] != 0)
                    ready = true;
                /*if(mCurrentButtonIndex==2) {
                    SENSOR_DELAY = 100;
                    animDuration = 250;
                    Log.d("delay","delay--"+SENSOR_DELAY);
                    Log.d("delay","animation duration--"+animDuration);
                }
                else
                {
                    SENSOR_DELAY = 20000;
                    animDuration = 210;
                }*/

                if(!isGyroPresent)
                {
                    Log.d("delay","sensor delay--"+SENSOR_DELAY);

                    Log.d("delay","current button indes--"+mCurrentButtonIndex);
                   /* if(mCurrentButtonIndex==2)
                       SENSOR_DELAY=100;
                    else
                        SENSOR_DELAY=80;
*/
//                    if(mCurrentButtonIndex==2){
//                        doUpdate(null);
//                        counter= 1;
//                    }
                    if (counter++ % 3 == 0)
                    {
                        doUpdate(null);
                        counter= 1;
                    }

                }
                else
                    {
                       if(counter++ %15== 0)
                       {
                           doUpdate(null);
                           counter= 1;
                       }
                    }

                break;
            case Sensor.TYPE_GYROSCOPE:
                isGyroPresent=true;
                for (int i = 0; i < 3; i++) {
                    gyroValues[i] = event.values[i];
                }
                Log.d("TAG", "In TYPE_GYROSCOPE " + gyroValues[2]);
                mSensorManager.getRotationMatrix(mRotationMatrix, null, accelValues, gyroValues);


                if (gyroValues[2] != 0)
                    ready = true;
                if (counter++ % 15== 0) {

                    doUpdate(null);
                    counter = 1;
                }
                break;

            /*case Sensor.TYPE_ORIENTATION:
                for (int i = 0; i < 3; i++) {
                    if (i == 2)
                        orientationValues[i] = -event.values[i];
                    else
                        orientationValues[i] = event.values[i];
                }
                Log.d("TAG","In orientation");
                ready=true;
                break;
*/
        }
        mSensorManager.getOrientation(mRotationMatrix, orientationValues);
        if (!ready)
            return;
       /* if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE)
        {
            if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
                mSensorManager.getRotationMatrix(mRotationMatrix, null, accelValues, compassValues);
            else
                mSensorManager.getRotationMatrix(mRotationMatrix, null, accelValues, gyroValues);
        }
        else if(event.sensor.getType()==Sensor.TYPE_MAGNETIC_FIELD) {

            mSensorManager.getRotationMatrix(mRotationMatrix, null, accelValues, compassValues);
        }*/





        //   if (SensorManager.getRotationMatrix(inR, inclineMatrix, accelValues, gyroValues)) {
        // got a good rotation matrix
            /*SensorManager.getOrientation(inR, prefValues);
            mInclination = SensorManager.getInclination(inclineMatrix);*/
        // Display every 10th value
        Log.d("TAG","Counter--"+counter);


        // }
    }

    public void doUpdate(View view) {
        Log.d(TAG, "do update");
        if (!ready)
            return;

        pitch = (float) getPitch(orientationValues[1]) * 57.29577f;
        roll = (float) getRoll(orientationValues[2]) * 57.29577f;
        current_time = Calendar.getInstance().getTimeInMillis();


        mSensorChangedTextView1.setText("P: " + d.format(pitch));
        mSensorChangedTextView2.setText("R: " + d.format(roll));
        mSensorChangedTextView3.setText("delay: " + Long.toString(current_time - prev_time));
        mSensorChangedTextView4.setText("DP:" + d.format(pitch - prevPitch) + "\n" + "DR:" + d.format(roll - prevRoll));
        prev_time = current_time;

        if (mPitchRecords.size() == 2 && mRollRecords.size() == 2)
        {
            if (((pitch - prevPitch < -0.2) && (pitch - prevPitch > 0.2)) || ((roll - prevRoll > 0.2) && (roll - prevRoll < -0.2)))
            {
                SENSOR_DELAY = 20000;
                animDuration = 210;

                Log.d("TAG","sensor delay -- 20000" +(pitch - prevPitch));
                Log.d("TAG","sensor delay -- 20000" +(roll - prevRoll));
                Log.d("TAG","animation duration --210");
            }
            else
            {
                SENSOR_DELAY = 80000;
                animDuration = 80;

                Log.d("TAG","sensor delay -- 80000" +(pitch - prevPitch));
                Log.d("TAG","sensor delay -- 80000" +(roll - prevRoll));
            }
        }

        calGuidanceSystem((float) pitch, (float) roll);
        prevPitch = (float) pitch;
        prevRoll = (float) roll;
        mPitchRecords.add((float) pitch);
        mRollRecords.add((float) roll);

        mSensorChangedTextView1.invalidate();
        mSensorChangedTextView2.invalidate();
        mSensorChangedTextView3.invalidate();
        mSensorChangedTextView4.invalidate();
    }

    public double getRoll(double f) {
        double result = 0;
        switch (mDisplay.getRotation()) {
            case Surface.ROTATION_0:
                result = f; //* 180 / Math.PI;
                break;
            case Surface.ROTATION_90:
                result = f; //* 180 / Math.PI;
                break;
            case Surface.ROTATION_180:
                result = -f; //* 180 / Math.PI;
                break;
            case Surface.ROTATION_270:
                result = -f; // * 180 / Math.PI;
                break;
        }
        return result;
    }

    public double getPitch(double f)
    {
        double result = 0;
        switch (mDisplay.getRotation()) {
            case Surface.ROTATION_0:
                result = f; //* 180 / Math.PI;
                break;
            case Surface.ROTATION_90:
                result = -f; //* 180 / Math.PI;
                break;
            case Surface.ROTATION_180:
                result = -f; //* 180 / Math.PI;
                break;
            case Surface.ROTATION_270:
                result = f; // * 180 / Math.PI;
                break;
        }
        return result;
    }

    public void calGuidanceSystem(float pitch, float roll)
    {

        if (mCurrentButtonIndex == 1) {
            optimalPitch = optimalPitch_tv;
            optimalRoll = optimalRoll_tv;
            maxOffPitch = maxOffPitch_tv;
            maxOffRoll = maxOffRoll_tv;
            criticalPitch = criticalPitch_tv;
            criticalRoll = criticalRoll_tv;
            validPitch = validPitch_tv;
            validRoll = validRoll_tv;
            mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_topview_right_gray));
            mSensorChangeValueImageView.setVisibility(View.VISIBLE);
            innerRegionOfInterest.setVisibility(View.VISIBLE);
            sideViewLine.setVisibility(View.GONE);
            tvReading="TOPVIEW: "+pitch+" "+roll;

        } else if (mCurrentButtonIndex == 2) {
            optimalPitch = optimalPitch_tsv;
            optimalRoll = optimalRoll_tsv;
            maxOffPitch = maxOffPitch_tsv;
            maxOffRoll = maxOffRoll_tsv;
            criticalPitch = criticalPitch_tsv;
            criticalRoll = criticalRoll_tsv;
            validPitch = validPitch_tsv;
            validRoll = validRoll_tsv;
            mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_angleview_right_gray));
            mSensorChangeValueImageView.setVisibility(View.VISIBLE);
            innerRegionOfInterest.setVisibility(View.VISIBLE);
            sideViewLine.setVisibility(View.GONE);
            tsvReading="TOPSIDEVIEW: "+pitch+" "+roll;
            Log.d(TAG, "tsvreading -- "+tsvReading);

        } else if (mCurrentButtonIndex == 3) {
            optimalPitch = optimalPitch_sv;
            optimalRoll = optimalRoll_sv;
            maxOffPitch = maxOffPitch_sv;
            maxOffRoll = maxOffRoll_sv;
            criticalPitch = criticalPitch_sv;
            criticalRoll = criticalRoll_sv;
            validPitch = validPitch_sv;
            validRoll = validRoll_sv;
            mCaptureImageButton.setEnabled(true);
            mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_sideview_right_green));
            mVerticalGuaze.setVisibility(View.GONE);
            mVerticalLowerGuaze.setVisibility(View.GONE);
            mHorizontalGuaze.setVisibility(View.GONE);
         //   mSensorChangeValueImageView.setVisibility(View.GONE);
           // innerRegionOfInterest.setVisibility(View.GONE);
            sideViewLine.setVisibility(View.GONE);
            svReading="SIDEVIEW: "+pitch+" "+roll;
        }

        validPitch_max = optimalPitch + validPitch;
        validPitch_min = optimalPitch - validPitch;

        validRoll_max = optimalRoll + validRoll;
        validRoll_min = optimalRoll - validRoll;
       if(mCurrentButtonIndex==1 ||mCurrentButtonIndex==2) {
           if (pitch >= validPitch_min && pitch <= validPitch_max) {
               if (roll >= validRoll_min && roll <= validRoll_max) {
                   mSensorChangedTextView2.setTextColor(Color.GREEN);
                   if (mCurrentButtonIndex == 1)
                       mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_topview_right_green));
                   else if (mCurrentButtonIndex == 2)
                       mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_angleview_right_green));
                   //mSensorChangeValueImageView.setBackground(getResources().getDrawable(R.drawable.circular_view_green));
                   mCaptureImageButton.setEnabled(true);
                   isInside = true;
                 //  innerRegionOfInterest.setVisibility(View.GONE);
               } else {
                   mSensorChangedTextView2.setTextColor(Color.RED);
                   mSensorChangeValueImageView.setBackground(getResources().getDrawable(R.drawable.circular_view_orange));
                   if (mCurrentButtonIndex == 1)
                       mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_topview_right_gray));
                   else if (mCurrentButtonIndex == 2)
                       mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_angleview_right_gray));
               /* else
                    mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_sideview_right_gray));*/
                   mCaptureImageButton.setEnabled(false);
                   isInside = false;
                   innerRegionOfInterest.setVisibility(View.VISIBLE);
               }
               mSensorChangedTextView1.setTextColor(Color.GREEN);

           } else if (roll >= validRoll_min && roll <= validRoll_max) {

               mSensorChangeValueImageView.setBackground(getResources().getDrawable(R.drawable.circular_view_orange));
               if (mCurrentButtonIndex == 1)
                   mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_topview_right_gray));
               else if (mCurrentButtonIndex == 2)
                   mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_angleview_right_gray));
          /*  else
                mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_sideview_right_gray));
*/
               mCaptureImageButton.setEnabled(false);
               mSensorChangedTextView1.setTextColor(Color.RED);
               mSensorChangedTextView2.setTextColor(Color.GREEN);
               isInside = false;
               innerRegionOfInterest.setVisibility(View.VISIBLE);
           } else {
               mSensorChangeValueImageView.setBackground(getResources().getDrawable(R.drawable.circular_view_orange));
               if (mCurrentButtonIndex == 1)
                   mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_topview_right_gray));
               else if (mCurrentButtonIndex == 2)
                   mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_angleview_right_gray));
         /*   else
                mCropViewLayout.setBackground(getBaseContext().getResources().getDrawable(R.drawable.a4_sideview_right_gray));
*/
               mCaptureImageButton.setEnabled(false);
               mSensorChangedTextView1.setTextColor(Color.RED);
               mSensorChangedTextView2.setTextColor(Color.RED);
               isInside = false;
               innerRegionOfInterest.setVisibility(View.VISIBLE);
           }
       }

        if(mCurrentButtonIndex==1||mCurrentButtonIndex==2)
        {

            float regionWidth,regionHeight;
            float widthForInnerRegion;
            float heightForInnerRegion;

            regionWidth=mVerticalGuaze.getWidth();
            regionHeight=mVerticalGuaze.getHeight();

        /*Log.d("TAG","rWidth--"+regionWidth);
        Log.d("TAG","rHeight--"+regionHeight);

        Log.d("TAG","lViewWidth--"+LEFT_VIEW_WIDTH);
        Log.d("TAG","rViewWidth--"+RIGHT_VIEW_WIDTH);
*/
      /*      if (IS_HYSTERESIS_ON == 1) {
                if (isInside == false)
                {
                    widthForInnerRegion = (((validPitch / 2) / criticalPitch) * (regionWidth / 4) * 2) + (WIDTH_BALL / 2) * 2;
                    heightForInnerRegion = (((validRoll / 2) / criticalRoll) * (regionHeight / 4) * 2) + (WIDTH_BALL / 2) * 2;



                } else {
                    widthForInnerRegion = (((validPitch) / criticalPitch) * (regionWidth / 4) * 2) + (WIDTH_BALL / 2) * 2;
                    heightForInnerRegion = (((validRoll) / criticalRoll) * (regionHeight / 4) * 2) + (WIDTH_BALL / 2) * 2;



                }
            } else {
                widthForInnerRegion = (((validPitch) / criticalPitch) * (regionWidth / 4) * 2) + (WIDTH_BALL / 2) * 2;
                heightForInnerRegion = (((validRoll) / criticalRoll) * (regionHeight / 4) * 2) + (WIDTH_BALL / 2) * 2;

            }

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) innerRegionOfInterest.getLayoutParams();
            RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) innerRegionOfInterest.getLayoutParams();

            params2.width = (int) widthForInnerRegion;
            params.height = (int) heightForInnerRegion;
            innerRegionOfInterest.setLayoutParams(params);
            innerRegionOfInterest.setLayoutParams(params2);
*/

            // Log.d("TAG", "isInside--" +isInside);pe

       /* Log.d("TAG","lViewWidth--"+LEFT_VIEW_WIDTH);
        Log.d("TAG","rViewWidth--"+RIGHT_VIEW_WIDTH);*/


            //for displacement of ball according to sensor values
            ballx = mSensorChangeValueImageView.getX();
            bally = mSensorChangeValueImageView.getY();
            ballx = mSensorChangeValueImageView.getX() + (WIDTH_BALL / 2);
            bally = mSensorChangeValueImageView.getY() + (WIDTH_BALL / 2);

            float ballVX, ballVY, ballHX, ballHY;

           /* ballVX = mVerticalGuaze.getX();
            ballVY = mVerticalGuaze.getY();


            ballHX = mHorizontalGuaze.getX();
            ballHY = mHorizontalGuaze.getY();*/



            float devPitch = pitch - optimalPitch;
            float devRoll = optimalRoll - roll;


            if (abs(devPitch) < criticalPitch) {
                ballx = (regionWidth / 2) + (regionWidth / 4) * (devPitch / criticalPitch);
                ballx = ballx + LEFT_VIEW_WIDTH;
                //                       ballx = ballx + self.leftView.frame.size.width  - WIDTH_BALL/2;
                //# fast ball motion
                ballVX = (VERTICAL_GAUZE_WIDTH/2) + (VERTICAL_GAUZE_WIDTH/4) * (devPitch/criticalPitch);

                ballHX = (HORIZONTAL_GAUZE_WIDTH/2) + (HORIZONTAL_GAUZE_WIDTH/4) * (devPitch/criticalPitch);
                ballHX = ballHX + 5;
            } else {
                //# slow ball motion
                ballx = (regionWidth / 2) + (regionWidth / 4) * (devPitch / abs(devPitch)) * (1 + (abs(devPitch) - criticalPitch) / (maxOffPitch - criticalPitch));
                ballx = ballx + LEFT_VIEW_WIDTH;
                //                       ballx = ballx + self.leftView.frame.size.width - WIDTH_BALL/2;
                ballVX = (VERTICAL_GAUZE_WIDTH/2) + (VERTICAL_GAUZE_WIDTH/4) * (devPitch/abs(devPitch))*(1 + (abs(devPitch)-criticalPitch)/(maxOffPitch - criticalPitch));

                ballHX = (HORIZONTAL_GAUZE_WIDTH/2) + (HORIZONTAL_GAUZE_WIDTH/4) * (devPitch/abs(devPitch))*(1 + (abs(devPitch)-criticalPitch)/(maxOffPitch - criticalPitch));
            }
            if (abs(devRoll) < criticalRoll) {
                bally = (regionHeight / 2) + (regionHeight / 4) * devRoll / criticalRoll;
                ballVY = (VERTICAL_GAUZE_HEIGHT/2) + (VERTICAL_GAUZE_HEIGHT/4) * devRoll/criticalRoll;
                ballHY = (HORIZONTAL_GAUZE_HEIGHT/2) + (HORIZONTAL_GAUZE_HEIGHT/4) * devRoll/criticalRoll;
                //                       bally = bally - WIDTH_BALL/2;
            } else {
                bally = (regionHeight / 2) + (regionHeight / 4) * (devRoll / abs(devRoll)) * (1 + (abs(devRoll) - criticalRoll) / (maxOffRoll - criticalRoll));
                //                       bally = bally - WIDTH_BALL/2;
                ballVY = (VERTICAL_GAUZE_HEIGHT/2) + (VERTICAL_GAUZE_HEIGHT/4)* (devRoll/abs(devRoll))*(1 + (abs(devRoll)-criticalRoll)/(maxOffRoll - criticalRoll));
                ballHY = (HORIZONTAL_GAUZE_HEIGHT/2) + (HORIZONTAL_GAUZE_HEIGHT/4)* (devRoll/abs(devRoll))*(1 + (abs(devRoll)-criticalRoll)/(maxOffRoll - criticalRoll));
            }


            if (ballx > (regionWidth + (LEFT_VIEW_WIDTH - (WIDTH_BALL / 2)))) {
                ballx = regionWidth + (LEFT_VIEW_WIDTH - (WIDTH_BALL / 2));
            } else if (ballx < LEFT_VIEW_WIDTH + (WIDTH_BALL / 2)) {
                ballx = LEFT_VIEW_WIDTH + (WIDTH_BALL / 2);
            }

            if (bally > regionHeight - (WIDTH_BALL / 2)) {
                bally = regionHeight - (WIDTH_BALL / 2);
            } else if (bally < (WIDTH_BALL / 2)) {
                bally = (WIDTH_BALL / 2);
            }

            if ( ballVX > VERTICAL_GAUZE_WIDTH - 5) {
                ballVX = VERTICAL_GAUZE_WIDTH - 5;
            }
            else if ( ballVX < minXVertical + 5){
                ballVX = minXVertical + 5;
            }

            if (ballVY > VERTICAL_GAUZE_HEIGHT - 5){
                ballVY = VERTICAL_GAUZE_HEIGHT - 5;
            }
            else if (ballVY < 5){
                ballVY = 5;
            }

            if ( ballHX > HORIZONTAL_GAUZE_WIDTH - 5) {
                ballHX = HORIZONTAL_GAUZE_WIDTH - 5;
            }
            else if ( ballHX < minXHorizontal + 5){
                ballHX = minXHorizontal + 5;
            }

            if (ballHY > HORIZONTAL_GAUZE_HEIGHT - 5) {
                ballHY = HORIZONTAL_GAUZE_HEIGHT - 5;
            }
            else if (ballHY < 5){
                ballHY = 5;
            }


            float VX = ballVX - (WIDTH_BALL / 2);
            float HX = ballHX - (WIDTH_BALL / 2);
            float VY = ballVY - (WIDTH_BALL / 2);
            float HY = ballHY - (WIDTH_BALL / 2);

           /* mSensorChangeValueImageView.animate()
                    .translationX(ballVX-(WIDTH_BALL/2))
                    .translationY(ballVY-(WIDTH_BALL/2))
                    .setInterpolator(new AccelerateInterpolator())
                    .setDuration(animDuration);
            mSensorChangeValueImageView2.animate()
                    .translationX(ballHX-(WIDTH_BALL/2))
                    .translationY(ballHY-(WIDTH_BALL/2))
                    .setInterpolator(new AccelerateInterpolator())
                    .setDuration(animDuration);*/
            /* float x=mSensorChangeValueImageView.getX();
            float y=mSensorChangeValueImageView.getY();*/
            if (((VX >= minXVertical) && (VX <=VERTICAL_GAUZE_WIDTH-WIDTH_BALL)) ||((VY>=mVerticalGuaze.getY() )&& (VY<=VERTICAL_GAUZE_HEIGHT-WIDTH_BALL))) {
                mSensorChangeValueImageView.animate()
                        .translationX(0)
                        .translationY(VY)
                        .setInterpolator(new AccelerateInterpolator())
                        .setDuration(animDuration);

               /* mSensorChangeValueImageView.setX(x);
                mSensorChangeValueImageView.setY(y);*/
            }
            else
            {
                if (VX < 0) {
                    mSensorChangeValueImageView.animate()
                            .translationX(0)
                            .translationY(VY)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                   /* mSensorChangeValueImageView.setX(minXVertical);
                    mSensorChangeValueImageView.setY(y);*/
                }
                if (VX + WIDTH_BALL > (VERTICAL_GAUZE_WIDTH- WIDTH_BALL))
                {

                    mSensorChangeValueImageView.animate()
                            .translationX(0)
                            .translationY(VY)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                   /* mSensorChangeValueImageView.setX(mMaxXBound - WIDTH_BALL);
                    mSensorChangeValueImageView.setY(y);*/
                }
                if (VY < 0) {
                    mSensorChangeValueImageView.animate()
                            .translationX(0)
                            .translationY(0)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                   /* mSensorChangeValueImageView.setX(minXVertical);
                    mSensorChangeValueImageView.setY(y);*/
                }
                if (VY + WIDTH_BALL > (VERTICAL_GAUZE_HEIGHT))
                {

                    mSensorChangeValueImageView.animate()
                            .translationX(0)
                            .translationY(VERTICAL_GAUZE_HEIGHT-WIDTH_BALL)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                   /* mSensorChangeValueImageView.setX(mMaxXBound - WIDTH_BALL);
                    mSensorChangeValueImageView.setY(y);*/
                }
            }

            if (((HY >=0) && (HY <=HORIZONTAL_GAUZE_HEIGHT-WIDTH_BALL)) ||((HX>=0 )&& (HX<=HORIZONTAL_GAUZE_WIDTH-WIDTH_BALL))) {
                mSensorChangeValueImageView2.animate()
                        .translationX(HX)
                        .translationY(0)
                        .setInterpolator(new AccelerateInterpolator())
                        .setDuration(animDuration);
               /*
                mSensorChangeValueImageView.setX(x);
                mSensorChangeValueImageView.setY(y);*/
            }
            else
            {
                if (HY < 0) {
                    mSensorChangeValueImageView2.animate()
                            .translationX(HX)
                            .translationY(0)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                   /* mSensorChangeValueImageView.setX(minXVertical);
                    mSensorChangeValueImageView.setY(y);*/
                } else if (HY + WIDTH_BALL > (HORIZONTAL_GAUZE_HEIGHT))
                {

                    mSensorChangeValueImageView2.animate()
                            .translationX(HX)
                            .translationY(0)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                    /*mSensorChangeValueImageView.setX(mMaxXBound - WIDTH_BALL);
                    mSensorChangeValueImageView.setY(y);*/
                } else if (HX < 0) {
                    mSensorChangeValueImageView2.animate()
                            .translationX(minXHorizontal)
                            .translationY(0)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                    /*mSensorChangeValueImageView.setX(minXVertical);
                    mSensorChangeValueImageView.setY(y);*/
                } else if (HX + WIDTH_BALL > HORIZONTAL_GAUZE_WIDTH)
                {
                    mSensorChangeValueImageView2.animate()
                            .translationX(HORIZONTAL_GAUZE_WIDTH-WIDTH_BALL)
                            .translationY(0)
                            .setInterpolator(new AccelerateInterpolator())
                            .setDuration(animDuration);
                   /* mSensorChangeValueImageView.setX(mMaxXBound - WIDTH_BALL);
                    mSensorChangeValueImageView.setY(y);*/
                }
            }

            Log.d("hello","minYHorizontal--"+minYHorizontal);
            Log.d("hello","horizontal Guaze Y--"+mHorizontalGuaze.getY());
            Log.d("hello","horizontal ball Y--"+mSensorChangeValueImageView2.getY());

        }
        else
        {
            mCaptureImageButton.setEnabled(true);
            mCropViewLayout.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
                @Override
                public boolean onPreDraw() {
                    captureRegionWidth = mCropViewLayout.getWidth();
                    captureRegionHeight = mCropViewLayout.getHeight();
                    return true;
                }
            });

            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) sideViewLine.getLayoutParams();
            //RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) sideViewLine.getLayoutParams();


            params.height =captureRegionHeight;
            innerRegionOfInterest.setLayoutParams(params);
            sideViewLine.setX(captureRegionWidth*sideViewlinePos);
            sideViewLine.setY(0);
        }
        Log.d("TAG","ballx--"+ballx);
        Log.d("TAG","bally--"+bally);
        //  Log.d("TAG","SENSOR_DELAY:"+SENSOR_DELAY);
/*mSensorChangeValueImageView.setX(ballx-(WIDTH_BALL/2));
mSensorChangeValueImageView.setY(bally-(WIDTH_BALL/2));
*/

     /*   mSensorChangeValueImageView.animate()
                       .translationX(x)
                       .translationY(y)
                       .setInterpolator(new AccelerateInterpolator())
                       .setDuration(200);
*/


        //mSensorChangeValueImageView.animate().setStartDelay(500);
        // movingBall.frame = CGRectMake(ballx - (WIDTH_BALL/2), bally - (WIDTH_BALL/2), WIDTH_BALL, WIDTH_BALL);
//        setPosition();

        /*Log.d("X: ", Float.toString(mSensorChangeValueImageView.getX()));
        Log.d("Y: ", Float.toString(mSensorChangeValueImageView.getY()));
        Log.d("Width: ", Float.toString(WIDTH));
        Log.d("Height: ", Float.toString(HEIGHT));
*/

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    // pratik's code

   /* public void updateOrientationDisplay() {

        double azimuthValue = sensorFusion.getAzimuth();
        double rollValue = sensorFusion.getRoll();
        double pitchValue = sensorFusion.getPitch();

        pitch = pitchValue;
        Log.i(TAG, "pitch " + pitch);
        roll = rollValue;
        Log.i(TAG, "roll " + roll);
        yaw = azimuthValue;
        Log.i(TAG, "yaw " + yaw);

        if (mCurrentButtonIndex == 1) {
            optimalPitch = optimalPitch_tv;
            optimalRoll = optimalRoll_tv;
            maxOffPitch = maxOffPitch_tv;
            maxOffRoll = maxOffRoll_tv;
            criticalPitch = criticalPitch_tv;
            criticalRoll = criticalRoll_tv;

            validPitch = validPitch_tv;
            validRoll = validRoll_tv;
        } else if (mCurrentButtonIndex == 2) {
            optimalPitch = optimalPitch_tsv;
            optimalRoll = optimalRoll_tsv;
            maxOffPitch = maxOffPitch_tsv;
            maxOffRoll = maxOffRoll_tsv;
            criticalPitch = criticalPitch_tsv;
            criticalRoll = criticalRoll_tsv;

            validPitch = validPitch_tsv;
            validRoll = validRoll_tsv;
        } else if (mCurrentButtonIndex == 3) {
            optimalPitch = optimalPitch_sv;
            optimalRoll = optimalRoll_sv;
            maxOffPitch = maxOffPitch_sv;
            maxOffRoll = maxOffRoll_sv;
            criticalPitch = criticalPitch_sv;
            criticalRoll = criticalRoll_sv;

            validPitch = validPitch_sv;
            validRoll = validRoll_sv;
        }

        validPitch_max = optimalPitch + validPitch;
        validPitch_min = optimalPitch - validPitch;

        validRoll_max = optimalRoll + validRoll;
        validRoll_min = optimalRoll - validRoll;

        if (pitch >= validPitch_min && pitch <= validPitch_max) {
            mSensorChangedTextView1.setTextColor(Color.parseColor("#5fc260"));
        } else {
            mSensorChangedTextView1.setTextColor(Color.parseColor("#E60000"));
        }

        if (roll >= validRoll_min && roll <= validRoll_max) {
            mSensorChangedTextView2.setTextColor(Color.parseColor("#5fc260"));
        } else {
            mSensorChangedTextView2.setTextColor(Color.parseColor("#E60000"));
        }

        mSensorChangedTextView1.setText(String.format(Locale.getDefault(), "p : %.1f", -pitch));
        mSensorChangedTextView2.setText(String.format(Locale.getDefault(), "r : %.1f", roll));

        moveX = (int) (xCenter + (-pitch * mYCalculatedStepMoveFactor));
        moveY = (int) (yCenter + (-roll * mXCalculatedStepMoveFactor));

        Log.i(TAG, "moveX " + moveX);
        Log.i(TAG, "moveY " + moveY);

        if (moveX > mMinXBound && moveX < mMaxXBound) {
            x = moveX;
        } else if (moveX <= mMinXBound) {
            x = mMinXBound;
        } else if (moveX >= mMaxXBound) {
            x = mMaxXBound;
        }

        if (moveY > mMinYBound && moveY < mMaxYBound) {
            y = moveY;
        } else if (moveY <= mMinYBound) {
            y = mMinYBound;
        } else if (moveY >= mMaxYBound) {
            y = mMaxYBound;
        }

        Log.i(TAG, " old x = " + mSensorChangeValueImageView.getX() + " old y = " + mSensorChangeValueImageView.getY());

        ObjectAnimator animX = ObjectAnimator.ofFloat(mSensorChangeValueImageView, "x", x);
        animX.setInterpolator(new LinearInterpolator());
        ObjectAnimator animY = ObjectAnimator.ofFloat(mSensorChangeValueImageView, "y", y);
        animY.setInterpolator(new LinearInterpolator());

        if (x < mMinXBound) {
            mSensorChangeValueImageView.setX(mMinXBound);
        } else if (x > mMaxXBound) {
            mSensorChangeValueImageView.setX(mMaxXBound);
        }

        if (y < mMinYBound) {
            mSensorChangeValueImageView.setY(mMinYBound);
        } else if (y > mMaxYBound) {
            mSensorChangeValueImageView.setY(mMaxYBound);
        }

        AnimatorSet animSetXY = new AnimatorSet();
        animSetXY.playTogether(animX, animY);
        animSetXY.setDuration(100);
//        animSetXY.start();

//        if (Math.abs(x - mSensorChangeValueImageView.getX()) > 15) {
//            animSetXY.start();
//        } else {
            mSensorChangeValueImageView.setX(x);
//        }

//        if (Math.abs(y - mSensorChangeValueImageView.getY()) > 15) {
//            animSetXY.start();
//        } else {
            mSensorChangeValueImageView.setY(y);
//        }
    }*/

    public void registerSensorManagerListeners() {
        accel = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        compass = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        gyro = sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        sensorManager.registerListener(this,
                accel,SENSOR_DELAY);

        sensorManager.registerListener(this,
                compass,
                SENSOR_DELAY);

        sensorManager.registerListener(this,
                gyro,
                SENSOR_DELAY);

    }

//    private void initFusionSensor() {
//        sensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
//        registerSensorManagerListeners();
//
//        d.setMaximumFractionDigits(2);
//        d.setMinimumFractionDigits(2);
//
//        sensorFusion = new SensorFusion(this);
//        sensorFusion.setMode(SensorFusion.Mode.GYRO);
//    }

//    @Overrifini
}
