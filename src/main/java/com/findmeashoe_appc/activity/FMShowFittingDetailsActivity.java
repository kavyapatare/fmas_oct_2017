package com.findmeashoe_appc.activity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMProduct;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;

import org.json.JSONException;
import org.json.JSONObject;

public class FMShowFittingDetailsActivity extends AppCompatActivity {
    public static final String TAG = "FMShowFittingDetails";
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private LinearLayout mBackButton;
    private LinearLayout mMenuButton;
    private Context mContext;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private FMCustomer mCurrentFMCustomer;
    private WebView mCurrentSelectedShoeWebView;
    private FMProduct mCurrentProduct;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_fitting_details);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the show fitting details screen ui
     */
    @SuppressLint("JavascriptInterface")
    private void initializeView() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        Intent intent = getIntent();
        mCurrentProduct = (FMProduct) intent.getSerializableExtra("Current Product");
        IAUtilities.showingLogs(TAG, "mCurrentProduct name = " + mCurrentProduct.getProductName(), IAUtilities.LOGS_SHOWN);
        mToolbar = (Toolbar) findViewById(R.id.toolbar_show_fitting_details);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mBackButton = (LinearLayout) findViewById(R.id.web_back_button_ll);
        mMenuButton = (LinearLayout) findViewById(R.id.web_menu_button_ll);
        mCurrentSelectedShoeWebView = (WebView) findViewById(R.id.shoe_fitting_details_widget_wv);

        mToolbar.setVisibility(View.GONE);
        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        mCurrentFMCustomer = new FMCustomer();
        mCurrentFMCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));
        if (!mCurrentFMCustomer.getCustomerEmailId().equalsIgnoreCase("")) {
            mToolBarCustomerNameTextView.setText("Hello " + mCurrentFMCustomer.getCustomerFirstName() + " !");
        }

        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });
        final String mWidgetXmlString = "<!DOCTYPE html>" +
                
                "    <html lang='en'>" +
                
                "    <head>" +
                
                "        <meta charset='UTF-8'>" +

                "        <meta name='viewport' content='initial-scale=0.58, user-scalable=no, width=device-width, height=device-height, target-densitydpi=device-dpi'>" +
                
                "        <title>FMAS</title>" +

                "        <script src='http://uwlive.findmeashoe.com/jquery.min.js' type='text/javascript'></script>" +

                "        <script src='http://uwlive.findmeashoe.com/md5.js' type='text/javascript'></script>" +

                "        " +

                "        <script src='http://uwlive.findmeashoe.com/fmas-universal-widget-latest-min.js' type='text/javascript'></script>" +

                "        <link href='http://uwlive.findmeashoe.com/fmas-universal-widget-latest-min.css'  title='uwstyle' rel='stylesheet' />" +

                "        <link href='http://uwlive.findmeashoe.com/fmas-s2s-latest-min.css' rel='stylesheet' />" +

                "        " +
                
                "        <script type='text/javascript'>" +
                
                "        $(function() {" +
                
                "            if (typeof(FmasUW) !== 'undefined') {" +
                
                "                FmasUW.MainController.initWidget({" +
                
                "                    skuId: '" + mCurrentProduct.getProductId() + "'," +
                
                "                    sizeStandard: \"" + FMUrl.SERVICE_STANDARDS + "\"," +
                
                "                    sizeSelected: '0'," +

                "                    userStatus: 'init'," +
                
                "                    orgId: '" + FMUrl.ORG_ID + "'," +
                
                "                    parent: 'fmas-parent'," +
                
                "                    name: \"" + mCurrentProduct.getProductName() + "\"," +

                "                    customerId: ''," +

                "                    sessionId: '69bc35ed139ea9f9f35f7b3d134693c1'," +

                "                    customerEmailId: '" + mCurrentFMCustomer.getCustomerEmailId() + "'," +

                "                    callback: function (o) {" +
                
                "                        if (console) {" +

                "                            console.dir(o);" +

                "                        }" +

                "                    }" +

                "                });" +

                "            }" +

                "        });" +

                "        " +

                "        $('#fmas-parent').ready(function() {" +

                "        setTimeout(function(){" +

                "                $('.fmas-parent').trigger( 'click' );" +

                "                $('a').trigger( 'click' );" +

                "                document.getElementById('fake-parent').contentWindow.document.location.href='scheme://';" +

                "            }, 200);" +

                "        });" +

                "        $(document).bind('DOMNodeRemoved', function (e) {" +

                "            if(e.target.nodeName === 'A' ){" +

                "                fillData();" +

                "            }" +
                "        });" +
                "        function fillData(){" +
                "            sizeSelected = FmasOutputObj.sizeSelected;" +
                "           alert(sizeSelected);"+
                "            window.location.href = 'scheme2://';" +
                "        }" +
                "        function getFilledData(){" +
                "            return sizeSelected;" +
                "        }" +
                "    </script>" +
                "    </head>" +
                "    <body>" +
                "        <div id='fmas-parent' style='display:none'></div>" +
                "        <iframe id='fake-parent' style='display:none'></div>" +
                "    </body>" +
                "    </html>";
        mCurrentSelectedShoeWebView.getSettings().setJavaScriptEnabled(true);
        mCurrentSelectedShoeWebView.setWebChromeClient(new GetSelectedSizeWebChromeClient());
        mCurrentSelectedShoeWebView.getSettings().setAllowFileAccess(true);//for making chrome the default browser
        mCurrentSelectedShoeWebView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                if (!((FMShowFittingDetailsActivity)mContext).isFinishing()) {
                    progressDialog.dismiss();
                }

                IAUtilities.showingLogs(TAG, "onpage finished url == " + url, IAUtilities.LOGS_SHOWN);

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                IAUtilities.showingLogs(TAG, "webview url = " + url, IAUtilities.LOGS_SHOWN);
                if (url == "scheme://"){
                } else if (url == "scheme2://"){
                }
                return true;
            }
        }); // for setting webview
        mCurrentSelectedShoeWebView.loadDataWithBaseURL(null,mWidgetXmlString,null,"text/html", "UTF-8");
        mCurrentSelectedShoeWebView.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        mCurrentSelectedShoeWebView.getSettings().setLoadWithOverviewMode(true);
        mCurrentSelectedShoeWebView.getSettings().setUseWideViewPort(true);

    }

    /**
     * used to setup the custom webview chrome client
     */
    public class GetSelectedSizeWebChromeClient extends WebChromeClient{
        @Override
        public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
            IAUtilities.showingLogs(TAG, "message = " + message, IAUtilities.LOGS_SHOWN);
            IAUtilities.showingLogs(TAG, "url = " + url, IAUtilities.LOGS_SHOWN);

            if (!message.equalsIgnoreCase("undefined")){
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("SelectedShoe", mCurrentProduct.getProductName());
                    jsonObject.put("SelectedSize",Double.parseDouble(message));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                result.confirm();
                finish();
                new IAUtilities().setSharedPreferences(mContext, jsonObject.toString(), "SelectedShoeSize");
                Toast.makeText(mContext,"The selected size for " + mCurrentProduct.getProductName() + " is " + Double.parseDouble(message),Toast.LENGTH_SHORT).show();
            }else{
                result.confirm();
                finish();
            }
            return true ;
        }
    }
}
