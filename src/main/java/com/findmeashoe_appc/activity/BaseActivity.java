package com.findmeashoe_appc.activity;

import android.support.v7.app.AppCompatActivity;

import com.findmeashoe_appc.utils.IAUtilities;
import com.parse.ParseInstallation;

/**
 * Created by iaurokitkat on 30/12/15.
 */
public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    @Override
    protected void onResume() {
        super.onResume();
        ParseInstallation.getCurrentInstallation().getUpdatedAt();
        ParseInstallation.getCurrentInstallation().saveInBackground();
        IAUtilities.showingLogs(TAG, "current installation = " + ParseInstallation.getCurrentInstallation().getUpdatedAt(), IAUtilities.LOGS_SHOWN);
    }
}
