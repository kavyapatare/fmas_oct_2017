package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;
import com.parse.ParseObject;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

public class FMUploadImagesRevisedActivity extends AppCompatActivity implements FMWebConnection.ProgressUpdateCallback {
    private Toolbar mToolbar;
    public static final String TAG = "FMUploadImagesRevised";
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private Context mContext;
    private FMCustomer mCurrentFMCustomer;
    private ProgressBar mProgressBar;
    private Bitmap mTopViewBitmap, mAngleViewBitmap, mSideViewBitmap;
    private String mFileStoragePath;
    private String mTopViewPath;
    private String mAngleViewPath;
    private String mSideViewPath;
    private String allReadings;
    private IACustomButton mCancelUploadButton;
    private AddCustomerDetailsAsyncTask mAddCustomerDetailsAsyncTask;
    private UploadImagesAsyncTask mUploadImagesAsyncTask;
    private GetMeasurementAsyncTask mGetMeasurementAsyncTask;
    private IACustomTextView mUploadStatusTextView;
    private String mFootResult;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private LinearLayout mToolbarMenuButton;
    private ParseObject mUploadSpeedObject;
    private File mTopViewImageFile;
    private File mAngleViewImageFile;
    private File mSideViewImageFile;
    private boolean mIsCancelled = false;
    private String mCameraPictureSize;
    private ImageView tvImage,tsvImage,svImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_images_revised);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the upload images screen
     */
    private void initializeView() {
        mCameraPictureSize = getIntent().getStringExtra("image_resolution");
        allReadings = getIntent().getStringExtra("allReading");

        mFileStoragePath = mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES) +
                File.separator + "com.findmeashoe_appc.camera" + File.separator;

        mTopViewPath = mFileStoragePath + "/image_1.png";
        mAngleViewPath = mFileStoragePath + "/image_2.png";
        mSideViewPath = mFileStoragePath + "/image_3.png";

        mToolbar = (Toolbar) findViewById(R.id.toolbar_preview_revised);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mCancelUploadButton = (IACustomButton) findViewById(R.id.cancel_btn);
        mProgressBar = (ProgressBar) findViewById(R.id.Progressbar);
        mUploadStatusTextView = (IACustomTextView) findViewById(R.id.upload_status_tv);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        mCurrentFMCustomer = new FMCustomer();
        mCurrentFMCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));

        mToolBarCustomerNameTextView.setText("Hello " + mCurrentFMCustomer.getCustomerFirstName() + " !");
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });

        tvImage=(ImageView)findViewById(R.id.img_tv);
        tsvImage=(ImageView)findViewById(R.id.img_tsv);
        svImage=(ImageView)findViewById(R.id.img_sv);

        mTopViewImageFile = new File(mTopViewPath);
        mAngleViewImageFile = new File(mAngleViewPath);
        mSideViewImageFile = new File(mSideViewPath);

        if (mTopViewImageFile.exists() && mAngleViewImageFile.exists() && mSideViewImageFile.exists()) {
            mTopViewBitmap = BitmapFactory.decodeFile(mTopViewImageFile.getAbsolutePath());
            mAngleViewBitmap = BitmapFactory.decodeFile(mAngleViewImageFile.getAbsolutePath());
            mSideViewBitmap = BitmapFactory.decodeFile(mSideViewImageFile.getAbsolutePath());

            tvImage.setImageBitmap(mTopViewBitmap);
            tsvImage.setImageBitmap(mAngleViewBitmap);
            svImage.setImageBitmap(mSideViewBitmap);
        }

        mUploadStatusTextView.setText(mContext.getResources().getString(R.string.almost_done));

        mCancelUploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mIsCancelled = true;
                mCancelUploadButton.setEnabled(false);
                goToOnUploadUnsuccessfulActivity("", "Sorry,something went wrong!", mIsCancelled);
                mProgressBar.setProgress(0);
                if (mAddCustomerDetailsAsyncTask != null) {
                    mAddCustomerDetailsAsyncTask.cancel(true);
                }
                if (mUploadImagesAsyncTask != null) {
                    mUploadImagesAsyncTask.cancel(true);
                }
                if (mGetMeasurementAsyncTask != null) {
                    mGetMeasurementAsyncTask.cancel(true);
                }
                mCancelUploadButton.setEnabled(true);
            }
        });
        if (!new IAUtilities().isNetworkAvailable(mContext)) {
            goToOnUploadUnsuccessfulActivity(getResources().getString(R.string.network_error), "", false);
        } else {
            if (mTopViewImageFile.length() > 0
                    && mAngleViewImageFile.length() > 0
                    && mSideViewImageFile.length() > 0) {
                mAddCustomerDetailsAsyncTask = new AddCustomerDetailsAsyncTask(mCurrentFMCustomer);
                mAddCustomerDetailsAsyncTask.execute();
            } else {
                Toast.makeText(mContext, "Unable to access foot images. Please retake images.", Toast.LENGTH_LONG).show();
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAddCustomerDetailsAsyncTask != null) {
            mAddCustomerDetailsAsyncTask = null;
        }
        if (mUploadImagesAsyncTask != null) {
            mUploadImagesAsyncTask = null;
        }
        if (mGetMeasurementAsyncTask != null) {
            mGetMeasurementAsyncTask = null;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mAddCustomerDetailsAsyncTask != null) {
            mAddCustomerDetailsAsyncTask.cancel(true);
        }
        if (mUploadImagesAsyncTask != null) {
            mUploadImagesAsyncTask.cancel(true);
        }
        if (mGetMeasurementAsyncTask != null) {
            mGetMeasurementAsyncTask.cancel(true);
        }
        finish();
    }


    /**
     * used to call the add user api
     */
    private class AddCustomerDetailsAsyncTask extends AsyncTask<Void, Void, FMResponse> {

        private FMCustomer mFMCustomer;

        public AddCustomerDetailsAsyncTask(FMCustomer mFMCustomer) {
            this.mFMCustomer = mFMCustomer;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (mFMCustomer.getCustomerEmailId() != null && mFMCustomer.getCustomerPassword() != null) {

                    jsonObject.put("tag", "adduser_tag");
                    IAUtilities.showingLogs(TAG, "tag = " + jsonObject.get("tag"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("memberEmail", mFMCustomer.getCustomerEmailId());
                    IAUtilities.showingLogs(TAG, "memberEmail = " + jsonObject.getString("memberEmail"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("username", mFMCustomer.getCustomerEmailId().replaceAll("[^A-Za-z0-9]", ""));
                    IAUtilities.showingLogs(TAG, "username = " + jsonObject.getString("username"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("gender", mFMCustomer.getCustomerGender());
                    IAUtilities.showingLogs(TAG, "gender = " + jsonObject.getString("gender"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("age", "30");
                    IAUtilities.showingLogs(TAG, "age = " + jsonObject.getString("age"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("email", FMUrl.COMMON_VENDOR_EMAIL_ID);
                    IAUtilities.showingLogs(TAG, "email = " + jsonObject.getString("email"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("width", "100");
                    IAUtilities.showingLogs(TAG, "width = " + jsonObject.getString("width"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("length", " ");
                    IAUtilities.showingLogs(TAG, "length = " + jsonObject.getString("length"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("toeType", " ");
                    IAUtilities.showingLogs(TAG, "toeType = " + jsonObject.getString("toeType"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("bigToeAngle", " ");
                    IAUtilities.showingLogs(TAG, "bigToeAngle = " + jsonObject.getString("bigToeAngle"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("smallToeAngle", " ");
                    IAUtilities.showingLogs(TAG, "smallToeAngle = " + jsonObject.getString("smallToeAngle"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("inStepHeight", " ");
                    IAUtilities.showingLogs(TAG, "inStepHeight = " + jsonObject.getString("inStepHeight"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("ballHeight", " ");
                    IAUtilities.showingLogs(TAG, "ballHeight = " + jsonObject.getString("ballHeight"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("user_email", mFMCustomer.getCustomerEmailId());

                    jsonObject.put("user_password", new IAUtilities().getSharedPreferences(mContext, "encryptedKey"));

                    jsonObject.put("ws_key", mContext.getResources().getString(R.string.ws_key));

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (new IAUtilities().getSharedPreferences(mContext, "ServerIP").length() > 0) {
                JSONObject mServerIpJsonObject;
                try {
                    mServerIpJsonObject = new JSONObject(new IAUtilities().getSharedPreferences(mContext, "ServerIP"));
                    FMUrl.CURRENT_IP_ADDRESS = mServerIpJsonObject.getString("CurrentIp");
                    if (FMUrl.CURRENT_IP_ADDRESS.equalsIgnoreCase("")) {
                        FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
                JSONObject mServerIpJsonObject = new JSONObject();
                try {
                    mServerIpJsonObject.put("CurrentIp", FMUrl.CURRENT_IP_ADDRESS);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new IAUtilities().setSharedPreferences(mContext, mServerIpJsonObject.toString(), "ServerIP");
                IAUtilities.showingLogs(TAG, "Json Object = " + mServerIpJsonObject.toString(), IAUtilities.LOGS_SHOWN);
            }

            FMWebConnection webConnection = new FMWebConnection();
            IAUtilities.showingLogs(TAG, "FMUrl.BASE_URL = " + FMUrl.BASE_URL + FMUrl.CURRENT_IP_ADDRESS + FMUrl.ADD_USER_FAMILY_URL, IAUtilities.LOGS_SHOWN);
            FMResponse FMResponse = webConnection.postCustomerData(FMUrl.BASE_URL + FMUrl.CURRENT_IP_ADDRESS + FMUrl.ADD_USER_FAMILY_URL, jsonObject);
            IAUtilities.showingLogs(TAG, "OnUploadFmasResponse == " + FMResponse, IAUtilities.LOGS_SHOWN);

            return FMResponse;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mProgressBar.setProgress(0);
        }

        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "AddCustomerDetailsAsyncTask completed", IAUtilities.LOGS_SHOWN);
                //go to next page
                mUploadImagesAsyncTask = new UploadImagesAsyncTask(mFMCustomer);
                mUploadImagesAsyncTask.execute();
            } else {
                //show error message
                goToOnUploadUnsuccessfulActivity(getResources().getString(R.string.network_error), "", false);
            }

        }
    }

    /**
     * used to call the Upload images api
     */
    private class UploadImagesAsyncTask extends AsyncTask<Void, Integer, FMResponse> {
        private FMCustomer mFMCustomer;

        public UploadImagesAsyncTask(FMCustomer mFMCustomer) {
            this.mFMCustomer = mFMCustomer;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setProgress(0);
            mProgressBar.setMax(100);
            Drawable customDrawable = getResources().getDrawable(R.drawable.custom_progressbar);
            mProgressBar.setProgressDrawable(customDrawable);
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            JSONObject jsonObject = new JSONObject();
            try {
                if (mFMCustomer.getCustomerEmailId() != null && mFMCustomer.getCustomerPassword() != null) {
                    jsonObject.put("tag", "client");
                    IAUtilities.showingLogs(TAG, "tag = " + jsonObject.get("tag"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("topView", new IAUtilities().bitmapToBase64(mTopViewBitmap));
                    IAUtilities.showingLogs(TAG, "topView = " + jsonObject.getString("topView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("topsideView", new IAUtilities().bitmapToBase64(mAngleViewBitmap));
                    IAUtilities.showingLogs(TAG, "topsideView = " + jsonObject.getString("topsideView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("sideView", new IAUtilities().bitmapToBase64(mSideViewBitmap));
                    IAUtilities.showingLogs(TAG, "sideView = " + jsonObject.getString("sideView"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("email", FMUrl.COMMON_VENDOR_EMAIL_ID);
                    IAUtilities.showingLogs(TAG, "email = " + jsonObject.getString("email"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("device", "Model : " + new IAUtilities().getDeviceName()
                            + " , Android OS : " + Build.VERSION.RELEASE
                            + " , Camera: " + new IAUtilities().getCameraDetails() + " MP "+allReadings);
                    IAUtilities.showingLogs(TAG, "device = " + jsonObject.getString("device"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("gender", mFMCustomer.getCustomerGender());
                    IAUtilities.showingLogs(TAG, "gender = " + jsonObject.getString("gender"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("age", mFMCustomer.getCustomerAge());
                    IAUtilities.showingLogs(TAG, "age = " + jsonObject.getString("age"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("username", mFMCustomer.getCustomerEmailId().replaceAll("[^A-Za-z0-9]", ""));
                    IAUtilities.showingLogs(TAG, "username = " + jsonObject.getString("username"), IAUtilities.LOGS_SHOWN);

                    try
                    {
                        jsonObject.put("appVersion", mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0).versionName);
                        IAUtilities.showingLogs(TAG, "appVersion = " + jsonObject.getString("appVersion"), IAUtilities.LOGS_SHOWN);
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }

                    jsonObject.put("orgID", FMUrl.ORG_ID);
                    IAUtilities.showingLogs(TAG, "orgID = " + jsonObject.getString("orgID"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("user_email", mFMCustomer.getCustomerEmailId());
                    IAUtilities.showingLogs(TAG, "user_email = " + jsonObject.getString("user_email"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("user_password", new IAUtilities().getSharedPreferences(mContext, "encryptedKey"));
                    IAUtilities.showingLogs(TAG, "user_password = " + jsonObject.getString("user_password"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("ws_key", mContext.getResources().getString(R.string.ws_key));
                    IAUtilities.showingLogs(TAG, "ws_key = " + jsonObject.getString("ws_key"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("paper_type", "3");
                    IAUtilities.showingLogs(TAG, "paper_type = " + jsonObject.getString("paper_type"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("foot_type", "0");
                    IAUtilities.showingLogs(TAG, "foot_type = " + jsonObject.getString("foot_type"), IAUtilities.LOGS_SHOWN);

                    jsonObject.put("geography", new IAUtilities().getSharedPreferences(mContext, "CountryCode"));
                    IAUtilities.showingLogs(TAG, "geography = " + jsonObject.getString("geography"), IAUtilities.LOGS_SHOWN);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (new IAUtilities().getSharedPreferences(mContext, "ServerIP").length() > 0) {
                JSONObject mServerIpJsonObject;
                try {
                    mServerIpJsonObject = new JSONObject(new IAUtilities().getSharedPreferences(mContext, "ServerIP"));
                    FMUrl.CURRENT_IP_ADDRESS = mServerIpJsonObject.getString("CurrentIp");
                    if (FMUrl.CURRENT_IP_ADDRESS.equalsIgnoreCase("")) {
                        FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
                JSONObject mServerIpJsonObject = new JSONObject();
                try {
                    mServerIpJsonObject.put("CurrentIp", FMUrl.CURRENT_IP_ADDRESS);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                new IAUtilities().setSharedPreferences(mContext, mServerIpJsonObject.toString(), "ServerIP");
                IAUtilities.showingLogs(TAG, "Json Object = " + mServerIpJsonObject.toString(), IAUtilities.LOGS_SHOWN);
            }

            FMWebConnection webConnection = new FMWebConnection(mContext);
            FMResponse mFMResponse = null;
            mFMResponse = webConnection.postCustomerWithErrorCodesData(FMUrl.BASE_URL + FMUrl.CURRENT_IP_ADDRESS
                    + FMUrl.FOOT_MEASURE_URL, jsonObject);

            IAUtilities.showingLogs(TAG, "OnUploadFmasResponse == " + mFMResponse, IAUtilities.LOGS_SHOWN);

            return mFMResponse;
        }

        @Override
        protected void onProgressUpdate(Integer... values)
        {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled()
        {
            super.onCancelled();
            mProgressBar.setProgress(0);
        }

        @Override
        protected void onPostExecute(FMResponse response) {
            mUploadStatusTextView.setText(mContext.getResources().getString(R.string.uploaded_successfully)
                    + "\n" + mContext.getResources().getString(R.string.waiting_for_response));
            super.onPostExecute(response);
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "UploadImagesAsyncTask completed", IAUtilities.LOGS_SHOWN);
                //go to next page
                if (response.getResponseData().contains(" Foot Not Detected ")) {
                    goToOnUploadUnsuccessfulActivity(response.getErrorCode(), response.getErrorMessage(), false);
                }
                if (response.getResponseData().contains("SmallToeAngle") && response.getResponseData().contains("ToeType")
                        && response.getResponseData().contains("FootHeight") && response.getResponseData().contains("FootWidth")
                        && response.getResponseData().contains("BallHeight") && response.getResponseData().contains("InStepHeight")) {
                    mGetMeasurementAsyncTask = new GetMeasurementAsyncTask(mFMCustomer);
                    mGetMeasurementAsyncTask.execute();                }
            } else {
                //show error message
                goToOnUploadUnsuccessfulActivity(response.getErrorCode(), response.getErrorMessage(), false);
            }
        }
    }

    /**
     * called to set the progress for the progress bar
     * @param mPercentageProgress
     */
    @Override
    public void onProgressUpdateSet(float mPercentageProgress) {
        mProgressBar.setProgress((int) Math.ceil(mPercentageProgress));
    }


    /**
     * used to call the Get Measurement api
     */
    private class GetMeasurementAsyncTask extends AsyncTask<Void, Integer, FMResponse> {
        private FMCustomer mCurrentFMCustomer;


        public GetMeasurementAsyncTask(FMCustomer mFMCustomer) {
            this.mCurrentFMCustomer = mFMCustomer;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            String mResultUrl = null;
            String mEmailID = null;

            mEmailID = mCurrentFMCustomer.getCustomerEmailId();
            IAUtilities.showingLogs(TAG, "email = " + mEmailID, IAUtilities.LOGS_SHOWN);

            mResultUrl = FMUrl.TARGET_BASE_URL + FMUrl.SERVICES_URL + FMUrl.GET_CUSTOMER_MEASUREMENT_URL + mEmailID;
            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse mFMResponse = null;
            mFMResponse = webConnection.getResponse(mResultUrl);

            IAUtilities.showingLogs(TAG, "OnGetMeasurementResponse == " + mFMResponse, IAUtilities.LOGS_SHOWN);

            return mFMResponse;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            mProgressBar.setProgress(0);
        }

        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "GetMeasurementAsyncTask completed", IAUtilities.LOGS_SHOWN);
                //go to next page
                try {
                    JSONObject jsonObject = new JSONObject(response.getResponseData());
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        JSONObject jsonObject1 = jsonObject.getJSONObject("data");
                        mFootResult = jsonObject1.toString();
                        goToOnSuccessfulResponseActivity();
                    } else {
                        goToOnUploadUnsuccessfulActivity(getResources().getString(R.string.network_error), "", false);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                //show error message
                goToOnUploadUnsuccessfulActivity(getResources().getString(R.string.network_error), "", false);
            }
        }
    }

    private void goToOnSuccessfulResponseActivity() {
        Intent intent = new Intent(FMUploadImagesRevisedActivity.this, FMOnSuccessfulResponseActivity.class);

        new IAUtilities().setSharedPreferences(mContext, mFootResult, "FootResult");
        startActivityForResult(intent, Integer.parseInt(mContext.getResources().getString(R.string.on_successful_response)));
    }

    /**
     * called to redirect the user to the upload unsuccessful screen
     * @param mErrorCode upload response error code
     * @param mErrorMessage upload response error message
     * @param mIsCancelled true, if cancelled by user
     */
    private void goToOnUploadUnsuccessfulActivity(String mErrorCode, String mErrorMessage, boolean mIsCancelled) {

        Intent intent = new Intent(FMUploadImagesRevisedActivity.this, FMOnUploadUnsuccessfulActivity.class);
        intent.putExtra("error code", mErrorCode);
        intent.putExtra("remark", mErrorMessage);
        intent.putExtra("is cancelled", mIsCancelled);
        startActivityForResult(intent, Integer.parseInt(mContext.getResources().getString(R.string.on_upload_unsuccessful)));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onactivity result -- Revised activity");
        if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.on_successful_response))) {
            if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result))) {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result)), data);
                finish();
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images))) {

                setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images)));
                finish();
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code))) {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code)));
                finish();
            }
        } else if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.on_upload_unsuccessful))) {
            if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.resend_images))) {
                initializeView();
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.retake_images))) {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_images)));
                finish();
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code))) {
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code)));
                finish();
            }
        } else if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.upload_images_revised))){
            Log.d(TAG, "onactivity result -- revisse activity retake");
            setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_images)));
            finish();
        }
    }
}
