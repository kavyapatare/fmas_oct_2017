package com.findmeashoe_appc.activity;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomEditText;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.SaveCallback;

public class FMFeedBackActivity extends AppCompatActivity implements RatingBar.OnRatingBarChangeListener{
    private Context mContext;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private IACustomButton mSendUserFeedbackButton;
    private RatingBar mUsefulnessRatingBar;
    private RatingBar mEaseOfUseRatingBar;
    private RatingBar mLikelyToRecommendRatingBar;
    private FMCustomer mCurrentFMCustomer;
    private IACustomEditText mCustomerCommentField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_back);
        mContext = this;
        initializeView();
    }

    /**
     * called to initialize the feedback screen ui
     */
    private void initializeView(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar_feedback);
        mToolBarCustomerNameTextView = (IACustomTextView)mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mSendUserFeedbackButton = (IACustomButton) findViewById(R.id.feedback_done_btn);
        mUsefulnessRatingBar = (RatingBar) findViewById(R.id.usefulness_rating_bar);
        mEaseOfUseRatingBar = (RatingBar) findViewById(R.id.ease_of_use_rating_bar);
        mLikelyToRecommendRatingBar = (RatingBar) findViewById(R.id.recommendation_rating_bar);
        mCustomerCommentField = (IACustomEditText) findViewById(R.id.feedback_comment_et);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();
        mCurrentFMCustomer = new FMCustomer();
        mCurrentFMCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));
        if (!mCurrentFMCustomer.getCustomerEmailId().equalsIgnoreCase("")){
            mToolBarCustomerNameTextView.setText("Hello " + mCurrentFMCustomer.getCustomerFirstName() + " !");
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            LayerDrawable layerDrawable = (LayerDrawable) mUsefulnessRatingBar.getProgressDrawable();
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(0)), getResources().getColor(R.color.light_gray));   // Empty star
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), getResources().getColor(android.R.color.transparent)); // Partial star
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)), getResources().getColor(R.color.gold));  // Full star
            mUsefulnessRatingBar.setProgressDrawable(layerDrawable);
            mEaseOfUseRatingBar.setProgressDrawable(layerDrawable);
            mLikelyToRecommendRatingBar.setProgressDrawable(layerDrawable);
        }

        mUsefulnessRatingBar.setOnRatingBarChangeListener(this);
        mEaseOfUseRatingBar.setOnRatingBarChangeListener(this);
        mLikelyToRecommendRatingBar.setOnRatingBarChangeListener(this);

        mSendUserFeedbackButton.setEnabled(false);

        mSendUserFeedbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSendUserFeedbackButton.setEnabled(false);
                ParseObject mFeedbackObject = new ParseObject("Feedback");
                mFeedbackObject.put("VirtualFittingUsefulness",mUsefulnessRatingBar.getRating());
                mFeedbackObject.put("Usability",mEaseOfUseRatingBar.getRating());
                mFeedbackObject.put("RecommendToFriend",mLikelyToRecommendRatingBar.getRating());
                mFeedbackObject.put("EmailId", mCurrentFMCustomer.getCustomerEmailId());
                mFeedbackObject.put("Comments", mCustomerCommentField.getText().toString());

                if(new IAUtilities().isNetworkAvailable(mContext)){
                    mFeedbackObject.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            openDialog(mContext.getResources().getString(R.string.feedback_is_appreciated));
                            mSendUserFeedbackButton.setEnabled(true);
                        }
                    });

                }else{
                    openDialog("Please try again when network is available.");
                }
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mSendUserFeedbackButton.setEnabled(true);
                    }
                },5000);
            }
        });

        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });

        mCustomerCommentField.setOnTextChangeListener(new IACustomEditText.onTextChangeListener() {
            @Override
            public void onTextChanged(String mText) {
                if (mUsefulnessRatingBar.getRating() < 1 || mEaseOfUseRatingBar.getRating() < 1 || mLikelyToRecommendRatingBar.getRating() < 1 || mText.length() < 1){
                    mSendUserFeedbackButton.setEnabled(false);
                } else {
                    mSendUserFeedbackButton.setEnabled(true);
                }
            }
        });
    }

    /**
     * called to open a popup message dialog
     * @param mMessageString the message to be shown
     */
    public void openDialog(final String mMessageString) {
        final Dialog dialog = new Dialog(mContext);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.default_dialog);
        TextView mMessageContentView = (TextView) dialog.findViewById(R.id.message_dialog_tv);
        mMessageContentView.setText(mMessageString);
        if (!((AppCompatActivity)mContext).isFinishing()){
            dialog.show();
        }
        IACustomButton mBackToDashboardButton = (IACustomButton) dialog.findViewById(R.id.dialog_done_btn);
        mBackToDashboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!((AppCompatActivity)mContext).isFinishing()){
                    dialog.dismiss();
                }
                finish();
            }
        });
    }

    /**
     * used to enable the send feedback button after user has
     * entered the readings
     * @param ratingBar the ratingbar to be monitored
     * @param rating the value of the rating
     * @param fromUser
     */
    @Override
    public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser) {
        if (mUsefulnessRatingBar.getRating() < 1 || mEaseOfUseRatingBar.getRating() < 1 || mLikelyToRecommendRatingBar.getRating() < 1 || mCustomerCommentField.getText().length() < 1){
            mSendUserFeedbackButton.setEnabled(false);
        } else {
            mSendUserFeedbackButton.setEnabled(true);
        }
    }
}
