package com.findmeashoe_appc.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.utils.IAUtilities;

import org.json.JSONObject;

/**
 * Created by kaveri on 11/10/17.
 */

public class FMChoosePaperTypeActivity extends Activity {
    RadioButton radioButtonYes,radioButtonNo;
    IACustomButton beginButton;
    RadioGroup radioGroup;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_paper_type);
        radioGroup=(RadioGroup)findViewById(R.id.radioButtonGroup);
        radioButtonYes=(RadioButton)findViewById(R.id.radioButtonYes);
        radioButtonNo=(RadioButton)findViewById(R.id.radioButtonNo);
        beginButton=(IACustomButton)findViewById(R.id.begin_btn);
        /*radioButtonYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButtonNo.setChecked(false);
             //   Intent i=new Intent(FMChoosePaperTypeActivity.this,FMGetStartedActivity.class);
                setResult(Integer.parseInt(getBaseContext().getResources().getString(R.string.retake_all_images)));
                finish();
            }
        });

        radioButtonNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                radioButtonYes.setChecked(false);
                //   Intent i=new Intent(FMChoosePaperTypeActivity.this,FMGetStartedActivity.class);
                setResult(Integer.parseInt(getBaseContext().getResources().getString(R.string.retake_all_images)));
                finish();
            }
        });*/
        beginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(radioButtonYes.isChecked())
                {
                    Log.d("TAG","Data--"+new IAUtilities().getSharedPreferences(FMChoosePaperTypeActivity.this, "CurrentFileIndexNo"));

                    Intent i=new Intent(FMChoosePaperTypeActivity.this,FMGetStartedActivity.class);
                    startActivity(i);

                    //  setResult(Integer.parseInt(getBaseContext().getResources().getString(R.string.retake_all_images)));
                    finish();
                }
                if(radioButtonNo.isChecked())
                {
                    Log.d("TAG","Data--"+new IAUtilities().getSharedPreferences(FMChoosePaperTypeActivity.this, "CurrentFileIndexNo"));
                    Intent i=new Intent(FMChoosePaperTypeActivity.this,FMGetStartedActivity.class);
                    startActivity(i);
                    finish();
                }

            }
        });
    }


    /*public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioButtonYes:
                if (checked)
                    radioButtonNo.setChecked(false);
                    // Pirates are the best
                    break;
            case R.id.radioButtonNo:

                if (checked)
                    radioButtonYes.setChecked(false);

                    // Ninjas rule
                    break;
        }
    }*/
    /*protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Integer.parseInt(this.getResources().getString(R.string.upload_images_revised))) {
            if (resultCode == Integer.parseInt(this.getResources().getString(R.string.see_catalog_result))) {
                setResult(Integer.parseInt(this.getResources().getString(R.string.see_catalog_result)), data);
                finish();
            } else if (resultCode == Integer.parseInt(this.getResources().getString(R.string.retake_images))) {
                setResult(Integer.parseInt(this.getResources().getString(R.string.retake_all_images)));
                finish();
            } else if (resultCode == Integer.parseInt(this.getResources().getString(R.string.retake_all_images))) {
                setResult(Integer.parseInt(this.getResources().getString(R.string.retake_all_images)));
                finish();
            } else if (resultCode == Integer.parseInt(this.getResources().getString(R.string.cancelled_result_code))) {

            }
        }
    }*/
}
