package com.findmeashoe_appc.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.adapters.FMCategoryGridAdapter;
import com.findmeashoe_appc.ds.FMCategory;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMProduct;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;
import com.findmeashoe_appc.web.FMWebConnection;
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class FMShowCategoryActivity extends AppCompatActivity {
    public static final String TAG = "FMShowCategoryActivity";
    private static final int MAX_CPU_CORES = Runtime.getRuntime().availableProcessors();
    private static final int MAX_THREADS = MAX_CPU_CORES * 2;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private Context mContext;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private GridView mMainCategoryGridView;
    private GridView mSubCategoryGridView;
    private ArrayList<FMCategory> mMainCategoryList;
    private ArrayList<FMCategory> mSubCategoryList;
    private FMCustomer mCurrentCustomer;
    private GetMainCategoriesAsyncTask mGetMainCategoriesAsyncTask;
    private GetSubCategoriesAsyncTask mGetSubCategoriesAsyncTask;
    private GetProductFittingScoreAsyncTask mGetProductFittingScoreAsyncTask;
    private FMCategoryGridAdapter mMainCategoryGridAdapter;
    private FMCategoryGridAdapter mFMSubCategoryGridAdapter;
    private IACustomTextView mCategoryTextView;
    private ArrayList<FMProduct> mProductsList;
    private ProgressDialog progressDialog;
//    private ProductsThreadPoolExecutor mExecutor;
    private GetProductsAsyncTask mGetProductsAsyncTask;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_category);
        mContext = this;
        initializeView();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /**
     * called to initialize the show categories screen ui
     */
    private void initializeView() {
        mToolbar = (Toolbar) findViewById(R.id.toolbar_see_catalog);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mMainCategoryGridView = (GridView) findViewById(R.id.catalog_category_gv);
        mSubCategoryGridView = (GridView) findViewById(R.id.selected_category_gv);
        mCategoryTextView = (IACustomTextView) findViewById(R.id.category_tv);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        mMainCategoryGridView.setVisibility(View.VISIBLE);
        mSubCategoryGridView.setVisibility(View.GONE);

        mCurrentCustomer = new FMCustomer();
        mCurrentCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));
        if (!mCurrentCustomer.getCustomerEmailId().equalsIgnoreCase("")) {
            mToolBarCustomerNameTextView.setText("Hello " + mCurrentCustomer.getCustomerFirstName() + " !");
        }

        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });
        mCategoryTextView.setText(getResources().getString(R.string.select_a_category));

        new IAUtilities().setSharedPreferences(mContext, null, "SelectedShoeSize");

        if (new IAUtilities().isNetworkAvailable(mContext)) {
            mGetMainCategoriesAsyncTask = new GetMainCategoriesAsyncTask();
            mGetMainCategoriesAsyncTask.execute();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "FMShowCategory Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.findmeashoe_appc/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "FMShowCategory Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app URL is correct.
                Uri.parse("android-app://com.findmeashoe_appc/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /**
     * Get Root Categories api called
     */
    private class GetMainCategoriesAsyncTask extends AsyncTask<Void, Integer, FMResponse> {
        private ProgressDialog progressDialog;
        private ArrayList<FMCategory> mAsyncTaskMainCategoryList = new ArrayList<>();

        public GetMainCategoriesAsyncTask() {
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMShowCategoryActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            String mResultUrl = FMUrl.GET_CATEGORIES_URL + "display=[id,name]" +
                    "&filter[active]=[1]" +
                    "&filter[id_parent]=" + FMUrl.ROOT_SUPERUSER_VALUE +
                    "&output_format=JSON" +
                    "&ws_key=" + getResources().getString(R.string.ws_key);
            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse mFMResponse = null;
            mFMResponse = webConnection.getResponse(mResultUrl);
            JSONObject jsonObject = null;
            try {
                if (mFMResponse.isSuccess()) {
                    jsonObject = new JSONObject(mFMResponse.getResponseData());
                    final JSONArray jsonArray = jsonObject.getJSONArray("categories");
                    for (int mCount = 0; mCount < jsonArray.length(); mCount++) {

                        final FMCategory fmCategory = new FMCategory();
                        fmCategory.setCategoryId(jsonArray.getJSONObject(mCount).getInt("id"));
                        fmCategory.setCategoryName(jsonArray.getJSONObject(mCount).getString("name"));


                        mResultUrl = FMUrl.GET_CATEGORIES_URL + "display=[id,name]" +
                                "&filter[active]=[1]" +
                                "&filter[id_parent]=" + fmCategory.getCategoryId() +
                                "&output_format=JSON" +
                                "&ws_key=" + getResources().getString(R.string.ws_key);
                        IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

                        FMResponse genderResponse = webConnection.getResponse(mResultUrl);
                        if (genderResponse.isSuccess()) {
                            JSONObject genderJsonObject = new JSONObject(genderResponse.getResponseData());
                            JSONArray genderJsonArray = genderJsonObject.getJSONArray("categories");
                            FMCategory genderCategory = new FMCategory();
                            genderCategory.setCategoryId(genderJsonArray.getJSONObject(0).getInt("id"));
                            genderCategory.setCategoryName(genderJsonArray.getJSONObject(0).getString("name"));
                            mResultUrl = FMUrl.GET_PRODUCTS_URL + "display=[id,name,price]" +
                                    "&filter[active]=[1]" +
                                    "&filter[id_category_default]=" + genderCategory.getCategoryId() +
                                    "&output_format=JSON" +
                                    "&ws_key=" + getResources().getString(R.string.ws_key);
                            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

                            FMResponse selectedCategoryResponse = webConnection.getResponse(mResultUrl);
                            if (selectedCategoryResponse.isSuccess()) {
                                JSONObject categoryJsonObject = new JSONObject(selectedCategoryResponse.getResponseData());
                                JSONArray categoryJsonArray = categoryJsonObject.getJSONArray("products");

                                mResultUrl = "http://findmeashoe.in/api/images/products/" + categoryJsonArray.getJSONObject(0).getInt("id")
                                        + "?&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H";
                                IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);
                                fmCategory.setCategoryImageUrlLink(mResultUrl);
                            } else {
                                //handle error conditions
                            }
                            mAsyncTaskMainCategoryList.add(fmCategory);
                        } else
                         {
                            try{
                                final FMCategory fmNewCategory = new FMCategory();
                                fmNewCategory.setCategoryId(jsonArray.getJSONObject(mCount).getInt("id"));
                                fmNewCategory.setCategoryName(jsonArray.getJSONObject(mCount).getString("name"));


                                mResultUrl = FMUrl.GET_CATEGORIES_URL + "display=[id,name]" +
                                        "&filter[active]=[1]" +
                                        "&filter[id]=" + fmNewCategory.getCategoryId() +
                                        "&output_format=JSON" +
                                        "&ws_key=" + getResources().getString(R.string.ws_key);
                                IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

                                FMResponse newGenderResponse = webConnection.getResponse(mResultUrl);
                                if (newGenderResponse.isSuccess()) {
                                    JSONObject genderJsonObject = new JSONObject(newGenderResponse.getResponseData());
                                    JSONArray genderJsonArray = genderJsonObject.getJSONArray("categories");
                                    FMCategory genderCategory = new FMCategory();
                                    genderCategory.setCategoryId(genderJsonArray.getJSONObject(0).getInt("id"));
                                    genderCategory.setCategoryName(genderJsonArray.getJSONObject(0).getString("name"));
                                    mResultUrl = FMUrl.GET_PRODUCTS_URL + "display=[id,name,price]" +
                                            "&filter[active]=[1]" +
                                            "&filter[id_category_default]=" + genderCategory.getCategoryId() +
                                            "&output_format=JSON" +
                                            "&ws_key=" + getResources().getString(R.string.ws_key);
                                    IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

                                    FMResponse selectedCategoryResponse = webConnection.getResponse(mResultUrl);
                                    if (selectedCategoryResponse.isSuccess()) {
                                        JSONObject categoryJsonObject = new JSONObject(selectedCategoryResponse.getResponseData());
                                        JSONArray categoryJsonArray = categoryJsonObject.getJSONArray("products");

                                        mResultUrl = "http://findmeashoe.in/api/images/products/" + categoryJsonArray.getJSONObject(0).getInt("id")
                                                + "?&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H";
                                        IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);
                                        fmCategory.setCategoryImageUrlLink(mResultUrl);
                                    } else {
                                        //handle error conditions
                                    }
                                    mAsyncTaskMainCategoryList.add(fmCategory);
                                }
                            } catch (JSONException e){

                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            IAUtilities.showingLogs(TAG, "OnGetRootCategoriesResponse == " + mFMResponse, IAUtilities.LOGS_SHOWN);

            return mFMResponse;
        }


        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "GetMainCategoriesAsyncTask completed", IAUtilities.LOGS_SHOWN);
                //go to next page
                try {
                    mMainCategoryList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response.getResponseData());
                    JSONArray jsonArray = jsonObject.getJSONArray("categories");
                    for (int mCount = 0; mCount < jsonArray.length(); mCount++) {
                        FMCategory fmCategory = new FMCategory();
                        fmCategory.setCategoryId(jsonArray.getJSONObject(mCount).getInt("id"));
                        fmCategory.setCategoryName(jsonArray.getJSONObject(mCount).getString("name"));
                        if (mAsyncTaskMainCategoryList.size() > mCount) {
                            fmCategory.setCategoryImageUrlLink(mAsyncTaskMainCategoryList.get(mCount).getCategoryImageUrlLink());
                        }
                        mMainCategoryList.add(fmCategory);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    mMainCategoryGridAdapter = new FMCategoryGridAdapter(mMainCategoryList, mContext);
                    mMainCategoryGridView.setAdapter(mMainCategoryGridAdapter);
                    mMainCategoryGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            mMainCategoryGridView.setEnabled(false);
                            if (new IAUtilities().isNetworkAvailable(mContext)){
                                mCategoryTextView.setText(mMainCategoryList.get(position).getCategoryName());
                                mGetSubCategoriesAsyncTask = new GetSubCategoriesAsyncTask(((FMCategory)
                                        mMainCategoryGridView.getItemAtPosition(position)).getCategoryId());
                                mGetSubCategoriesAsyncTask.execute();
                                mMainCategoryGridView.setVisibility(View.GONE);
                                mSubCategoryGridView.setVisibility(View.VISIBLE);
                            }
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mMainCategoryGridView.setEnabled(true);
                                }
                            },3000);
                        }
                    });
                }
            } else {
                //show error message
                //failure
            }
        }
    }

    /**
     * Get Selected Gender Categories api called
     */
    private class GetSubCategoriesAsyncTask extends AsyncTask<Void, Integer, FMResponse> {
        private ProgressDialog progressDialog;
        private int mSelectedCategoryId;
        private ArrayList<FMCategory> mAsyncTaskCategoryList = new ArrayList<>();

        public GetSubCategoriesAsyncTask(int mSelectedCategoryId) {
            this.mSelectedCategoryId = mSelectedCategoryId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMShowCategoryActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            String mResultUrl;

            mResultUrl = FMUrl.GET_CATEGORIES_URL + "display=[id,name]" +
                    "&filter[active]=[1]" +
                    "&filter[id_parent]=" + mSelectedCategoryId +
                    "&output_format=JSON" +
                    "&ws_key=" + getResources().getString(R.string.ws_key);
            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

            final FMWebConnection webConnection = new FMWebConnection();
            FMResponse mFMResponse = null;
            mFMResponse = webConnection.getResponse(mResultUrl);
            try {
                if (mFMResponse.isSuccess()) {
                    JSONObject genderJsonObject = new JSONObject(mFMResponse.getResponseData());
                    final JSONArray genderJsonArray = genderJsonObject.getJSONArray("categories");
                    for (int mCount = 0; mCount < genderJsonArray.length(); mCount++) {
//                        mExecutor = new ProductsThreadPoolExecutor(MAX_CPU_CORES, MAX_THREADS, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(MAX_THREADS, true));
                        FMCategory genderCategory = new FMCategory();
                        genderCategory.setCategoryId(genderJsonArray.getJSONObject(mCount).getInt("id"));
                        genderCategory.setCategoryName(genderJsonArray.getJSONObject(mCount).getString("name"));


                        mResultUrl = FMUrl.GET_PRODUCTS_URL + "display=[id,name,price]" +
                                "&filter[active]=[1]" +
                                "&filter[id_category_default]=" + genderCategory.getCategoryId() +
                                "&output_format=JSON" +
                                "&ws_key=" + getResources().getString(R.string.ws_key);
                        IAUtilities.showingLogs(TAG, "count = " + mCount + " mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

                        FMResponse selectedCategoryResponse = webConnection.getResponse(mResultUrl);
                        if (selectedCategoryResponse.isSuccess()) {
                            JSONObject categoryJsonObject = new JSONObject(selectedCategoryResponse.getResponseData());
                            JSONArray categoryJsonArray = categoryJsonObject.getJSONArray("products");
                            mResultUrl = "http://findmeashoe.in/api/images/products/" + categoryJsonArray.getJSONObject(0).getInt("id")
                                    + "?&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H";
                            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);
                            genderCategory.setCategoryImageUrlLink(mResultUrl);
                        } else {
                            //handle error conditions
                            genderCategory.setCategoryImageUrlLink("");
                        }
                        mAsyncTaskCategoryList.add(genderCategory);
                    }
                } else {
                    mResultUrl = FMUrl.GET_CATEGORIES_URL + "display=[id,name]" +
                            "&filter[active]=[1]" +
                            "&filter[id]=" + mSelectedCategoryId +
                            "&output_format=JSON" +
                            "&ws_key=" + getResources().getString(R.string.ws_key);
                    IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

                    final FMWebConnection newWebConnection = new FMWebConnection();
                    FMResponse mNewFMResponse = null;
                    mNewFMResponse = webConnection.getResponse(mResultUrl);

                    if (mNewFMResponse.isSuccess()) {
                        JSONObject genderJsonObject = new JSONObject(mNewFMResponse.getResponseData());
                        final JSONArray genderJsonArray = genderJsonObject.getJSONArray("categories");
                        for (int mCount = 0; mCount < genderJsonArray.length(); mCount++) {
//                        mExecutor = new ProductsThreadPoolExecutor(MAX_CPU_CORES, MAX_THREADS, 100, TimeUnit.SECONDS, new ArrayBlockingQueue<Runnable>(MAX_THREADS, true));
                            FMCategory genderCategory = new FMCategory();
                            genderCategory.setCategoryId(genderJsonArray.getJSONObject(mCount).getInt("id"));
                            genderCategory.setCategoryName(genderJsonArray.getJSONObject(mCount).getString("name"));


                            mResultUrl = FMUrl.GET_PRODUCTS_URL + "display=[id,name,price]" +
                                    "&filter[active]=[1]" +
                                    "&filter[id_category_default]=" + genderCategory.getCategoryId() +
                                    "&output_format=JSON" +
                                    "&ws_key=" + getResources().getString(R.string.ws_key);
                            IAUtilities.showingLogs(TAG, "count = " + mCount + " mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

                            FMResponse selectedCategoryResponse = webConnection.getResponse(mResultUrl);
                            if (selectedCategoryResponse.isSuccess()) {
                                JSONObject categoryJsonObject = new JSONObject(selectedCategoryResponse.getResponseData());
                                JSONArray categoryJsonArray = categoryJsonObject.getJSONArray("products");
                                mResultUrl = "http://findmeashoe.in/api/images/products/" + categoryJsonArray.getJSONObject(0).getInt("id")
                                        + "?&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H";
                                IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);
                                genderCategory.setCategoryImageUrlLink(mResultUrl);
                            } else {
                                //handle error conditions
                                genderCategory.setCategoryImageUrlLink("");
                            }
                            mAsyncTaskCategoryList.add(genderCategory);

                            return mNewFMResponse;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            IAUtilities.showingLogs(TAG, "OnGetRootCategoriesResponse == " + mFMResponse, IAUtilities.LOGS_SHOWN);
            return mFMResponse;
        }


        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            progressDialog.dismiss();
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "GetSubCategoriesAsyncTask completed", IAUtilities.LOGS_SHOWN);
                //go to next page
                try {
                    mSubCategoryList = new ArrayList<>();
                    JSONObject jsonObject = new JSONObject(response.getResponseData());
                    JSONArray jsonArray = jsonObject.getJSONArray("categories");
                    for (int mCount = 0; mCount < jsonArray.length(); mCount++) {
                        FMCategory fmCategory = new FMCategory();
                        fmCategory.setCategoryId(jsonArray.getJSONObject(mCount).getInt("id"));
                        fmCategory.setCategoryName(jsonArray.getJSONObject(mCount).getString("name"));
                        if (mAsyncTaskCategoryList != null && mAsyncTaskCategoryList.size() > mCount) {
                            fmCategory.setCategoryImageUrlLink(mAsyncTaskCategoryList.get(mCount).getCategoryImageUrlLink());
                        } else {
                            fmCategory.setCategoryImageUrlLink("");
                        }
                        mSubCategoryList.add(fmCategory);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } finally {
                    mFMSubCategoryGridAdapter = new FMCategoryGridAdapter(mSubCategoryList, mContext);
                    mSubCategoryGridView.setAdapter(mFMSubCategoryGridAdapter);
                    mSubCategoryGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                            mSubCategoryGridView.setEnabled(false);
                            if (new IAUtilities().isNetworkAvailable(mContext)) {
                                mGetProductsAsyncTask = new GetProductsAsyncTask(((FMCategory) mSubCategoryGridView.getItemAtPosition(position)).getCategoryId());
                                mGetProductsAsyncTask.execute();
                            }
                            Handler handler = new Handler();
                            handler.postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mSubCategoryGridView.setEnabled(true);
                                }
                            },3000);
                        }
                    });
                    mMainCategoryGridView.setEnabled(true);
                }
            } else {
                //show error message
                //failure
            }
        }
    }

    /**
     * called to redirect the user to the products list screen
     * @param mProductsList
     * @param progressDialog
     */
    private void goToSeeProductsActivity(ArrayList<FMProduct> mProductsList,ProgressDialog progressDialog) {
        progressDialog.dismiss();
        Intent intent = new Intent(FMShowCategoryActivity.this, FMShowProductsActivity.class);
        intent.putExtra("Selected Category", mProductsList);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        if (mSubCategoryGridView.getVisibility() == View.VISIBLE) {
            mCategoryTextView.setText(getResources().getString(R.string.select_a_category));
            mMainCategoryGridView.setVisibility(View.VISIBLE);
            mSubCategoryGridView.setVisibility(View.GONE);
            if (mSubCategoryList !=null) {
                mSubCategoryList.clear();
            }
            if (mFMSubCategoryGridAdapter != null)
            mFMSubCategoryGridAdapter.notifyDataSetChanged();
        } else {
            super.onBackPressed();
        }
    }

    /**
     * unused for now. Called to execute Threads efficiently
     */
    private class ProductsThreadPoolExecutor extends ThreadPoolExecutor {
        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize,
                                          long keepAliveTime, TimeUnit unit,
                                          BlockingQueue<Runnable> workQueue) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue);
        }

        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory);
        }

        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, RejectedExecutionHandler handler) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, handler);
        }

        public ProductsThreadPoolExecutor(int corePoolSize, int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
            super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        }

        @Override
        protected void beforeExecute(Thread t, Runnable r) {
            super.beforeExecute(t, r);

        }

        @Override
        protected void afterExecute(Runnable r, Throwable t) {
            super.afterExecute(r, t);
        }
    }

    /**
     * Get Products api called after selecting a category to show products
     */
    private class GetProductsAsyncTask extends AsyncTask<Void, Integer, FMResponse> {
        private int mSelectedCategoryId;

        public GetProductsAsyncTask(int mSelectedCategoryId) {
            this.mSelectedCategoryId = mSelectedCategoryId;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(FMShowCategoryActivity.this);
            progressDialog.setMessage("Loading");
            progressDialog.setCancelable(false);
            progressDialog.setIndeterminate(true);
            progressDialog.show();
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            String mResultUrl;

            mResultUrl = FMUrl.GET_CATALOG_PRODUCTS_URL +
                    "category_filter[id]=" + "[" + mSelectedCategoryId + "]" +
                    "&show_only_product_collection=1" +
                    "&category_filter[active]=[1]" +
                    "&product_display=[id,name,price,buy_link,description_short]" +
                    "&product_filter[active]=[1]" +
                    "&ws_key=" + getResources().getString(R.string.ws_key);
            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse mFMResponse = null;
            mFMResponse = webConnection.getResponse(mResultUrl);

            IAUtilities.showingLogs(TAG, "OnGetRootCategoriesResponse == " + mFMResponse, IAUtilities.LOGS_SHOWN);
            return mFMResponse;
        }


        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "GetSelectedCategoryAsyncTask completed", IAUtilities.LOGS_SHOWN);
                //go to next page
                ArrayList<FMProduct> mRawProductsList = new ArrayList<>();
                JSONArray mProductsJsonArray;
                try {
                    mProductsJsonArray = new JSONArray(response.getResponseData());
                    for (int mCount = 0; mCount < mProductsJsonArray.length(); mCount++) {

                        FMProduct fmProduct = new FMProduct();
                        fmProduct.setProductId(mProductsJsonArray.getJSONObject(mCount).getInt("id"));
                        fmProduct.setProductName(mProductsJsonArray.getJSONObject(mCount).getString("name"));
                        fmProduct.setProductPrice(mProductsJsonArray.getJSONObject(mCount).getString("price"));
                        fmProduct.setProductDescription(mProductsJsonArray.getJSONObject(mCount).getString("description_short"));
                        fmProduct.setProductBuyLink(mProductsJsonArray.getJSONObject(mCount).getString("buy_link"));
                        String mResultUrl = "http://findmeashoe.in/api/images/products/" + fmProduct.getProductId()
                                + "?&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H";
                        IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);
                        fmProduct.setProductImageUrlLink(mResultUrl);
                        mRawProductsList.add(fmProduct);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                mGetProductFittingScoreAsyncTask = new GetProductFittingScoreAsyncTask(mRawProductsList,progressDialog);
                mGetProductFittingScoreAsyncTask.execute();
                mSubCategoryGridView.setEnabled(true);
            } else {
                //show error message
                progressDialog.dismiss();
                if (response.getErrorMessage().equalsIgnoreCase("Response is empty")) {
                    Toast.makeText(mContext, "Sorry! No products are available right now.Please try again later.", Toast.LENGTH_SHORT).show();
                }
                //failure
            }
        }
    }

    /**
     * Get Product Fitting Score api called
     */
    private class GetProductFittingScoreAsyncTask extends AsyncTask<Void, Void, FMResponse> {
        private ProgressDialog mProgressDialog;
        private ArrayList<FMProduct> mUnsortedProductsList;

        public GetProductFittingScoreAsyncTask(ArrayList<FMProduct> mUnsortedProductsList,ProgressDialog mProgressDialog) {
            this.mProgressDialog = mProgressDialog;
            this.mUnsortedProductsList = mUnsortedProductsList;
        }

        @Override
        protected FMResponse doInBackground(Void... params) {
            String mResultUrl = null;
            String mEmailID = null;

            mEmailID = mCurrentCustomer.getCustomerEmailId();
            IAUtilities.showingLogs(TAG, "email = " + mEmailID, IAUtilities.LOGS_SHOWN);

            StringBuilder mProductIdsList = new StringBuilder();
                for (int mProductsCount = 0; mProductsCount < mUnsortedProductsList.size(); mProductsCount++) {
                    mProductIdsList.append(mUnsortedProductsList.get(mProductsCount).getProductId());
                    if (mProductsCount < (mUnsortedProductsList.size()-1)) {
                        mProductIdsList.append(",");
                    }
                }

            mResultUrl = FMUrl.GET_PRODUCT_FITTING_SCORE_URL
                    + FMUrl.ORG_ID + File.separator
                    + mEmailID + File.separator
                    + mProductIdsList.toString() + File.separator
                    + FMUrl.DESC_URL;

            IAUtilities.showingLogs(TAG, "mResultUrl = " + mResultUrl, IAUtilities.LOGS_SHOWN);

            FMWebConnection webConnection = new FMWebConnection();
            FMResponse mFMResponse;
            mFMResponse = webConnection.getResponse(mResultUrl);

            return mFMResponse;
        }

        @Override
        protected void onPostExecute(FMResponse response) {
            super.onPostExecute(response);
            mProgressDialog.dismiss();
            if (response.isSuccess()) {
                IAUtilities.showingLogs(TAG, "GetProductFittingScoreAsyncTask completed", IAUtilities.LOGS_SHOWN);
                JSONObject mFittingScoreJsonObject;
                try{
                    mFittingScoreJsonObject = new JSONObject(response.getResponseData());
                    JSONObject mDataJsonObject = mFittingScoreJsonObject.getJSONObject("data");
                    JSONArray mProductIdsJsonArray = mDataJsonObject.getJSONArray("productIds");
                    IAUtilities.showingLogs(TAG,"product ids = " + mProductIdsJsonArray.toString(),IAUtilities.LOGS_SHOWN);


                    mProductsList = new ArrayList<>();

                    for (int i = 0;i < mProductIdsJsonArray.length();i++){
                        for (int j = 0;j < mUnsortedProductsList.size();j++){
                            if (mProductIdsJsonArray.getInt(i) == mUnsortedProductsList.get(j).getProductId()){
                                mProductsList.add(mUnsortedProductsList.get(j));
                            }
                        }
                    }
                    JSONArray mProductFitQualityScoreJsonArray = mDataJsonObject.getJSONArray("fitQualityScore");
                    IAUtilities.showingLogs(TAG,"fit Quality Scores = " + mProductFitQualityScoreJsonArray.toString(),IAUtilities.LOGS_SHOWN);

                    JSONArray mProductSizeJsonArray = mDataJsonObject.getJSONArray("size");

                    for (int mSizeCount = 0; mSizeCount < mProductSizeJsonArray.length(); mSizeCount++){
                        mProductsList.get(mSizeCount).setProductFittingScore(mProductFitQualityScoreJsonArray.getString(mSizeCount));
                        if (!mProductSizeJsonArray.get(mSizeCount).toString().isEmpty()){
                            mProductsList.get(mSizeCount).setProductSize(Double.toString(mProductSizeJsonArray.getJSONObject(mSizeCount).getDouble("ind")));
                        } else {
                            mProductsList.get(mSizeCount).setProductSize("NF");
                        }
                    }

                } catch (JSONException e){
                    e.printStackTrace();
                }
                goToSeeProductsActivity(mProductsList,progressDialog);
                //go to next page

            } else {
                //show error message
                if (response.getErrorMessage().equalsIgnoreCase("Response is empty")){
                    Toast.makeText(mContext,"Sorry. This product is not available right now. Please try again later.",Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}