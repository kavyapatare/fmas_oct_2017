package com.findmeashoe_appc.activity;

import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomButton;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMDrawerLayoutFunctionality;
import com.findmeashoe_appc.utils.IAUtilities;
import com.google.android.youtube.player.YouTubeBaseActivity;

public class FMShowTutorialVideoActivity extends YouTubeBaseActivity /*implements YouTubePlayer.OnInitializedListener */{
    public static final String TAG = "FMShowTutorialVideo";
    private Context mContext;
    private Toolbar mToolbar;
    private IACustomTextView mToolBarCustomerNameTextView;
    private LinearLayout mToolbarBackButton;
    private LinearLayout mToolbarMenuButton;
    private IACustomTextView mCurrentPhotoIndicatorTextView;
    private IACustomButton mTutorialNextButton;
    private FMDrawerLayoutFunctionality mFMDrawerLayoutFunctionality;
    private AudioManager mYoutubeAudioManager;
    private int mCurrentDeviceVolume;
    private RelativeLayout mVideoPlayerRelativeLayout;
    private boolean mIsPlayedOnce = false,hideVideos=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_youtube_tutorial_video);
        initializeView();
    }

    /**
     * called to initialize the tutorial videos screen ui
     */
    private void initializeView() {
        mContext = this;
        mToolbar = (Toolbar) findViewById(R.id.toolbar_show_tutorial_video);
        mToolBarCustomerNameTextView = (IACustomTextView) mToolbar.findViewById(R.id.toolbar_customer_name_field_tv);
        mToolbarBackButton = (LinearLayout) mToolbar.findViewById(R.id.back_button_ll);
        mToolbarMenuButton = (LinearLayout) mToolbar.findViewById(R.id.menu_button_ll);
        mTutorialNextButton = (IACustomButton) findViewById(R.id.tutorial_next_btn);
        mCurrentPhotoIndicatorTextView = (IACustomTextView) findViewById(R.id.current_photo_indicator_tv);
        mVideoPlayerRelativeLayout = (RelativeLayout) findViewById(R.id.tutorial_video_rl);

        mFMDrawerLayoutFunctionality = new FMDrawerLayoutFunctionality(this);
        mFMDrawerLayoutFunctionality.initializeDrawerLayout();

        FMCustomer fmCustomer = new FMCustomer();
        fmCustomer.loadData(new IAUtilities().getSharedPreferences(mContext, "FMCustomer"));
        if (!fmCustomer.getCustomerEmailId().equalsIgnoreCase("")) {
            mToolBarCustomerNameTextView.setText("Hello " + fmCustomer.getCustomerFirstName() + " !");
        }


        mYoutubeAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        mCurrentDeviceVolume = mYoutubeAudioManager.getStreamVolume(AudioManager.STREAM_MUSIC);

        mYoutubeAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                mYoutubeAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                0);
        mTutorialNextButton.setEnabled(true);
        mTutorialNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FMShowTutorialVideoActivity.this, FMCameraActivity.class);
                startActivityForResult(intent, Integer.parseInt(mContext.getResources().getString(R.string.camera)));
            }
        });
        mToolbarBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mToolbarMenuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFMDrawerLayoutFunctionality.openDrawer();
            }
        });
       // playVideo();


        if(!hideVideos){

        mVideoPlayerRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              mYoutubeAudioManager.setStreamVolume(
                        AudioManager.STREAM_MUSIC,
                        mYoutubeAudioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC),
                        0);
                mIsPlayedOnce = false;
                playVideo();
            }
        });}


    }

    /**
     * called to start playing the corresponding video in full screen
     */
    private void playVideo(){
        if (new IAUtilities().isNetworkAvailable(mContext) && !mIsPlayedOnce){
            if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(1))) {
                mCurrentPhotoIndicatorTextView.setText(mContext.getResources().getString(R.string.first_photo));
                startActivityForResult(new Intent(FMShowTutorialVideoActivity.this,FMFullScreenVideoActivity.class)
                        .putExtra("url", FMUrl.FIRST_VIDEO_URL), FMUrl.START_VIDEO);
            } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(2))) {
                mCurrentPhotoIndicatorTextView.setText(mContext.getResources().getString(R.string.second_photo));
                startActivityForResult(new Intent(FMShowTutorialVideoActivity.this,FMFullScreenVideoActivity.class)
                        .putExtra("url",FMUrl.SECOND_VIDEO_URL), FMUrl.START_VIDEO);
            } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(3))) {
                mCurrentPhotoIndicatorTextView.setText(mContext.getResources().getString(R.string.third_photo));
                startActivityForResult(new Intent(FMShowTutorialVideoActivity.this,FMFullScreenVideoActivity.class)
                        .putExtra("url",FMUrl.THIRD_VIDEO_URL), FMUrl.START_VIDEO);
            } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(4))) {
                mCurrentPhotoIndicatorTextView.setText(mContext.getResources().getString(R.string.first_photo));
                startActivityForResult(new Intent(FMShowTutorialVideoActivity.this,FMFullScreenVideoActivity.class)
                        .putExtra("url",FMUrl.FIRST_VIDEO_URL), FMUrl.START_VIDEO);
            } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(5))) {
                mCurrentPhotoIndicatorTextView.setText(mContext.getResources().getString(R.string.second_photo));
                startActivityForResult(new Intent(FMShowTutorialVideoActivity.this,FMFullScreenVideoActivity.class)
                        .putExtra("url",FMUrl.SECOND_VIDEO_URL), FMUrl.START_VIDEO);
            } else if (new IAUtilities().getSharedPreferences(mContext, "CurrentFileIndexNo").equalsIgnoreCase(String.valueOf(6))) {
                mCurrentPhotoIndicatorTextView.setText(mContext.getResources().getString(R.string.third_photo));
                startActivityForResult(new Intent(FMShowTutorialVideoActivity.this,FMFullScreenVideoActivity.class)
                        .putExtra("url",FMUrl.THIRD_VIDEO_URL), FMUrl.START_VIDEO);
            }
        }
    }
    @Override
    protected void onPause() {
        super.onPause();
        mVideoPlayerRelativeLayout.setEnabled(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mVideoPlayerRelativeLayout.setEnabled(true);
            }
        },1000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onactivity result -- tuts activity ");
        mYoutubeAudioManager.setStreamVolume(
                AudioManager.STREAM_MUSIC,
                mCurrentDeviceVolume,
                0);
        if (requestCode == Integer.parseInt(mContext.getResources().getString(R.string.camera))){
            if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.reinitialize_activity))){
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.reinitialize_activity)));
                finish();
            } else if(resultCode == Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result))){
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.see_catalog_result)),data);
                finish();
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images))){
                Log.d(TAG, "onactivity result -- tuts activity retake");
                setResult(Integer.parseInt(mContext.getResources().getString(R.string.retake_all_images)));
                finish();
            } else if (resultCode == Integer.parseInt(mContext.getResources().getString(R.string.cancelled_result_code))){

            }
        } else if (requestCode == FMUrl.START_VIDEO){
            if (resultCode == FMUrl.VIDEO_COMPLETE){
                mTutorialNextButton.setEnabled(true);
            }
        }
    }
}
