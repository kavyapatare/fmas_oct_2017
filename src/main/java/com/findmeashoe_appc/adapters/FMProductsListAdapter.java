package com.findmeashoe_appc.adapters;

import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.Toast;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMProduct;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by NinaadJoshi on 8/9/15.
 */
public class FMProductsListAdapter extends BaseAdapter {
    public static final String TAG = "GBRestaurantListAdapter";
    private Context context;
    private LayoutInflater layoutInflater;
    private List<FMProduct> objects = new ArrayList<FMProduct>();
    private FMProductsListAdapterInterface mListener;
    private FMCustomer mCurrentCustomer;

    public FMProductsListAdapter(ArrayList<FMProduct> mProducts, Context context, FMCustomer mCurrentCustomer) {
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
        objects = mProducts;
        this.mCurrentCustomer = mCurrentCustomer;
        mListener = (FMProductsListAdapterInterface)context;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Object getItem(int position) {
        return objects.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(final int position, View view, ViewGroup viewGroup) {
        final ViewHolder viewHolder;

        if (view == null) {
            view = layoutInflater.inflate(R.layout.item_list_product, null);
            viewHolder = new ViewHolder();
            viewHolder.ivProductImage = (ImageView) view.findViewById(R.id.product_iv);
            viewHolder.tvProducttName = (IACustomTextView) view.findViewById(R.id.product_name_tv);
            viewHolder.tvProductPrice = (IACustomTextView) view.findViewById(R.id.product_price_tv);
            viewHolder.llTryMeButton = (LinearLayout) view.findViewById(R.id.try_me_btn);
            viewHolder.tvProductSize = (IACustomTextView) view.findViewById(R.id.product_size_tv);
            viewHolder.pbImageLoading = (ProgressBar) view.findViewById(R.id.progressbar_image);
            viewHolder.pbSizeLoading = (ProgressBar) view.findViewById(R.id.progressbar_size);
            viewHolder.rbProductFitScore = (RatingBar) view.findViewById(R.id.fit_score_rating_bar);
            viewHolder.tvProductFitScore = (IACustomTextView) view.findViewById(R.id.fit_score_tv);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }
        viewHolder.tvProducttName.setText(objects.get(position).getProductName());
        Double mTempPrice = Double.parseDouble(objects.get(position).getProductPrice());
        viewHolder.tvProductPrice.setText(String.format("₹ %.2f",mTempPrice));
        viewHolder.llTryMeButton.setEnabled(false);
        viewHolder.rbProductFitScore.setMax(100);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            LayerDrawable layerDrawable = (LayerDrawable) viewHolder.rbProductFitScore.getProgressDrawable();
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(0)), context.getResources().getColor(R.color.light_gray));   // Empty star
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), context.getResources().getColor(android.R.color.transparent)); // Partial star
            DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)), context.getResources().getColor(R.color.golden_yellow));  // Full star
            viewHolder.rbProductFitScore.setProgressDrawable(layerDrawable);
        }

        double mFitScore = 0;
        if (!objects.get(position).getProductFittingScore().isEmpty()) {
            mFitScore = Double.parseDouble(objects.get(position).getProductFittingScore());
            viewHolder.rbProductFitScore.setProgress((int) (mFitScore * 20));
            if (mFitScore <= 0.5) {
                viewHolder.tvProductFitScore.setText(context.getResources().getString(R.string.not_applicable));
            } else if (mFitScore <= 1.0) {
                viewHolder.tvProductFitScore.setText(context.getResources().getString(R.string.not_recommended));
            } else if (mFitScore <= 2.0) {
                viewHolder.tvProductFitScore.setText(context.getResources().getString(R.string.poor_fit));
            } else if (mFitScore <= 3.0) {
                viewHolder.tvProductFitScore.setText(context.getResources().getString(R.string.average_fit));
            } else if (mFitScore <= 4.0) {
                viewHolder.tvProductFitScore.setText(context.getResources().getString(R.string.good_fit));
            } else if (mFitScore <= 5.0) {
                viewHolder.tvProductFitScore.setText(context.getResources().getString(R.string.perfect_fit));
            } else {
                viewHolder.tvProductFitScore.setText(context.getResources().getString(R.string.not_applicable));
            }
        }
        viewHolder.pbImageLoading.setVisibility(View.VISIBLE);
        viewHolder.pbSizeLoading.setVisibility(View.VISIBLE);
        viewHolder.ivProductImage.setVisibility(View.GONE);

        if (!objects.get(position).getProductSize().equalsIgnoreCase("NF")){
            viewHolder.pbSizeLoading.setVisibility(View.GONE);
            viewHolder.tvProductSize.setText(objects.get(position).getProductSize());
            viewHolder.llTryMeButton.setEnabled(true);
            viewHolder.llTryMeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onTryMeButtonClick(objects.get(position));
                }
            });

        }
        else{
            viewHolder.pbSizeLoading.setVisibility(View.GONE);
            viewHolder.tvProductSize.setText("NF");
            viewHolder.llTryMeButton.setEnabled(true);
            viewHolder.llTryMeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context,"Size not available.",Toast.LENGTH_SHORT).show();
                }
            });

        }

        if(!objects.get(position).getProductImageUrl().equalsIgnoreCase("")) {
            Picasso.with(context).load(objects.get(position).getProductImageUrl())
                    .into(viewHolder.ivProductImage, new Callback() {
                        @Override
                        public void onSuccess() {
                            viewHolder.ivProductImage.setVisibility(View.VISIBLE);
                            viewHolder.pbImageLoading.setVisibility(View.GONE);
                        }

                        @Override
                        public void onError() {
                            viewHolder.pbImageLoading.setVisibility(View.GONE);
                        }
                    });
        }
        else{
            viewHolder.pbImageLoading.setVisibility(View.GONE);
        }

        return view;
    }

    private class ViewHolder {
        ImageView ivProductImage;
        IACustomTextView tvProducttName,
                tvProductPrice,
                tvProductSize,
                tvProductFitScore;
        LinearLayout llTryMeButton;
        ProgressBar pbImageLoading,
                pbSizeLoading;
        RatingBar rbProductFitScore;
    }

    public interface FMProductsListAdapterInterface{
        void onTryMeButtonClick(FMProduct fmProduct);
    }
}
