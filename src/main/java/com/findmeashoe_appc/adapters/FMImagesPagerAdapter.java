package com.findmeashoe_appc.adapters;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.widget.ImageView;


/**
 * Created by iaurokitkat on 14/9/15.
 */
public class FMImagesPagerAdapter extends PagerAdapter {
    public String[] tabs = {"Top View", "Angle View", "Side View"};
    private int mNotifUnreadCount;
    private ImageView mTopView;
    private ImageView mAngleView;
    private ImageView mSideView;

    public FMImagesPagerAdapter(ImageView mTopView, ImageView mAngleView, ImageView mSideView) {
        this.mTopView = mTopView;
        this.mAngleView = mAngleView;
        this.mSideView = mSideView;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return false;
    }
}
