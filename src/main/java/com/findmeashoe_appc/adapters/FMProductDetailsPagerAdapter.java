package com.findmeashoe_appc.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.findmeashoe_appc.ds.FMProduct;
import com.findmeashoe_appc.fragments.FMProductDetailsFragment;

import java.util.ArrayList;

/**
 * Created by sayali on 20/7/16.
 */
public class FMProductDetailsPagerAdapter extends FragmentPagerAdapter{
    private ArrayList<FMProduct> objects;
    private FMProductDetailsFragment fmProductDetailsFragment;

    public FMProductDetailsPagerAdapter(FragmentManager fm, ArrayList<FMProduct> mProductsList) {
        super(fm);
        objects = mProductsList;
    }

    @Override
    public int getCount() {
        return objects.size();
    }

    @Override
    public Fragment getItem(int position) {
        fmProductDetailsFragment = FMProductDetailsFragment.newInstance(objects.get(position));
        return fmProductDetailsFragment;
    }
}
