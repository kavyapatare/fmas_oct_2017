package com.findmeashoe_appc.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.findmeashoe_appc.ui.IACustomTextView;

/**
 * Created by iaurokitkat on 25/11/15.
 */
public class FMSpinnerAdapter extends ArrayAdapter<String> {

    public FMSpinnerAdapter(Context context, int textViewResourceId, String[] objects) {
        super(context, textViewResourceId, objects);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = null;

        // If this is the initial dummy entry, make it hidden
        if (position == 0) {
            IACustomTextView tv = new IACustomTextView(getContext());
            tv.setHeight(0);
            tv.setVisibility(View.GONE);
            v = tv;
        }
        else {
            // Pass convertView as null to prevent reuse of special case views

            v = super.getDropDownView(position, null, parent);
        }

        // Hide scroll bar because it appears sometimes unnecessarily, this does not prevent scrolling
        parent.setVerticalScrollBarEnabled(false);
        return v;
    }
}
