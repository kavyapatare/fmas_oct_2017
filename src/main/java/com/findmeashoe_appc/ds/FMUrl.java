package com.findmeashoe_appc.ds;

/**
 * Created by sayali on 24/9/15.
 */
public class FMUrl {
    public static String CURRENT_IP_ADDRESS;

    public static final String COMBINATIONS_URL = "combinations/";
    public static final String PRODUCT_OPTION_VALUES_URL = "product_option_values/";
    public static final String SERVICES_URL = "services/";
    public static final String COMPARE_WITH_USER_URL = "compare-with-user/";
    public static final String SERVICE_STANDARDS_URL = "ind/";
    public static final String FOOT_MEASURE_URL = "/FootMeasure";
    public static final String CUSTOMERS_URL = "customers/?";
    public static final String CATEGORIES_URL = "categories/?";
    public static final String PRODUCTS_URL = "products/?";
    public static final String GET_CUSTOMER_MEASUREMENT_URL = "get-customer-measurement/";
    public static final String STORE_MEASUREMENT_URL = "store-measurement/";
    public static final String ADD_USER_FAMILY_URL = "/addusersfamily";
    public static final String FORGOT_PASSWORD_URL = "forgot_password?";
    public static final String GET_FIT_SORTED_PRODUCT_LIST_URL = "get-fit-sorted-product-list/";
    public static final String ASC_URL = "asc";
    public static final String DESC_URL = "desc";
    public static final String CATALOG_PRODUCTS_URL = "catalog_products?";

    public static final String SERVICE_STANDARDS = "ind";
    public static final String ORG_ID = String.valueOf(8);
    public static final String STATUS = String.valueOf(3);
    public static final String TOE_BOX_FS = String.valueOf(1);
    public static final String TOE_ANGLE_FS = String.valueOf(1);
    public static final String ROOT_SUPERUSER_VALUE = String.valueOf(2);
    public static final String DEMO_BASE_IP_ADDRESS = "54.179.147.79";
    public static final String LIVE_BASE_IP_ADDRESS = "54.179.147.104/";
    public static final String DEV_BASE_IP_ADDRESS = "54.169.230.57/";
    public static final String BASE_URL = "http://";
    public static final String PRESTASHOP_BASE_API = BASE_URL + "findmeashoe.in/api/";
    public static final String TARGET_BASE_URL = BASE_URL + "live.findmeashoe.com/";

    public static final String PRIVACY_POLICY_URL = "http://findmeashoe.in/index.php?id_cms=5&controller=cms&id_lang=1";
    public static final String HOME_SCREEN_URL = "http://www.findmeashoe.in";
    public static final String GET_LOCATION_URL = "https://freegeoip.net/json/";

    public static final String NEW_LOGIN_URL = PRESTASHOP_BASE_API + CUSTOMERS_URL;
    public static final String NEW_REGISTER_URL = "http://X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H@findmeashoe.in/api/customers/output_format=JSON&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H";
    public static final String COMMON_VENDOR_EMAIL_ID = "adminappc@findmeashoe.com";

    public static final String GET_CATEGORIES_URL = PRESTASHOP_BASE_API + CATEGORIES_URL;
    public static final String GET_PRODUCTS_URL = PRESTASHOP_BASE_API + PRODUCTS_URL;

    public static final String GET_PRODUCT_FITTING_SCORE_URL = TARGET_BASE_URL + SERVICES_URL + GET_FIT_SORTED_PRODUCT_LIST_URL;
    public static final String GET_CATALOG_PRODUCTS_URL = PRESTASHOP_BASE_API + CATALOG_PRODUCTS_URL;

    public static final String FIRST_VIDEO_URL = "https://s3-ap-southeast-1.amazonaws.com/cdn.assets.findmeashoe/Videos/Top+View.mp4";
    public static final String SECOND_VIDEO_URL = "https://s3-ap-southeast-1.amazonaws.com/cdn.assets.findmeashoe/Videos/Top+Side+View.mp4";
    public static final String THIRD_VIDEO_URL = "https://s3-ap-southeast-1.amazonaws.com/cdn.assets.findmeashoe/Videos/Side+View.mp4";

    public static final int START_VIDEO = 99;
    public static final int VIDEO_COMPLETE = 200;
}
