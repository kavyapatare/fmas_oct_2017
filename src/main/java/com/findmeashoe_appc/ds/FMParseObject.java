package com.findmeashoe_appc.ds;

import com.parse.ParseObject;

/**
 * Created by iaurokitkat on 22/12/15.
 */
public class FMParseObject extends ParseObject{
    private String mCustomerEmailId = "";
    private String mCustomerFeedback = "";
    private int mUsefulnessStar = -1;
    private int mEaseOfUseStar = -1;
    private int mLikelyToRecommendStar = -1;

    public FMParseObject() {
    }

    public FMParseObject(FMParseObject fmParseObject) {
        this.setCustomerEmailId(fmParseObject.getCustomerEmailId());
        this.setCustomerFeedback(fmParseObject.getCustomerFeedback());
        this.setUsefulnessStar(fmParseObject.getUsefulnessStar());
        this.setEaseOfUseStar(fmParseObject.getEaseOfUseStar());
        this.setLikelyToRecommendStar(fmParseObject.getLikelyToRecommendStar());
    }

    public String getCustomerEmailId() {
        return mCustomerEmailId;
    }

    public void setCustomerEmailId(String mCustomerEmailId) {
        this.mCustomerEmailId = mCustomerEmailId;
    }

    public String getCustomerFeedback() {
        return mCustomerFeedback;
    }

    public void setCustomerFeedback(String mCustomerFeedback) {
        this.mCustomerFeedback = mCustomerFeedback;
    }

    public int getUsefulnessStar() {
        return mUsefulnessStar;
    }

    public void setUsefulnessStar(int mUsefulnessStar) {
        this.mUsefulnessStar = mUsefulnessStar;
    }

    public int getEaseOfUseStar() {
        return mEaseOfUseStar;
    }

    public void setEaseOfUseStar(int mEaseOfUseStar) {
        this.mEaseOfUseStar = mEaseOfUseStar;
    }

    public int getLikelyToRecommendStar() {
        return mLikelyToRecommendStar;
    }

    public void setLikelyToRecommendStar(int mLikelyToRecommendStar) {
        this.mLikelyToRecommendStar = mLikelyToRecommendStar;
    }
}
