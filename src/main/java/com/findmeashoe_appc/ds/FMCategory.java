package com.findmeashoe_appc.ds;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by iaurokitkat on 17/12/15.
 */
public class FMCategory {
    private int mCategoryId = -1;
    private String mCategoryName = "";
    private String mCategoryImageUrl = "";
    private String mCategoryImageUrlLink = "";

    public FMCategory()
    {
    }

    public FMCategory(FMCategory fmCategory)
    {
        this.setCategoryId(fmCategory.getCategoryId());
        this.setCategoryName(fmCategory.getCategoryName());
        this.setCategoryImageUrl(fmCategory.getCategoryImageUrl());
        this.setCategoryImageUrlLink(fmCategory.getCategoryImageUrlLink());
    }

    public int getCategoryId()
    {
        return mCategoryId;
    }

    public void setCategoryId(int mCategoryId) {
        this.mCategoryId = mCategoryId;
    }

    public String getCategoryName() {
        return mCategoryName;
    }

    public void setCategoryName(String mCategoryName) {
        this.mCategoryName = mCategoryName;
    }

    public String getCategoryImageUrl() {
        return mCategoryImageUrl;
    }

    public void setCategoryImageUrl(String mCategoryImageUrl) {
        this.mCategoryImageUrl = mCategoryImageUrl;
    }

    public String getCategoryImageUrlLink() {
        return mCategoryImageUrlLink;
    }

    public void setCategoryImageUrlLink(String mCategoryImageUrlLink) {
        this.mCategoryImageUrlLink = mCategoryImageUrlLink;
    }

    public void loadData(String response) {
        try {

            JSONObject jsonObject = new JSONObject(response);
            this.setCategoryId(jsonObject.optInt("id"));
            this.setCategoryName(jsonObject.optString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
