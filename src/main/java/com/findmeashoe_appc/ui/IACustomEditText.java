package com.findmeashoe_appc.ui;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

import com.findmeashoe_appc.R;

/**
 * This custom EditText field contains all the functions of a regular EditText and a custom font typeface.
 * Created by iaurokitkat on 28/7/15.
 */
public class IACustomEditText extends EditText {

    private onTextChangeListener mOnTextChangeListener;

    public IACustomEditText(Context context) {
        super(context);
    }

    public IACustomEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);
        init(mFontName);
        mTypedArray.recycle();
    }

    public IACustomEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);
        init(mFontName);
        mTypedArray.recycle();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public IACustomEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        TypedArray mTypedArray = context.obtainStyledAttributes(attrs, R.styleable.IACustomTextView);
        String mFontName = mTypedArray.getString(R.styleable.IACustomTextView_customFont);
        init(mFontName);
        mTypedArray.recycle();
    }

    /**
     * Setting custom typeface
     *
     * @param mFontName
     */

    public void init(String mFontName) {
        if (mFontName != null) {
            try {
                Typeface mTypeface = null;

                if (mFontName.contains("Bold")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Bold.ttf");
                } else if (mFontName.contains("Semibold")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Semibold.ttf");
                } else if (mFontName.contains("Regular")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Regular.ttf");
                } else if (mFontName.contains("Light")) {
                    mTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/SourceSansPro-Light.ttf");
                }
                setTypeface(mTypeface);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        //    super.onTextChanged(text, start, lengthBefore, lengthAfter);
        if (mOnTextChangeListener != null)
            mOnTextChangeListener.onTextChanged(text.toString());
    }

    public onTextChangeListener getOnTextChangeListener() {
        return mOnTextChangeListener;
    }

    public void setOnTextChangeListener(onTextChangeListener mOnTextChangeListener) {
        this.mOnTextChangeListener = mOnTextChangeListener;
    }

    public interface onTextChangeListener {
        public void onTextChanged(String mText);
    }
}
