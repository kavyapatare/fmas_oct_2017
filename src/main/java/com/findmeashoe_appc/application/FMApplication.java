package com.findmeashoe_appc.application;

import android.app.Application;
import android.content.Context;

//import com.clevertap.android.sdk.ActivityLifecycleCallback;
import com.findmeashoe_appc.R;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;

/**
 * Created by iaurokitkat on 22/12/15.
 */
public class FMApplication extends Application {
    private static final String TAG = "FMApplication";
    private Context mContext;
    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
        Parse.enableLocalDatastore(mContext);
        Parse.initialize(mContext, mContext.getResources().getString(R.string.parse_app_id),
                mContext.getResources().getString(R.string.parse_client_key));
        ParseACL defaultACL = new ParseACL();

        // If you would like all objects to be private by default, remove this line.
        defaultACL.setPublicReadAccess(true);
        defaultACL.setPublicWriteAccess(true);

        ParseACL.setDefaultACL(defaultACL, true);

        ParseInstallation.getCurrentInstallation().saveInBackground();
    }
}
