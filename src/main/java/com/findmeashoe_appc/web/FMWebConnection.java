package com.findmeashoe_appc.web;

import android.content.Context;
import android.net.Uri;

import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.utils.IAUtilities;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Iterator;

/**
 * Created by sayali on 24/9/15.
 */
public class FMWebConnection {

    public interface ProgressUpdateCallback {
        public void onProgressUpdateSet(float mPercentageProgress);
    }

    private static String TAG = "FMASWebConnection";

    private static final int SUCCESS_CODE = 200;
    private static final int OBJECT_CREATED = 201;
    private static final int BAD_REQUEST_ERROR_CODE = 400;
    private static final int INTERNAL_SERVER_ERROR_CODE = 500;
    private static final int NOT_FOUND_ERROR_CODE = 404;
    private static final int SCANNED_WITHIN_24_HRS = 406;
    private static final int UNAUTHORIZED_ERROR = 401;
    private final String UTF8 = "utf8";
    private final String GET_REQUEST = "GET";
    private final String POST_REQUEST = "POST";
    private ProgressUpdateCallback mListener;

    public FMWebConnection(Context mContext) {
        mListener = (ProgressUpdateCallback) mContext;
    }

    public FMWebConnection() {
    }


    public FMResponse getResponse(String urlLink) {

        FMResponse mFMResponse = new FMResponse();

        try {
            URL url = new URL(urlLink);

            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestMethod(GET_REQUEST);
            httpUrlConnection.setConnectTimeout(60000);
            InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());

            if (httpUrlConnection.getResponseCode() == SUCCESS_CODE) {
                String mResponseData = convertInputStreamToString(in);
                if (mResponseData.isEmpty()
                        || mResponseData.equalsIgnoreCase("[]")){
                    mFMResponse.setErrorMessage("Response is empty");
                    mFMResponse.setIsSuccess(false);
                }else{
                    mFMResponse.setResponseData(mResponseData);
                    mFMResponse.setIsSuccess(true);
                }

            } else {
                mFMResponse.setIsSuccess(false);
                IAUtilities.showingLogs(TAG, "FMResponse.setErrorMessage() = " + httpUrlConnection.getResponseMessage(), IAUtilities.LOGS_SHOWN);
                mFMResponse.setErrorMessage(errorCodeText(httpUrlConnection.getResponseCode()));
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(BAD_REQUEST_ERROR_CODE));
            return mFMResponse;
        }catch (ConnectException e){
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage("The connection was lost.");
            return mFMResponse;
        }catch (IOException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(INTERNAL_SERVER_ERROR_CODE));
            return mFMResponse;
        }
        return mFMResponse;
    }

    public FMResponse getResponse(String urlLink, JSONObject jsonObject) {

        FMResponse mFMResponse = new FMResponse();

        try {
            urlLink.concat(getQuery(jsonObject));
            String newUrl = urlLink + getQuery(jsonObject);
            IAUtilities.showingLogs(TAG, "newUrl  == " + newUrl, IAUtilities.LOGS_SHOWN);
            URL url = new URL(newUrl);

            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestMethod(GET_REQUEST);
            httpUrlConnection.setConnectTimeout(60000);

            InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());


            if (httpUrlConnection.getResponseCode() == SUCCESS_CODE) {
                mFMResponse.setResponseData(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(true);
            } else {
                mFMResponse.setIsSuccess(false);
                IAUtilities.showingLogs(TAG, "FMResponse.setErrorMessage() = " + httpUrlConnection.getResponseMessage(), IAUtilities.LOGS_SHOWN);
                mFMResponse.setErrorMessage(errorCodeText(httpUrlConnection.getResponseCode()));
            }


        } catch (MalformedURLException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(BAD_REQUEST_ERROR_CODE));
            return mFMResponse;
        } catch (ConnectException e){
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage("The connection was lost.");
            return mFMResponse;
        }catch (IOException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(INTERNAL_SERVER_ERROR_CODE));
            return mFMResponse;
        }
        return mFMResponse;
    }

    public FMResponse postCustomerData(String urlLink, JSONObject jsonObject) {
        FMResponse mFMResponse = new FMResponse();

        try {
            URL url = new URL(urlLink);

            IAUtilities.showingLogs(TAG, " jsonObject = " + jsonObject.toString(), IAUtilities.LOGS_SHOWN);
            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestMethod(POST_REQUEST);
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            httpUrlConnection.setConnectTimeout(60000);
            IAUtilities.showingLogs(TAG, "Timestamp before request = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            OutputStream os = httpUrlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, UTF8));

            Uri.Builder builder = new Uri.Builder();

            Iterator<String> iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                try {
                    String value = jsonObject.get(key).toString();
                    builder.appendQueryParameter(key, value);
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

            String query = builder.build().getEncodedQuery();
            IAUtilities.showingLogs(TAG, " getQuery = " + query, IAUtilities.LOGS_SHOWN);

            writer.write(query);
            writer.flush();
            writer.close();
            os.close();
            IAUtilities.showingLogs(TAG, "Timestamp after response = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);

            IAUtilities.showingLogs(TAG, "responseCode = " + httpUrlConnection.getResponseCode(), IAUtilities.LOGS_SHOWN);
            IAUtilities.showingLogs(TAG, "responseMessage = " + httpUrlConnection.getResponseMessage(), IAUtilities.LOGS_SHOWN);


            if (httpUrlConnection.getResponseCode() == SUCCESS_CODE) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                mFMResponse.setResponseData(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(true);
                IAUtilities.showingLogs(TAG, "mainResponse = " + mFMResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == UNAUTHORIZED_ERROR) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setResponseData(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(true);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == SCANNED_WITHIN_24_HRS) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setResponseData(errorCodeText(SCANNED_WITHIN_24_HRS));
                mFMResponse.setIsSuccess(true);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == BAD_REQUEST_ERROR_CODE) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                if (httpUrlConnection.getResponseMessage().contains("has already been taken.")) {
                    mFMResponse.setErrorMessage("Username or Email has already been taken.");
                    mFMResponse.setIsSuccess(false);
                    return mFMResponse;
                }
                mFMResponse.setErrorMessage(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(false);
                return mFMResponse;

            } else {
                mFMResponse.setIsSuccess(false);
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setErrorMessage(convertInputStreamToString(in));
                return mFMResponse;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(BAD_REQUEST_ERROR_CODE));
            return mFMResponse;
        } catch (ProtocolException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(INTERNAL_SERVER_ERROR_CODE));
            return mFMResponse;
        } catch (ConnectException e){
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage("The connection was lost.");
            return mFMResponse;
        }catch (IOException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(NOT_FOUND_ERROR_CODE));
            return mFMResponse;
        }
    }




    public FMResponse postCustomerRawData(String urlLink, String mXmlDataString) {
        FMResponse mFMResponse = new FMResponse();

        try {
            URL url = new URL(urlLink);
            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestMethod(POST_REQUEST);
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            httpUrlConnection.setConnectTimeout(60000);
            IAUtilities.showingLogs(TAG, "Timestamp before request = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);
            OutputStream os = httpUrlConnection.getOutputStream();
            os.write(mXmlDataString.getBytes());
            IAUtilities.showingLogs(TAG, " getQuery = " + mXmlDataString, IAUtilities.LOGS_SHOWN);
            os.flush();
            os.close();
            IAUtilities.showingLogs(TAG, "Timestamp after response = " + System.currentTimeMillis(), IAUtilities.LOGS_SHOWN);

            IAUtilities.showingLogs(TAG, "responseCode = " + httpUrlConnection.getResponseCode(), IAUtilities.LOGS_SHOWN);
            IAUtilities.showingLogs(TAG, "responseMessage = " + httpUrlConnection.getResponseMessage(), IAUtilities.LOGS_SHOWN);


            if (httpUrlConnection.getResponseCode() == SUCCESS_CODE) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                mFMResponse.setResponseData(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(true);
                IAUtilities.showingLogs(TAG, "mainResponse = " + mFMResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == OBJECT_CREATED) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                mFMResponse.setResponseData(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(true);
                IAUtilities.showingLogs(TAG, "mainResponse = " + mFMResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == UNAUTHORIZED_ERROR) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setResponseData(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(true);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == SCANNED_WITHIN_24_HRS) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setResponseData(errorCodeText(SCANNED_WITHIN_24_HRS));
                mFMResponse.setIsSuccess(true);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == BAD_REQUEST_ERROR_CODE) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                if (httpUrlConnection.getResponseMessage().contains("has already been taken.")) {
                    mFMResponse.setErrorMessage("Username or Email has already been taken.");
                    mFMResponse.setIsSuccess(false);
                    return mFMResponse;
                }
                mFMResponse.setErrorMessage(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(false);
                return mFMResponse;

            } else {
                mFMResponse.setIsSuccess(false);
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setErrorMessage(convertInputStreamToString(in));
                return mFMResponse;
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(BAD_REQUEST_ERROR_CODE));
            return mFMResponse;
        } catch (ProtocolException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(INTERNAL_SERVER_ERROR_CODE));
            return mFMResponse;
        } catch (ConnectException e){
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage("The connection was lost.");
            return mFMResponse;
        }catch (IOException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(NOT_FOUND_ERROR_CODE));
            return mFMResponse;
        }
    }

    public FMResponse postCustomerWithErrorCodesData(String urlLink, JSONObject jsonObject) {

        FMResponse mFMResponse = new FMResponse();

        try {
            URL url = new URL(urlLink);

            HttpURLConnection httpUrlConnection = (HttpURLConnection) url.openConnection();
            httpUrlConnection.setRequestMethod(POST_REQUEST);
            httpUrlConnection.setDoOutput(true);
            httpUrlConnection.setDoInput(true);
            httpUrlConnection.setConnectTimeout(150000);

            OutputStream os = httpUrlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, UTF8));

            Uri.Builder builder = new Uri.Builder();

            Iterator<String> iterator = jsonObject.keys();
            while (iterator.hasNext()) {
                String key = iterator.next();
                try {
                    String value = jsonObject.get(key).toString();
                    builder.appendQueryParameter(key, value);
                } catch (JSONException e) {
                    // Something went wrong!
                }
            }

            String query = builder.build().getEncodedQuery();
            IAUtilities.showingLogs(TAG, " getQuery = " + query, IAUtilities.LOGS_SHOWN);
            int mQueryLength = query.length();
            float mQueryMultiplier = 100.0f / mQueryLength;
            for (int i=0;i<mQueryLength;i++){
                writer.write(query.charAt(i));
                mListener.onProgressUpdateSet((float)i * mQueryMultiplier);
            }
            writer.flush();
            writer.close();
            os.close();

            IAUtilities.showingLogs(TAG, "responseCode = " + httpUrlConnection.getResponseCode(), IAUtilities.LOGS_SHOWN);
            IAUtilities.showingLogs(TAG, "responseMessage = " + httpUrlConnection.getResponseMessage(), IAUtilities.LOGS_SHOWN);


            if (httpUrlConnection.getResponseCode() == SUCCESS_CODE) {

                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                String mResponseData = convertInputStreamToString(in);
                JSONObject mResponseDataObject;
                try {
                    mResponseDataObject = new JSONObject(mResponseData);
                    if (mResponseDataObject.getString("status").equalsIgnoreCase("ERR_000")) {
                        mFMResponse.setResponseData(mResponseData);
                        mFMResponse.setErrorCode(mResponseDataObject.getString("status"));
                        mFMResponse.setIsSuccess(true);
                    }else {
                        mFMResponse.setErrorMessage(mResponseDataObject.getString("remarks"));
                        mFMResponse.setErrorCode(mResponseDataObject.getString("status"));
                        mFMResponse.setIsSuccess(false);
                    }                }
                catch (JSONException e) {
                    e.printStackTrace();
                    if (mResponseData.startsWith("<!DOCTYPE html>") || mResponseData.contains("<html")){
                        mFMResponse.setErrorMessage("");
                        mFMResponse.setErrorCode("ERR_HTML");
                        mFMResponse.setIsSuccess(false);
                    }else{
                        mFMResponse.setErrorMessage("");
                        mFMResponse.setErrorCode("");
                        mFMResponse.setIsSuccess(false);
                    }
                }
                IAUtilities.showingLogs(TAG, "mainResponse = " + mFMResponse.getResponseData(), IAUtilities.LOGS_SHOWN);
                return mFMResponse;

            } else if (httpUrlConnection.getResponseCode() == UNAUTHORIZED_ERROR) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getInputStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setResponseData(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(true);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == SCANNED_WITHIN_24_HRS) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setResponseData(errorCodeText(SCANNED_WITHIN_24_HRS));
                mFMResponse.setIsSuccess(true);
                return mFMResponse;
            } else if (httpUrlConnection.getResponseCode() == BAD_REQUEST_ERROR_CODE) {
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                if (httpUrlConnection.getResponseMessage().contains("has already been taken.")) {
                    mFMResponse.setErrorMessage("Username or Email has already been taken.");
                    mFMResponse.setIsSuccess(false);
                    return mFMResponse;
                }
                mFMResponse.setErrorMessage(convertInputStreamToString(in));
                mFMResponse.setIsSuccess(false);
                return mFMResponse;

            } else {
                mFMResponse.setIsSuccess(false);
                InputStream in = new BufferedInputStream(httpUrlConnection.getErrorStream());
                IAUtilities.showingLogs(TAG, "String === " + convertInputStreamToString(in), IAUtilities.LOGS_SHOWN);
                mFMResponse.setErrorMessage(convertInputStreamToString(in));
                return mFMResponse;
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(BAD_REQUEST_ERROR_CODE));
            return mFMResponse;
        } catch (ProtocolException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(INTERNAL_SERVER_ERROR_CODE));
            return mFMResponse;
        }catch (ConnectException e){
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage("The connection was lost.");
            return mFMResponse;
        } catch (SocketTimeoutException e){
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage("The session timed out.");
            return mFMResponse;
        } catch (IOException e) {
            e.printStackTrace();
            mFMResponse.setIsSuccess(false);
            mFMResponse.setErrorMessage(errorCodeText(NOT_FOUND_ERROR_CODE));
            return mFMResponse;
        }
    }


    private String getQuery(JSONObject jsonObject) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        Iterator<String> a = jsonObject.keys();
        while (a.hasNext()) {
            String temp = a.next();
            if (first) {
                first = false;
            } else {
                result.append("&");
            }
            try {
                result.append(temp);
                result.append("=");
                result.append(jsonObject.getString(temp));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        IAUtilities.showingLogs(TAG, "getQuery = " + result, IAUtilities.LOGS_SHOWN);
        return result.toString();
    }

    private String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, UTF8));
        String line;
        String result = "";
        while ((line = bufferedReader.readLine()) != null) {
            result += line;
        }
        if (null != inputStream) {
            inputStream.close();
        }

        IAUtilities.showingLogs(TAG, "inputStream result  = " + result, IAUtilities.LOGS_SHOWN);
        return result;
    }


    private String errorCodeText(int errorCode) {

        switch (errorCode) {
            case BAD_REQUEST_ERROR_CODE:
                return "Bad Request";
            case OBJECT_CREATED:
                return "Object is Created";
            case UNAUTHORIZED_ERROR:
                return "Unauthorized Request";
            case INTERNAL_SERVER_ERROR_CODE:
                return "Internal server error";
            case NOT_FOUND_ERROR_CODE:
                return "URL not found";
            case SCANNED_WITHIN_24_HRS:
                return "Scanned within 24 hours";
            default:
                return "Unknown Error";
        }
    }
}
