package com.findmeashoe_appc.web;

import com.findmeashoe_appc.ds.FMCustomer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by iaurokitkat on 28/9/15.
 */
public class FMApiManager {
    public static final String TAG = "GBApiManager";

    public ArrayList<FMCustomer> loadCustomersList(String mCustomersListResponse) {


                        /*{
    "message": "OK",
    "result": [
        {
            "id": 43,
            "restaurant_id": 1,
            "restaurant_name": "Vedant",
            "award_type": 1,
            "reward_text": "Cofeee!!!"
        },
        {
            "id": 44,
            "restaurant_id": 2,
            "restaurant_name": "Abhishek",
            "award_type": 1,
            "reward_text": "tea!!!"
        },
        {
            "id": 45,
            "restaurant_id": 2,
            "restaurant_name": "Abhishek",
            "award_type": 1,
            "reward_text": "Bread and Butter"
        }
    ]
}*/


        ArrayList<FMCustomer> mCustomersList = new ArrayList<>();
        try {
            JSONObject mJsonOffersListResponseObject = new JSONObject(mCustomersListResponse);
            JSONArray mJsonResultObjectsArray = mJsonOffersListResponseObject.getJSONArray("customers");
            if (mJsonResultObjectsArray != null) {
                for (int i = 0; i < mJsonResultObjectsArray.length(); i++) {
                    FMCustomer mFMCustomer = new FMCustomer();
                    mFMCustomer.loadData(mJsonResultObjectsArray.getJSONObject(i).toString());
                    mCustomersList.add(mFMCustomer);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mCustomersList;
    }

    public FMCustomer loadCustomer(String mSingleOfferResponse) {
        FMCustomer mGBOffer = new FMCustomer();
        try {
            JSONObject mJsonSingleOfferResponseObject = new JSONObject(mSingleOfferResponse);
            JSONObject mJsonResultOfferObject = mJsonSingleOfferResponseObject.getJSONObject("customers");
            if (mJsonResultOfferObject.equals("")) {
                return null;
            }
            mGBOffer.loadData(mJsonResultOfferObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return mGBOffer;
    }
}
