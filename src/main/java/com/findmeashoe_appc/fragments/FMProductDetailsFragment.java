package com.findmeashoe_appc.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.findmeashoe_appc.R;
import com.findmeashoe_appc.activity.FMShowFittingDetailsActivity;
import com.findmeashoe_appc.ds.FMProduct;
import com.findmeashoe_appc.ds.FMResponse;
import com.findmeashoe_appc.ui.IACustomTextView;
import com.findmeashoe_appc.utils.FMXmlParser;
import com.findmeashoe_appc.web.FMWebConnection;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by sayali on 20/7/16.
 */
public class FMProductDetailsFragment extends Fragment {
    private ViewGroup rootView;
    private FMProduct fmProduct;
    private IACustomTextView mProductNameTextView;
    private ImageView mProductImageView;
    private IACustomTextView mProductDescriptionTextView;
    private IACustomTextView mProductPriceTextView;
    private IACustomTextView mProductSizeTextView;
    private RatingBar mProductDetailsFitScoreRatingBar;
    private IACustomTextView mProductDetailsFitScoreTextView;
    private ProgressBar mProductDetailsShowImageProgressBar;
    private RelativeLayout mProductDetailsTryMeButton;
    private Context mContext;

    public FMProductDetailsFragment(){

    }

    /**
     * called to populate a new instance of the fragment with relevant data
     * @param fmProduct
     * @return
     */
    public static FMProductDetailsFragment newInstance(FMProduct fmProduct){
        FMProductDetailsFragment fmProductDetailsFragment = new FMProductDetailsFragment();
        Bundle args = new Bundle();
        args.putSerializable("productDetail",fmProduct);
        fmProductDetailsFragment.setArguments(args);
        return fmProductDetailsFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        Bundle extra = getArguments();
        fmProduct = (FMProduct) extra.getSerializable("productDetail");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(
                R.layout.fragment_product_details, container, false);
        initializeView();
        return rootView;
    }

    /**
     * called to initialize the fragment view ui
     */
    private void initializeView(){
        mContext = getActivity();
        mProductNameTextView = (IACustomTextView) rootView.findViewById(R.id.product_details_product_name_tv);
        mProductImageView = (ImageView) rootView.findViewById(R.id.product_details_iv);
        mProductDescriptionTextView = (IACustomTextView) rootView.findViewById(R.id.product_details_product_description_tv);
        mProductPriceTextView = (IACustomTextView) rootView.findViewById(R.id.product_details_product_price_tv);
        mProductSizeTextView = (IACustomTextView) rootView.findViewById(R.id.product_details_product_size_tv);
        mProductDetailsFitScoreRatingBar = (RatingBar) rootView.findViewById(R.id.product_details_fit_score_rating_bar);
        mProductDetailsFitScoreTextView = (IACustomTextView) rootView.findViewById(R.id.product_details_fit_score_tv);
        mProductDetailsShowImageProgressBar = (ProgressBar) rootView.findViewById(R.id.product_details_progressbar_image);
        mProductDetailsTryMeButton = (RelativeLayout) rootView.findViewById(R.id.product_details_try_me_btn);
    }

    @Override
    public void onResume() {
        super.onResume();
        mProductDetailsShowImageProgressBar.setVisibility(View.VISIBLE);
        mProductNameTextView.setText(fmProduct.getProductName());
        if (!fmProduct.getProductDescription().isEmpty()) {
            mProductDescriptionTextView.setText(fmProduct.getProductDescription());
        } else {
            mProductDescriptionTextView.setText(mContext.getResources().getString(R.string.no_products_available));
        }
        Double mTempPrice = Double.parseDouble(fmProduct.getProductPrice());
        mProductPriceTextView.setText(String.format("₹ %.2f",mTempPrice));
        mProductSizeTextView.setText(fmProduct.getProductSize());
        mProductDetailsFitScoreRatingBar.setMax(100);
        double mFitScore = 0;
        if (!fmProduct.getProductFittingScore().isEmpty()) {
            mFitScore = Double.parseDouble(fmProduct.getProductFittingScore());
            mProductDetailsFitScoreRatingBar.setProgress((int) (mFitScore * 20));
            if (mFitScore <= 0.5) {
                mProductDetailsFitScoreTextView.setText(mContext.getResources().getString(R.string.not_applicable));
            } else if (mFitScore <= 1.0) {
                mProductDetailsFitScoreTextView.setText(mContext.getResources().getString(R.string.not_recommended));
            } else if (mFitScore <= 2.0) {
                mProductDetailsFitScoreTextView.setText(mContext.getResources().getString(R.string.poor_fit));
            } else if (mFitScore <= 3.0) {
                mProductDetailsFitScoreTextView.setText(mContext.getResources().getString(R.string.average_fit));
            } else if (mFitScore <= 4.0) {
                mProductDetailsFitScoreTextView.setText(mContext.getResources().getString(R.string.good_fit));
            } else if (mFitScore <= 5.0) {
                mProductDetailsFitScoreTextView.setText(mContext.getResources().getString(R.string.perfect_fit));
            } else {
                mProductDetailsFitScoreTextView.setText(mContext.getResources().getString(R.string.not_applicable));
            }

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                LayerDrawable layerDrawable = (LayerDrawable) mProductDetailsFitScoreRatingBar.getProgressDrawable();
                DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(0)), mContext.getResources().getColor(R.color.light_gray));   // Empty star
                DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(1)), mContext.getResources().getColor(android.R.color.transparent)); // Partial star
                DrawableCompat.setTint(DrawableCompat.wrap(layerDrawable.getDrawable(2)), mContext.getResources().getColor(R.color.golden_yellow));  // Full star
                mProductDetailsFitScoreRatingBar.setProgressDrawable(layerDrawable);
            }

        }
        if (fmProduct.getProductSize().equalsIgnoreCase("NF")) {
            mProductDetailsTryMeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(mContext,"Size not available.",Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            mProductDetailsTryMeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    goToShowFittingDetailsActivity(fmProduct);
                }
            });
        }

        if (!fmProduct.getProductImageUrl().isEmpty()){
            Picasso.with(getActivity()).load(fmProduct.getProductImageUrl()).into(mProductImageView, new Callback() {
                @Override
                public void onSuccess() {
                    mProductDetailsShowImageProgressBar.setVisibility(View.GONE);
                }

                @Override
                public void onError() {

                }
            });
        } else {
            new GetImageAsyncTask(fmProduct).execute();
        }
    }

    /**
     * called to redirect the user to the fitting details screen
     * @param fmProduct
     */
    private void goToShowFittingDetailsActivity(FMProduct fmProduct){
        Intent intent = new Intent(mContext,FMShowFittingDetailsActivity.class);
        intent.putExtra("Current Product",fmProduct);
        startActivity(intent);
    }


    /**
     * used to fetch the image urls for the corresponding products
     */
    private class GetImageAsyncTask extends AsyncTask<Void,Void,FMResponse> {
        private FMProduct fmProduct;

        public GetImageAsyncTask(FMProduct fmProduct){
            this.fmProduct = fmProduct;
        }

        @Override
        protected FMResponse doInBackground(Void... voids) {
            FMWebConnection webConnection = new FMWebConnection();
            String mResultUrl = fmProduct.getProductImageUrlLink();
            FMResponse mImageResponse = webConnection.getResponse(mResultUrl);
            if (mImageResponse.isSuccess()) {
                FMXmlParser fmXmlParser = new FMXmlParser();
                String mImageUrl = fmXmlParser.parse(mImageResponse.getResponseData());
                fmProduct.setProductImageUrl(mImageUrl);
            }
            return mImageResponse;
        }

        @Override
        protected void onPostExecute(FMResponse mImageResponse) {
            super.onPostExecute(mImageResponse);
            if (!fmProduct.getProductImageUrl().equalsIgnoreCase("")) {
                Picasso.with(getActivity()).load(fmProduct.getProductImageUrl()).into(mProductImageView, new Callback() {
                    @Override
                    public void onSuccess() {
                        mProductDetailsShowImageProgressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
            }
        }
    }
}
