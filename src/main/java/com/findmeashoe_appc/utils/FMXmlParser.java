package com.findmeashoe_appc.utils;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.StringReader;

/**
 * Created by iaurokitkat on 8/7/15.
 */
public class FMXmlParser {
    private String mCurrentImageUrlString;
    private String mText;

    public String getmCurrentImageUrlString() {
        return mCurrentImageUrlString;
    }

    public void setmCurrentImageUrlString(String mCurrentImageUrlString) {
        this.mCurrentImageUrlString = mCurrentImageUrlString;
    }

    public String parse(String data) {
        XmlPullParserFactory mFactory = null;
        XmlPullParser mParser = null;

        try {
            mFactory = XmlPullParserFactory.newInstance();
            mFactory.setNamespaceAware(true);
            mParser = mFactory.newPullParser();

            mParser.setInput(new StringReader(data));

            int eventType = mParser.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                String mTagName = mParser.getName();


                IAUtilities.showingLogs("Main", "mTagName  " + mTagName, IAUtilities.LOGS_SHOWN);

                switch (eventType) {
                    case XmlPullParser.START_TAG:
                        if (mTagName.equalsIgnoreCase("image")) {
                            IAUtilities.showingLogs("Main", "item start tag ", IAUtilities.LOGS_SHOWN);
                            mCurrentImageUrlString = "";

                        } else if (mTagName.equalsIgnoreCase("declination")) {


                            String url = mParser.getAttributeValue(0);
                            IAUtilities.showingLogs("Main", "url  " + url, IAUtilities.LOGS_SHOWN);

                            String mImageType = mParser.getAttributeValue(1);
                            IAUtilities.showingLogs("Main", "mImageType  " + mImageType, IAUtilities.LOGS_SHOWN);

                            return mImageType + "/?&ws_key=X99V0C9RPGY5Q1ETCKGCDQI241T4FP1H";
                        }
                        break;

                    case XmlPullParser.TEXT:
                        mText = mParser.getText();
                        IAUtilities.showingLogs("Main", "mtext " + mText, IAUtilities.LOGS_SHOWN);
                        break;

                    case XmlPullParser.END_TAG:
                        if (mTagName.equalsIgnoreCase("item")) {

                            mCurrentImageUrlString = null;
                        } else if (mTagName.equalsIgnoreCase("title")) {
                            if (mCurrentImageUrlString != null) {
                                IAUtilities.showingLogs("Main", "flag true title" + mText, IAUtilities.LOGS_SHOWN);
                                mCurrentImageUrlString = mText;
                            }
                        } else if (mTagName.equalsIgnoreCase("description")) {
                            if (mCurrentImageUrlString != null) {
                                IAUtilities.showingLogs("Main", "flag true description" + mText, IAUtilities.LOGS_SHOWN);
                                mCurrentImageUrlString = mText;
                                IAUtilities.showingLogs("Main", "flag true description  " + mText, IAUtilities.LOGS_SHOWN);
                            }
                        }
                        break;
                    default:
                        break;
                }
                eventType = mParser.next();
            }

        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mCurrentImageUrlString;
    }
}
