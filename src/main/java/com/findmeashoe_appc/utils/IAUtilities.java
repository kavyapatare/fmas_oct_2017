package com.findmeashoe_appc.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.findmeashoe_appc.R;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This package includes the most commonly used utilities as functions.
 * Created by iaurokitkat on 28/7/15.
 */
public class IAUtilities {

    public final static long ONE_SECOND = 1000;
    public final static long ONE_MINUTE = ONE_SECOND * 60;
    public final static long ONE_HOUR = ONE_MINUTE * 60;
    public final static long ONE_DAY = ONE_HOUR * 24;
    public final static long MONTHS = ONE_DAY * 30;
    private static final Integer TOAST_TIME = Toast.LENGTH_LONG;
    private static final String TAG = " TAG = iauroandroidlibs";
    private SharedPreferences mPrefs;
    public static final boolean LOGS_SHOWN = false;

    /**
     * @param mDuration The time difference in milliseconds
     * @return A String which shows the time difference in months(30 day month), days, hours, minutes, and seconds
     * This function returns a string which shows the time difference in months(30 day month), days, hours, minutes, and seconds.
     */
    private static String millisecondsToLongDHMS(long mDuration) {

        StringBuilder mStringBuilder = new StringBuilder();
        long temp;
        if (mDuration >= ONE_SECOND) {
            temp = mDuration / MONTHS;
            if (temp > 0) {
                mDuration -= temp * MONTHS;
                mStringBuilder.append(temp).append(" month").append(temp > 1 ? "s" : "")
                        .append(mDuration >= ONE_MINUTE ? ", " : "");
            }
            temp = mDuration / ONE_DAY;
            if (temp > 0) {
                mDuration -= temp * ONE_DAY;
                mStringBuilder.append(temp).append(" day").append(temp > 1 ? "s" : "")
                        .append(mDuration >= ONE_MINUTE ? ", " : "");
            }

            temp = mDuration / ONE_HOUR;
            if (temp > 0) {
                mDuration -= temp * ONE_HOUR;
                mStringBuilder.append(temp).append(" hour").append(temp > 1 ? "s" : "")
                        .append(mDuration >= ONE_MINUTE ? ", " : "");
            }

            temp = mDuration / ONE_MINUTE;
            if (temp > 0) {
                mDuration -= temp * ONE_MINUTE;
                mStringBuilder.append(temp).append(" minute").append(temp > 1 ? "s" : "");
            }

            if (!mStringBuilder.toString().equals("") && mDuration >= ONE_SECOND) {
                mStringBuilder.append(" and ");
            }

            temp = mDuration / ONE_SECOND;
            if (temp > 0) {
                mStringBuilder.append(temp).append(" second").append(temp > 1 ? "s" : "");
            }
            return mStringBuilder.toString();
        } else {
            return "0 second";
        }
    }

    /**
     * @param mMessage The message to be showed as a log
     *                 This function shows a log with the required message.
     */
    public static void showingLogs(String mTag, String mMessage, boolean mIsShown) {
        if (mIsShown)
            Log.i(mTag, mMessage);
    }

    /**
     * <b>public void hideKeyBoard(View view, Context mContext)</b><br>
     * <br>
     *
     * @param view     view at which keyboard needs to be closed <br>
     *                 <br>
     * @param mContext {@link android.app.Activity} {@link Context}
     *                 This function hides the keyboard.
     */
    public void hideKeyboard(View view, Context mContext) {
        InputMethodManager mInputMethodManager = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * @param mCurrentDate              The date object which is to be reformatted
     * @param mRequiredDateFormatString The required format to represent the given current date object
     * @return A String which represents the given current date object according to the required date format
     * This function represents a given date in the specified format.
     */
    public String convertDate(Date mCurrentDate, String mRequiredDateFormatString) {
        String mRequiredDateString = "";
        SimpleDateFormat mRequiredDateFormat = new SimpleDateFormat(mRequiredDateFormatString);
        mRequiredDateString = mRequiredDateFormat.format(mCurrentDate);
        return mRequiredDateString;
    }

    /**
     * @param mContext {@link android.app.Activity} {@link android.content,Context}
     * @return A String which represents the unique device identity number
     * This function shows the unique device identity number.
     */
    public String getDeviceIdentity(Context mContext) {
        return Settings.Secure.getString(mContext.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    /**
     * @param mComparisonDate The date to be compared with
     * @param mContext        {@link android.app.Activity} {@link android.content,Context}
     *                        This function shows the difference between the current date and the given comparison date.
     */
    public String getDateDifference(Date mComparisonDate, Context mContext) {
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date mCurrentDate = null;
        Calendar mCalendar = Calendar.getInstance();
        String mTimeDifference = null;
        try {
            mCurrentDate = mSimpleDateFormat.parse(mSimpleDateFormat.format(mCalendar.getTime()));
            IAUtilities.showingLogs("TAG", "Date time -- " + mCurrentDate.getTime(), IAUtilities.LOGS_SHOWN);
            long mDifference = mCurrentDate.getTime() - mComparisonDate.getTime();

            mTimeDifference = millisecondsToLongDHMS(mDifference);

            Toast.makeText(mContext, mTimeDifference, Toast.LENGTH_LONG).show();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mTimeDifference;
    }

    /**
     * <b>public boolean validateEmailId(String mEmail, Context mContext)</b>
     *
     * @param mEmail   The email address which needs to be validated
     * @param mContext {@link android.app.Activity} {@link android.content,Context}
     * @return boolean value whether the email address is valid or not
     * This function checks the validity of an email address.
     */
    public boolean validateEmailId(String mEmail, Context mContext) {
//        Pattern mPattern;
//        Matcher mMatcher;
//        String regularExpression =
//                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
//                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
//                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
//                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
//                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
//                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";
//
//        CharSequence inputString = mEmail;
//        mPattern = Pattern.compile(regularExpression, Pattern.CASE_INSENSITIVE);
//        mMatcher = mPattern.matcher(inputString);
//        if(mMatcher.matches())
//            return true;
//        else
//            return false;

        return android.util.Patterns.EMAIL_ADDRESS.matcher(mEmail).matches();
    }

    public boolean validateEmailIdMaximumLength(String mEmail, Context mContext){
        return mEmail.length() <= 35;
    }

    /**
     * @param mMobileNumber The mobile number which needs to be validated
     * @param mContext      {@link android.app.Activity} {@link android.content,Context}
     * @return boolean value whether the email address is valid or not
     * This function checks the validity of a mobile number.
     */
    public boolean validateMobileNumber(String mMobileNumber, Context mContext) {
        Pattern mPattern;
        Matcher mMatcher;
        String regularExpression = "[1-9][0-9]{9}$";
        mPattern = Pattern.compile(regularExpression, Pattern.CASE_INSENSITIVE);
        mMatcher = mPattern.matcher(mMobileNumber);
        return mMatcher.matches();
    }

    /**
     * @param mPassword The password which needs to be validated
     * @param mContext  {@link android.app.Activity} {@link android.content,Context}
     * @return boolean value whether the password is valid or not
     * This function checks the validity of a password.
     */
    public boolean validatePasswordMinimumLength(String mPassword, Context mContext) {
        return mPassword.length() >= 5;
    }

    /**
     * @param mPassword The password which needs to be validated
     * @param mContext  {@link android.app.Activity} {@link android.content,Context}
     * @return boolean value whether the password is valid or not
     * This function checks the validity of a password.
     */
    public boolean validatePasswordMaximumLength(String mPassword, Context mContext) {
        return mPassword.length() <= 25;
    }

    /**
     * @param mName    The name which needs to be validated
     * @param mContext {@link android.app.Activity} {@link android.content,Context}
     * @return boolean value whether the name is valid or not
     * This function checks the validity of a name.
     */
    public boolean validateNameMinimumLength(String mName, Context mContext) {
        return mName.length() >= 1;
    }

    /**
     * @param mName    The name which needs to be validated
     * @param mContext {@link android.app.Activity} {@link android.content,Context}
     * @return boolean value whether the name is valid or not
     * This function checks the validity of a name.
     */
    public boolean validateNameMaximumLength(String mName, Context mContext) {
        return mName.length() <= 25;
    }

    /**
     * @param mMessage The message to be showed as a toast
     * @param mContext {@link android.app.Activity} {@link android.content,Context}
     *                 This function generates a toast with the required message.
     */
    public void showingToast(String mMessage, Context mContext) {
        Toast.makeText(mContext, mMessage, TOAST_TIME).show();
    }

    /**
     * @param mTitleString The title string to be shown for the dialog
     * @param mString      The content string to be shown in the dialog
     * @param mContext     {@link android.app.Activity} {@link android.content,Context}
     *                     This function shows an alert dialog box with the given title and the given content string.
     */
    public void showDialog(String mTitleString, String mString, Context mContext) {
        new AlertDialog.Builder(mContext)
                .setTitle(mTitleString)
                .setMessage(mString)
                .setNeutralButton("Back", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface mDialog, int which) {
                        mDialog.dismiss();
                    }
                })
                .show();
    }

    public void setSharedPreferences(Context mContext, String mJsonDataString, String mPreferenceKey) {

        mPrefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.putString(mPreferenceKey, mJsonDataString);
        prefsEditor.commit();
    }

    public String getSharedPreferences(Context mContext, String mPreferenceKey) {
        mPrefs = PreferenceManager
                .getDefaultSharedPreferences(mContext);

        String mJsonDataString = mPrefs.getString(mPreferenceKey, "");
        return mJsonDataString;
    }

    public Long getDateDifferenceInMilliseconds(Date mComparisonDate, Context mContext) {
        SimpleDateFormat mSimpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date mCurrentDate = null;
        Calendar mCalendar = Calendar.getInstance();
        long mDifference = 0;
        try {
            mCurrentDate = mSimpleDateFormat.parse(mSimpleDateFormat.format(mCalendar.getTime()));
            IAUtilities.showingLogs("TAG", "Date time -- " + mCurrentDate.getTime(), IAUtilities.LOGS_SHOWN);
            mDifference = mCurrentDate.getTime() - mComparisonDate.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mDifference;
    }

    public String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    public static Bitmap decodeBase64(String input) {
        byte[] decodedByte = Base64.decode(input, 0);
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.length);
    }

    public String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }


    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    public int getCameraDetails() {
        Camera camera = Camera.open(0);    // For Back Camera
        Camera.Parameters params = camera.getParameters();
        List sizes = params.getSupportedPictureSizes();
        Camera.Size result = null;

        ArrayList<Integer> arrayListForWidth = new ArrayList<Integer>();
        ArrayList<Integer> arrayListForHeight = new ArrayList<Integer>();

        for (int i = 0; i < sizes.size(); i++) {
            result = (Camera.Size) sizes.get(i);
            arrayListForWidth.add(result.width);
            arrayListForHeight.add(result.height);
            IAUtilities.showingLogs("PictureSize", "Supported Size: " + result.width + "height : " + result.height, IAUtilities.LOGS_SHOWN);
        }
        if (arrayListForWidth.size() != 0 && arrayListForHeight.size() != 0) {
            System.out.println("Back max W :" + Collections.max(arrayListForWidth));              // Gives Maximum Width
            System.out.println("Back max H :" + Collections.max(arrayListForHeight));                 // Gives Maximum Height
            System.out.println("Back Megapixel :" + (((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1024000));
        }
        camera.release();

        return (((Collections.max(arrayListForWidth)) * (Collections.max(arrayListForHeight))) / 1000000);
    }

    public String md5(String string) {
        byte[] hash;

        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }

        StringBuilder hex = new StringBuilder(hash.length * 2);

        for (byte b : hash) {
            int i = (b & 0xFF);
            if (i < 0x10) hex.append('0');
            hex.append(Integer.toHexString(i));
        }

        return hex.toString();
    }

    /**
     * @param mContext {@link android.app.Activity} {@link android.content,Context}
     * @return A boolean value whether a network is available or not(wifi or cellular)
     * This function checks whether a network is available or not.
     */
    public boolean isNetworkAvailable(Context mContext) {

        ConnectivityManager mConnectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mNetworkInfo = mConnectivityManager.getActiveNetworkInfo();//.getActiveNetworkInfo();
        if (mNetworkInfo == null || !mNetworkInfo.isConnectedOrConnecting())
            new IAUtilities().showingToast("Network Not Available", mContext);
        return mNetworkInfo != null && mNetworkInfo.isConnectedOrConnecting();
    }

    /**
     * Get the network info
     *
     * @param context
     * @return
     */
    public static NetworkInfo getNetworkInfo(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Check if there is any connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnected(Context context) {
        NetworkInfo info = IAUtilities.getNetworkInfo(context);
        return (info != null && info.isConnected());
    }

    /**
     * Check if there is any connectivity to a Wifi network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedWifi(Context context) {
        NetworkInfo info = IAUtilities.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_WIFI);
    }

    /**
     * Check if there is any connectivity to a mobile network
     *
     * @param context
     * @return
     */
    public static boolean isConnectedMobile(Context context) {
        NetworkInfo info = IAUtilities.getNetworkInfo(context);
        return (info != null && info.isConnected() && info.getType() == ConnectivityManager.TYPE_MOBILE);
    }

    /**
     * Check if there is fast connectivity
     *
     * @param context
     * @return
     */
    public static boolean isConnectedFast(Context context) {
        NetworkInfo info = IAUtilities.getNetworkInfo(context);
        return (info != null && info.isConnected() && IAUtilities.isConnectionFast(info.getType(), info.getSubtype()));
    }

    /**
     * Check if the connection is fast
     *
     * @param type
     * @param subType
     * @return
     */
    public static boolean isConnectionFast(int type, int subType) {
        if (type == ConnectivityManager.TYPE_WIFI) {
            return true;
        } else if (type == ConnectivityManager.TYPE_MOBILE) {
            switch (subType) {
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_CDMA:
                    return false; // ~ 14-64 kbps
                case TelephonyManager.NETWORK_TYPE_EDGE:
                    return false; // ~ 50-100 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                    return true; // ~ 400-1000 kbps
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                    return true; // ~ 600-1400 kbps
                case TelephonyManager.NETWORK_TYPE_GPRS:
                    return false; // ~ 100 kbps
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                    return true; // ~ 2-14 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPA:
                    return true; // ~ 700-1700 kbps
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                    return true; // ~ 1-23 Mbps
                case TelephonyManager.NETWORK_TYPE_UMTS:
                    return true; // ~ 400-7000 kbps
            /*
             * Above API level 7, make sure to set android:targetSdkVersion
             * to appropriate level to use these
             */
                case TelephonyManager.NETWORK_TYPE_EHRPD: // API level 11
                    return true; // ~ 1-2 Mbps
                case TelephonyManager.NETWORK_TYPE_EVDO_B: // API level 9
                    return true; // ~ 5 Mbps
                case TelephonyManager.NETWORK_TYPE_HSPAP: // API level 13
                    return true; // ~ 10-20 Mbps
                case TelephonyManager.NETWORK_TYPE_IDEN: // API level 8
                    return false; // ~25 kbps
                case TelephonyManager.NETWORK_TYPE_LTE: // API level 11
                    return true; // ~ 10+ Mbps
                // Unknown
                case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                default:
                    return false;
            }
        } else {
            return false;
        }
    }

}
