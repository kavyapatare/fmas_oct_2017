package com.findmeashoe_appc.utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.findmeashoe_appc.activity.FMFeedBackActivity;
import com.findmeashoe_appc.activity.FMSettingsActivity;
import com.findmeashoe_appc.activity.FMStartingActivity;
import com.findmeashoe_appc.R;
import com.findmeashoe_appc.adapters.FMSettingsOptionsDrawerAdapter;
import com.findmeashoe_appc.ds.FMCustomer;
import com.findmeashoe_appc.ds.FMSettingsOptionsDrawer;
import com.findmeashoe_appc.ds.FMUrl;
import com.findmeashoe_appc.ui.IACustomButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by iaurokitkat on 11/12/15.
 */
public class FMDrawerLayoutFunctionality{
    private Activity mContext;
    private Integer[] navigationDrawerIcons;
    private String [] mSettingsOptions;
    private DrawerLayout mDrawerLayout;
    private ListView mSettingsOptionsDrawerList;
    private ArrayList<FMSettingsOptionsDrawer> mOptionsList;
    private FMSettingsOptionsDrawerAdapter fmSettingsOptionsDrawerAdapter;

    public FMDrawerLayoutFunctionality(Activity mContext) {
        this.mContext = mContext;
    }

    /**
     * called to initialize the drawer layout on the corresponding screens
     */
    public void initializeDrawerLayout(){
        mDrawerLayout = (DrawerLayout)mContext.findViewById(R.id.common_dl);
        mSettingsOptionsDrawerList = (ListView)mContext.findViewById(R.id.list_nav);
        mSettingsOptions = mContext.getResources().getStringArray(R.array.settings_options_available);

        navigationDrawerIcons = new Integer[]{
                /*R.drawable.settings,*/
                R.drawable.about,
                R.drawable.star,
                R.drawable.about,
                R.drawable.log_out};


        mOptionsList = new ArrayList<>();
        for (int mCount = 0; mCount< mSettingsOptions.length; mCount++){
            FMSettingsOptionsDrawer mTempSource = new FMSettingsOptionsDrawer();
            mTempSource.setSettingsOptionName(mContext, mSettingsOptions[mCount]);
            mTempSource.setSettingsOptionIcon(mContext,navigationDrawerIcons[mCount]);
            mOptionsList.add(mCount, mTempSource);
        }
        fmSettingsOptionsDrawerAdapter = new FMSettingsOptionsDrawerAdapter(mContext, mOptionsList);
        mSettingsOptionsDrawerList.setAdapter(fmSettingsOptionsDrawerAdapter);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        mSettingsOptionsDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                IAUtilities.showingLogs("TAG", "position " + position, IAUtilities.LOGS_SHOWN);
                if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT)){
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                }
                /*if(position == 0){
                    if (!(mContext instanceof FMSettingsActivity)){
                        goToSettingsActivity();
                    }
                }else */
                if(position == 0){
                    FMCustomer fmCustomer = new FMCustomer();
                    fmCustomer.loadData(new IAUtilities().getSharedPreferences(mContext,"FMCustomer"));
                    JSONObject jsonObject;
                    try {
                        jsonObject = new JSONObject(new IAUtilities().getSharedPreferences(mContext,"ServerIP"));
                        FMUrl.CURRENT_IP_ADDRESS = jsonObject.getString("CurrentIp");
                        if (FMUrl.CURRENT_IP_ADDRESS.equalsIgnoreCase("")){
                            FMUrl.CURRENT_IP_ADDRESS = FMUrl.DEMO_BASE_IP_ADDRESS;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    try {
                        openDialog(/*"Server Ip Address : " + FMUrl.CURRENT_IP_ADDRESS + ",\n" +*/
                                "Login Name : " + fmCustomer.getCustomerFirstName() + ",\n" +
                                "Version : " + (mContext.getPackageManager().getPackageInfo(mContext.getPackageName(),0)).versionName
                        /*+ " staging"*/);//staging build
                    } catch (PackageManager.NameNotFoundException e) {
                        e.printStackTrace();
                    }
                }else if(position == 1){
                    if (!(mContext instanceof FMFeedBackActivity)){
                        goToFeedbackActivity();
                    }
                }else if(position == 2){
                    Intent browserIntent;
                    browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(FMUrl.PRIVACY_POLICY_URL));
                    mContext.startActivity(browserIntent);
                }else if (position == 3){
                    goToStartingActivity();
                }
            }
        });
    }

    /**
     * called to open the drawer view on the screen
     */
    public void openDrawer(){
        mDrawerLayout.openDrawer(Gravity.RIGHT);
        new IAUtilities().hideKeyboard(mDrawerLayout,mContext);
    }

    /**
     * called to the log the user out of the current session and
     * take him to the starting screen
     */
    private void goToStartingActivity() {
        new IAUtilities().setSharedPreferences(mContext,null,"FMCustomer");
        new IAUtilities().setSharedPreferences(mContext,null,"CurrentFileIndexNo");
        new IAUtilities().setSharedPreferences(mContext,null,"FootResult");
        new IAUtilities().setSharedPreferences(mContext,null,"LetterPaper");
        Intent intent = new Intent(mContext, FMStartingActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mContext.startActivity(intent);
        mContext.finish();
    }

    /**
     * unused for now. Called to go to the settings screen
     */
    private void goToSettingsActivity() {
        Intent intent = new Intent(mContext, FMSettingsActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);
    }

    /**
     * called to go to the feedback screen
     */
    private void goToFeedbackActivity() {
        Intent intent = new Intent(mContext, FMFeedBackActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        mContext.startActivity(intent);
    }

    /**
     * called to show a popup message to the user which
     * enables him/her to check the company policies
     * @param mMessageString the message to be displayed to the user
     */
    public void openDialog(final String mMessageString) {
        final Dialog dialog = new Dialog(mContext); // context, this etc.
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setTitle("App Version : 2.0");
        dialog.setContentView(R.layout.two_button_dialog);
        TextView mMessageContentView = (TextView) dialog.findViewById(R.id.message_dialog_tv);
        mMessageContentView.setText(mMessageString);
        dialog.show();
        IACustomButton mBackToDashboardButton = (IACustomButton) dialog.findViewById(R.id.dialog_done_btn);
        IACustomButton mGoToLinkButton = (IACustomButton) dialog.findViewById(R.id.dialog_link_btn);
        mBackToDashboardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        mGoToLinkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(FMUrl.HOME_SCREEN_URL));
                mContext.startActivity(browserIntent);
                dialog.dismiss();
            }
        });
    }
}
